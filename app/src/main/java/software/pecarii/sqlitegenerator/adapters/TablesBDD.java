package software.pecarii.sqlitegenerator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.vues.VueTableBDD;

public final class TablesBDD extends BaseAdapter {
  public String[] tables_affichées;
  public final Context contexte_fourni;
  
  public TablesBDD(Context contexte, String[] tables) {
    this.tables_affichées = tables;
    this.contexte_fourni = contexte;
  }
  
  @Override
  public View getView(int position, View vue_table_bdd, 
                      ViewGroup racine) {

    if (vue_table_bdd == null) {
      vue_table_bdd = 
        LayoutInflater.from(this.contexte_fourni).
          inflate(R.layout.liste_fichiers_bdd_element, racine, false);
      /*System.out.println("vue_table_bdd == null");*/
    }
    
    ((VueTableBDD) vue_table_bdd).concernant(tables_affichées[position]);
    
    return vue_table_bdd;
    
  }
  
  @Override
  public int getCount() {
    return tables_affichées.length;
  }
  
  @Override
  public long getItemId(int position) {
    return (long) position;
  }
  
  @Override
  public Object getItem(int position) {
    return tables_affichées[position];
  }
  
  @Override
  public boolean hasStableIds() {
    return false;
  }
}
