package software.pecarii.sqlitegenerator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import software.pecarii.sqlitegenerator.vues.ListArrayViewElement;

public class ListArrayAdapter<T> extends BaseAdapter {
  public T[] array_to_display;
  public Context context;
  public int element_view_resid;

  public ListArrayAdapter(T[] array, int view_resid, Context c) {
    this.array_to_display = array;
    this.context = c;
    this.element_view_resid = view_resid;
  }

  @Override
  public View getView(int position, View recycled_view, 
                      ViewGroup root) {

    if (recycled_view == null) {
      recycled_view = 
        LayoutInflater.from(this.context).
          inflate(element_view_resid, root, false);
    }
    
    ((ListArrayViewElement<T>) recycled_view).
      display(array_to_display[position]);
    
    return recycled_view;
  }

  @Override
  public int getCount() {
    return array_to_display.length;
  }
  
  @Override
  public long getItemId(int position) {
    return (long) position;
  }
  
  @Override
  public Object getItem(int position) {
    return array_to_display[position];
  }
  
  @Override
  public boolean hasStableIds() {
    return false;
  }
}

