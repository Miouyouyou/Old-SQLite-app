package software.pecarii.sqlitegenerator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class FakeAdapter extends BaseAdapter {
  public int how_many_to_display;
  public Context context;
  public int element_view_resid;

  public FakeAdapter(int n_elements, int view_resid, Context c) {
    this.how_many_to_display = n_elements;
    this.context = c;
    this.element_view_resid = view_resid;
  }

  public void update(int n_elements) {
    this.how_many_to_display = n_elements;
  }

  @Override
  public View getView(int position, View recycled_view, 
                      ViewGroup root) {

    if (recycled_view == null) {
      recycled_view = 
        LayoutInflater.from(this.context).
          inflate(element_view_resid, root, false);
    }
    
    return recycled_view;
  }

  @Override
  public int getCount() {
    return this.how_many_to_display;
  }
  
  @Override
  public long getItemId(int position) {
    return (long) position;
  }
  
  @Override
  public Object getItem(int position) {
    return position;
  }
  
  @Override
  public boolean hasStableIds() {
    return true;
  }
}
