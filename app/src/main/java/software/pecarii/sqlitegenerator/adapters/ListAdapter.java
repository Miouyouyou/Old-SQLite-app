package software.pecarii.sqlitegenerator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import software.pecarii.sqlitegenerator.vues.ListArrayViewElement;

public class ListAdapter<T> extends BaseAdapter {
  public List<T> array_to_display;
  public Context context;
  public int element_view_resid;
  public final LayoutInflater inflater;

  public ListAdapter(List<T> array, int view_resid, Context c) {
    this.array_to_display = array;
    this.context = c;
    this.element_view_resid = view_resid;
    this.inflater = LayoutInflater.from(c);
  }

  @Override
  public View getView(int position, View recycled_view, 
                      ViewGroup root) {

    if (recycled_view == null) {
      recycled_view = 
        inflater.inflate(element_view_resid, root, false);
    }
    
    ((ListArrayViewElement<T>) recycled_view).
      display(array_to_display.get(position));
    
    return recycled_view;
  }

  @Override
  public int getCount() { return array_to_display.size(); }
  
  @Override
  public long getItemId(int position) { return (long) position; }
  
  @Override
  public Object getItem(int position) {
    return array_to_display.get(position);
  }
  
  @Override
  public boolean hasStableIds() { return false; }

}
