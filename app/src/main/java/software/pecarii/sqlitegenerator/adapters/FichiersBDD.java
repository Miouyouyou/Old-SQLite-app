package software.pecarii.sqlitegenerator.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.Arrays;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.vues.VueFichierBDD;

public final class FichiersBDD extends RecyclerView.Adapter<VueFichierBDD> {

  public final View.OnClickListener click_dispatcher;
  public final LayoutInflater inflater;
  public File[] bdd_gérées;
  private int s_fichiers;
  private int n_fichiers;
  
  public FichiersBDD(final LayoutInflater inflater,
                     final View.OnClickListener dispatcher) {
    this.inflater = inflater;
    this.bdd_gérées = new File[8];
    this.s_fichiers = 8;
    this.n_fichiers = 0;
    this.click_dispatcher = dispatcher;
    this.setHasStableIds(true);
  }
  
  @Override
  public int getItemCount() { return this.n_fichiers; }
  
  @Override
  public long getItemId(final int position) {
    return (long) this.bdd_gérées[position].hashCode();
  }
  
  @Override
  public int getItemViewType(final int position) {
    return R.layout.liste_fichiers_bdd_element;
  }

  @Override
  public VueFichierBDD
  onCreateViewHolder(final ViewGroup parent, final int position) {
    View v = inflater.inflate(R.layout.liste_fichiers_bdd_element,
                              parent, false);
    v.setOnClickListener(click_dispatcher);
    return new VueFichierBDD(v);
  }

  @Override
  public void
  onBindViewHolder(final VueFichierBDD vue_fichiers, final int position) {
    vue_fichiers.concernant(bdd_gérées[position]);
  }

  @Override
  public void onDetachedFromRecyclerView(final RecyclerView r) {
    menage();
  }

  public void ajouter(final File[] fichiers) {
    int n_nouveaux = fichiers.length;
    if (this.n_fichiers + n_nouveaux < this.s_fichiers);
    else {
      this.s_fichiers <<= 1;
      this.s_fichiers += n_nouveaux;
      this.bdd_gérées = Arrays.copyOf(bdd_gérées, s_fichiers);
    }
    System.arraycopy(fichiers, 0,
                     this.bdd_gérées, this.n_fichiers, n_nouveaux);

    n_fichiers += n_nouveaux;
    this.notifyDataSetChanged();
  }

  public void menage() {
    Arrays.fill(this.bdd_gérées, null);
    this.n_fichiers = 0;
  }

  public void reset() {
    menage();
    this.notifyDataSetChanged();
  }

  public File[] bdd_gérées() {
    return Arrays.copyOf(this.bdd_gérées, n_fichiers);
  }

}

