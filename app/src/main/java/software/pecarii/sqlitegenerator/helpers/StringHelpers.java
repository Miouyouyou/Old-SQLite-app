package software.pecarii.sqlitegenerator.helpers;

import java.lang.reflect.Array;

public final class StringHelpers {

  public final static StringBuilder sb;

  static {
    sb = new StringBuilder(64);
  }

  public static String concat(String... strings) {
    sb.delete(0, 2147483647);
    for (String string : strings) { sb.append(string); }
    return sb.toString();
  }

  /** Generate a concatenation of all the String provided, separated by 
   *  the provided separator.
   * Example :
   * <code>list(" , ", "Hey", "Hi", "Hello");</code>
   * -> "Hey , Hi , Hello"
   * @param separator  The String that will be inserted between the 
   *                   concatenated elements.
   * @param elements  The concatenated strings.
   * @return a concatenation of all the String provided, separated by 
   *         the provided separator.
   * @throws NullPointerException if the separator or the elements are 
   *         null.
   */
  public static String list(String separator, String... elements) {
  
    if (elements.length < 1) return "";
    
    StringBuilder string_builder = new StringBuilder(64);
    string_builder.append(elements[0]);

    for (int index = 1; index < elements.length; index++) {
      string_builder.append(separator);
      string_builder.append(elements[index]);
    }

    return string_builder.toString();
  }
 
  /** Generate a concatenation of the string representations of the
   *  provided objects, separated by the provided separator.
   * Example :
   * Given that :
   * - ObjectT1.toString() returns "Hi"
   * - ObjectT2.toString() returns "Hello"
   * 
   * <code>list(" , ", ObjectT1, ObjectT2);</code>
   * -> "Hey , Hi , Hello"
   * @param separator  The String that will be inserted between the 
   *                   concatenated elements.
   * @param elements  The objects, of the same class, whose string
   *                  representations will be concatenated.
   * @return a concatenation of the string representations of the 
   *         Objects provided, separated by the provided separator.
   * @throws NullPointerException if the separator or the elements are 
   *         null.
   */
  public static <T> String list(String separator, T... elements) {
  
    if (elements.length < 1) return "";
    
    StringBuilder string_builder = new StringBuilder(64);
    string_builder.append(elements[0].toString());

    for (int index = 1; index < elements.length; index++) {
      string_builder.append(separator);
      string_builder.append(elements[index].toString());
    }

    return string_builder.toString();
  }  
 
  /** Generate a String array containing the result of toString() of
   *  each element of the provided array.
   *
   * @param array  The provided array.
   *
   * @return a String array containing (each element of the provided
   *         array).toString()
   */  
  public static String[] toString_array(Object array) {
    final int n_elements    = Array.getLength(array);
    final String[] elements = new String[n_elements];
    
    for (int i = 0; i < n_elements; i++)
      elements[i] = Array.get(array, i).toString();
    
    return elements;
  } 
  
  public static <T> int cherche(T element, T[] elements) {
    int i = 0;

    for (; i < elements.length; i++)
      if (elements[i].equals(element)) break;

    return i;
  }

  public static String suffix(String text, String desired_suffix) {
    String trimmed_text = text.trim();
    int suffix_length = desired_suffix.length();
    int text_size = trimmed_text.length();
    if (text_size < suffix_length || 
        !trimmed_text.substring(text_size-suffix_length).equals(desired_suffix)) {
      return String.format("%s%s", trimmed_text, desired_suffix);
    } else return trimmed_text;
  }
}
