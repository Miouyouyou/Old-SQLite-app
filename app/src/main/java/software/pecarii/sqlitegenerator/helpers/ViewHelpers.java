package software.pecarii.sqlitegenerator.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import static android.content.DialogInterface.OnMultiChoiceClickListener;

import android.graphics.drawable.Drawable;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ViewSMD;

public class ViewHelpers {

  public static int[] resolution(Activity activity) {
    Display display = activity.getWindowManager().getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    return new int[] {size.x, size.y};
  }

  public static void set_entries_of(Spinner spinner, String[] entries) {
    spinner.setAdapter(standard_spinner_adapter_with(entries));
  }

  public static CharSequence span_lines(String text, Object... spans) {

    if (text.length() != 0); else return text;

    final int n_spans = spans.length;
    final SpannableString spanned = new SpannableString(text);
    int span  = 0;
    int start = 0;
    int end   = 0;

    end = text.indexOf('\n', start);
    if (end != -1) spanned.setSpan(spans[span++], start, end, 17);
    else return span_text(spanned, spans[span]);

    start = end+1;
    end = text.indexOf('\n', start);

    for (; span < n_spans && end != -1; span++) {
      spanned.setSpan(spans[span], start, end, 0);
      start = end+1;
      end   = text.indexOf('\n', start);
    }

    if (span < n_spans)
      spanned.setSpan(spans[span], start, spanned.length(), 0);

    return spanned;
  }

  public static SpannableString
  span_text(@NonNull SpannableString text, @NonNull Object span) {
    text.setSpan(span, 0, text.length(), 0);
    return text;
  }

  public static SpannableString
  span_text(@NonNull CharSequence text, @NonNull Object span) {
    return span_text(new SpannableString(text), span);
  }

  public static ArrayAdapter<String> 
    standard_spinner_adapter_with(String[] entries) {
    ArrayAdapter<String> adapter = 
      new ArrayAdapter<String>(Application.contexte(),
                          R.layout.support_simple_spinner_dropdown_item,
                          android.R.id.text1,
                          entries);
    adapter.setDropDownViewResource(
      R.layout.support_simple_spinner_dropdown_item
    );
    return adapter;
  }

  public static
  View[] warning_popup_views(LayoutInflater inflater,
                             @StringRes int content_text_res,
                             @StringRes int check_text_res) {

    View[] returned_views = new View[3];

    returned_views[0] = 
      inflater.inflate(R.layout.popup_avertissement_avec_coche, null, false);
    returned_views[1] = 
      returned_views[0].findViewById(R.id.message_avertissement);
    returned_views[2] = 
      returned_views[0].findViewById(R.id.coche_avertissement_compris);
    ((TextView) returned_views[1]).setText(content_text_res);
    ((TextView) returned_views[2]).setText(check_text_res);

    return returned_views;
  }

  public static 
  Dialog create_dialog(Activity activity, View dialog_view,
                       int[] buttons_labels,
                       DialogInterface.OnClickListener buttons_action ) {
    AlertDialog.Builder dialog_builder =
      new AlertDialog.Builder(activity);
    dialog_builder.setView(dialog_view);
    dialog_builder.setPositiveButton(buttons_labels[0], buttons_action);
    dialog_builder.setNegativeButton(buttons_labels[2], buttons_action);
    if (buttons_labels[1] != 0)
      dialog_builder.setNeutralButton(buttons_labels[1], buttons_action);
    return dialog_builder.create();
  }

  public static
  Dialog create_dialog(Activity activity, CharSequence[] list_elements,
                       OnMultiChoiceClickListener list_onclick,
                       int[] buttons_labels,
                       DialogInterface.OnClickListener buttons_action) {
    AlertDialog.Builder dialog_builder =
      new AlertDialog.Builder(activity);
    dialog_builder.setMultiChoiceItems(list_elements, null, list_onclick);
    dialog_builder.setPositiveButton(buttons_labels[0], buttons_action);
    dialog_builder.setNegativeButton(buttons_labels[2], buttons_action);
    if (buttons_labels[1] != 0)
      dialog_builder.setNeutralButton(buttons_labels[1], buttons_action);
    return dialog_builder.create();
  }

  public static
  TextView[] convert_menu(@MenuRes int menu_res, Object dispatch_object,
                          String dispatcher_method) {
    PopupMenu menu_builder =
      new PopupMenu(Application.contexte(), new View(Application.contexte()));
    menu_builder.inflate(menu_res);
    Menu menu = menu_builder.getMenu();

    final TextView[] menu_elements = new TextView[menu.size()];
    menu_builder = null;

    View.OnClickListener dispatcher =
      new ViewSMD(dispatcher_method, dispatch_object);

    TextView t;
    MenuItem current_item;
    for (int i = 0; i < menu.size(); i++) {
      current_item = menu.getItem(i);
      t            = new TextView(Application.contexte());

      t.setTextAppearance(Application.contexte(), R.style.texte_menu_inferieur);
      t.setGravity(Gravity.CENTER_HORIZONTAL);
      t.setText(current_item.getTitleCondensed());
      t.setId(current_item.getItemId());
      t.setCompoundDrawablesWithIntrinsicBounds(null, current_item.getIcon(), null, null);
      t.setOnClickListener(dispatcher);
      if (current_item.isVisible()); else t.setVisibility(View.GONE);

      menu_elements[i] = t;
    }

    return menu_elements;
  }

  /* Focus the next view */
  public static void move_focus(View v, int direction) {
    View next_view;
    if ((next_view = v.focusSearch(direction)) != null)
      next_view.requestFocus(direction);

  }

  public static int dp_to_px(int dp) {
    return (int) TypedValue.applyDimension(
      TypedValue.COMPLEX_UNIT_DIP, dp, Application.metriques()
    );
  }
}
