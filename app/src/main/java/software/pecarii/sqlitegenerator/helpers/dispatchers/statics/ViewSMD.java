package software.pecarii.sqlitegenerator.helpers.dispatchers.statics;

import android.view.View;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import software.pecarii.sqlitegenerator.helpers.MethodHelpers;

public class ViewSMD implements
  View.OnClickListener, View.OnLongClickListener {
  public Method method;
  public Object target;
  
  public ViewSMD(String method_name, Object target) {
    this.target = target;
    this.method = 
      MethodHelpers.get_method(method_name, target.getClass(), View.class);
  }
  
  public void onClick(View v) { 
    MethodHelpers.invoke_method(method, target, v);
  }

  public boolean onLongClick(View v) {
    return (Boolean) MethodHelpers.invoke_method(method, target, v);
  }
}
