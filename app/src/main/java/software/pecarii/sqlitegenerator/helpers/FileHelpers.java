package software.pecarii.sqlitegenerator.helpers;

import java.io.File;

public final class FileHelpers {

  public static String[] file_sizes;
  public static String readable_size_format;
  
  static {
    file_sizes = new String[]
      {"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB"};
    readable_size_format = "%d %s";
  }

  /**
   Return a human readable version of the provided file size.
   
   The convention is :
   - 0 return '0 Bytes'
   - 1023 return '1023 Bytes',
   - 1024 return '1 KiB',
   - 10240 return '10 KiB',
   - 1048575 return '1023 KiB',
   - 1048576 return '1 MiB',
   etc...
   Up to ZiB.
   
   Assumptions : The provided size is assumed positive !
   Negative sizes will just return "-sizeB" where 'size' will be
   replaced by the provided size number.
   
   @param file the file providing the size to represent.
   @return a human readable representation of the provided file's size,
           as a String.
  */
  public static String readable_filesize(File file) {
    return readable_filesize(file.length());
  }
  
  /**
   Return a human readable version of the provided file size.
   
   The convention is :
   - 0 return '0 Bytes'
   - 1023 return '1023 Bytes',
   - 1024 return '1 KiB',
   - 10240 return '10 KiB',
   - 1048575 return '1023 KiB',
   - 1048576 return '1 MiB',
   etc...
   Up to ZiB.
   
   Assumptions : The provided size is assumed positive !
   Negative sizes will just return "-size Bytes" where 'size' will be
   replaced by the provided size number.
   
   @param file_size the size to display
   @return a human readable representation of the provided file size,
           as a String.
  */
  public static String readable_filesize(long file_size) {
    int file_sizes_index = 0;
    
    while( file_size > 1023 ) {
      file_size = file_size >>> 10;
      file_sizes_index++;
    }
    
    return String.format(readable_size_format, 
                         file_size, 
                         file_sizes[file_sizes_index]);
  }

  /**
    Returns a path, containing the provided segments, separated by the 
    File separator understood by the System, according to the current
    JVM. ('/' for Unix/Android JVM).
    
    This method does not check if the returned path lead to an existing
    file or directory.
    
    This method does not prefix the first segment with a File separator.
    
    Example :
      ExternalStorage.path_from_segments('path', 'to', 'file')
    -> 'path/to/file'
    
    @param segments the path segments following the external storage
                    directory's path.
    
    @return a file's path formed with the provided segments, 
            as a String.
  */
  public static String path_from_segments(String... segments) {
    return StringHelpers.list(File.separator, segments);
  }

  public static void
  delete_files(File[] files) {
    for (File file : files) file.delete();
  }

  public static void 
  delete_files_within(File[] files, boolean[] to_delete) {
    for (int i = 0; i < files.length; i++)
      if (to_delete[i]) files[i].delete();
  }
}
