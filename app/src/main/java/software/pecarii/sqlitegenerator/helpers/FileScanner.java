package software.pecarii.sqlitegenerator.helpers;

import java.io.File;
import java.util.Arrays;

public abstract class FileScanner {

  private static final class SubFolder {
    final int length;
    int i;
    final File[] files;

    SubFolder(File[] files) {
      this.files   = files;
      this.length  = files.length;
      this.i = 0;
    }
  }

  private static File
  next_file_of(final SubFolder subfolder) {
    return subfolder.files[subfolder.i++];
  }

  private static boolean
  more_files_in(final SubFolder subfolder) {
    return subfolder.i != subfolder.length;
  }

  abstract public boolean good(File file);
  abstract public void report(File[] files);
  abstract public void final_report(File[] files);

  final byte report_limit = 10; /* report after x files */
  final byte max_depth    = 20; /* don't go too deep */

  public void scan(final File base_directory) {

    final File[] found_files = new File[report_limit];
    final SubFolder[] subfolders = new SubFolder[max_depth];
    final byte final_depth = max_depth - 1;

    try {
      subfolders[0] = new SubFolder(base_directory.listFiles());
    }
    catch (NullPointerException e) { return; }

    byte file_count = 0;
    byte current_depth = 0;

    SubFolder current_folder;
    File current_file;
    File[] sub_files;

    while (current_depth != -1 && !Thread.interrupted()) {

      current_folder = subfolders[current_depth];
      if (more_files_in(current_folder)) {

        current_file = next_file_of(current_folder);
        //System.err.println(current_file.getAbsolutePath());
        /* Got a directory that WE CAN USE ? Because Android LOVES
           returning directories that you cannot list files FROM !
           So we check if the file list isn't NULL.
         */
        if (current_file.isDirectory() &&
          current_file.getAbsolutePath().intern() != "/sys" &&
          current_file.getAbsolutePath().intern() != "/proc" &&
          current_depth < final_depth &&
          (sub_files = current_file.listFiles()) != null) {
          current_depth++;
          subfolders[current_depth] = new SubFolder(sub_files);
        }
        /* Else, got a good file ? */
        else if (current_file.isFile() && good(current_file)) {
          found_files[file_count++] = current_file;
          if (file_count < report_limit);
          else {
            report(Arrays.copyOf(found_files, report_limit));
            file_count = 0;
            Arrays.fill(found_files, null);
          }
        }
      }
      else current_depth--;

    }
    /* Final report */
    final_report(Arrays.copyOf(found_files, file_count));
  }

}
