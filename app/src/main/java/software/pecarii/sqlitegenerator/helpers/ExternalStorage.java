package software.pecarii.sqlitegenerator.helpers;

import android.os.Environment;

import java.io.File;
import java.util.Arrays;

import software.pecarii.sqlitegenerator.helpers.FileHelpers;
import software.pecarii.sqlitegenerator.helpers.StringHelpers;

public final class ExternalStorage {

  public static final String pfep_format = "%s%s%s";
  public static final String path;
  
  static {
    path = Environment.getExternalStorageDirectory().getAbsolutePath();
  }
  
  public static boolean writeable() {
    String state = Environment.getExternalStorageState();
    return Environment.MEDIA_MOUNTED.equals(state);

  }

  public static boolean readable() {
    String state = Environment.getExternalStorageState();
    return (Environment.MEDIA_MOUNTED.equals(state) ||
        Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
  }

  public static File directory() {
    return Environment.getExternalStorageDirectory();
  }

  public static String path() {
    return Environment.getExternalStorageDirectory().getAbsolutePath();
  }
  
  public static String path_from_external_path(String... segments) {
    if (segments == null || segments.length < 1) return path();
    
    return String.format(pfep_format,
                         path(),
                         File.separator,
                         FileHelpers.path_from_segments(segments));
    
  }
}

