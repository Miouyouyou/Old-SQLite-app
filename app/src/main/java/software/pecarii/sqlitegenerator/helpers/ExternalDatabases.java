package software.pecarii.sqlitegenerator.helpers;

import software.pecarii.sqlitegenerator.helpers.ExternalStorage;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;

public final class ExternalDatabases {

  // A SQLite database contains at least 1 page (512 bytes) and a 
  // complete header (100 bytes). Within the header, the magic signature
  // is 16 bytes long.
  // Still, any sqlite3 file seems to be, at least, 1024 bytes long...
  // https://www.sqlite.org/fileformat.html
  final static int SQLITE_SIGNATURE_SIZE = 16;
  final static int SQLITE_FILE_MINIMUM_SIZE = 612;
  final public static File external_db_folder;
  final public static byte[] sqlite3_header;
  
  static {
    external_db_folder = 
      new File(ExternalStorage.directory().getAbsolutePath(), "SQL-DB");
    if (!external_db_folder.exists()) { external_db_folder.mkdir(); }
    sqlite3_header = new byte[] {
       0x53, 0x51, 0x4c, 0x69, 0x74, 0x65, 0x20, 0x66, 
       0x6f, 0x72, 0x6d, 0x61, 0x74, 0x20, 0x33, 0x00
    };
  }

  public static File[] list_from_folder() {
    return sqlite_files_in(external_db_folder.listFiles());
  }
  
  public static byte create_database(String name) {
    return SQLiteHelpers.create(
      new File(external_db_folder, name).getAbsolutePath()
    );
  }

  public static File[] sqlite_files_in(File[] available_files_list) {

    final File[] temporary_database_list = 
      new File[available_files_list.length];
    final byte[] read_bytes = new byte[SQLITE_SIGNATURE_SIZE];
    
    int temp_list_pos = 0;
    FileInputStream file_reader = null;
    
    for (File unknown_file : available_files_list) {
      try {
          
        if (unknown_file.length() < SQLITE_FILE_MINIMUM_SIZE)
          continue;
        
        file_reader = new FileInputStream(unknown_file);
        file_reader.read(read_bytes, 0, SQLITE_SIGNATURE_SIZE);
        file_reader.close();
        
        if (Arrays.equals(read_bytes,sqlite3_header)) {
          temporary_database_list[temp_list_pos++] = unknown_file;
        }
        
      } catch (java.io.IOException e) { }
    }
    
    return
      Arrays.copyOfRange(temporary_database_list, 0, temp_list_pos);

  }

}

