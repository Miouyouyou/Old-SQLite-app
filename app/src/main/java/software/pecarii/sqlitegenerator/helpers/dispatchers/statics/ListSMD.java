package software.pecarii.sqlitegenerator.helpers.dispatchers.statics;

import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.lang.reflect.Method;

import software.pecarii.sqlitegenerator.helpers.MethodHelpers;

public class ListSMD implements ListView.OnItemClickListener {
  public Method method;
  public Object target;

  public ListSMD(String method_name, Object target) {
    this.target = target;
    this.method = MethodHelpers.get_method(
      method_name, target.getClass(),
      AdapterView.class, View.class, int.class, long.class);
  }

  @Override
  public void
  onItemClick(AdapterView liste, View e, int p, long id) {
    MethodHelpers.invoke_method(method, target, liste, e, p, id);
  }
}
