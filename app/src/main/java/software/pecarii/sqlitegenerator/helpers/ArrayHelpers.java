package software.pecarii.sqlitegenerator.helpers;

import java.util.Arrays;

public class ArrayHelpers {
  public static <T> T[] extend(T[] array, int added_places) {
    return Arrays.copyOf(array, array.length+added_places);
  }
  public static int[] extend(int[] array, int added_places) {
    return Arrays.copyOf(array, array.length+added_places);
  }
  public static <T> T[] add(T[] elements, T element) {
    int n_elements = elements.length;
    T[] nouveaux_elements = extend(elements, 1);
    nouveaux_elements[n_elements] = element;
    return nouveaux_elements;
  }
  public static <T> T[] add(T[] elements, T[] new_elements) {
    int n_elements = elements.length, n_new = new_elements.length;
    T[] nouveaux_elements = extend(elements, n_new);
    System.arraycopy(new_elements, 0, nouveaux_elements, n_elements, n_new);
    return nouveaux_elements;
  }
  public static <T> T[] store(T[] elements, T element, char index) {
    if (elements.length > index);
    else elements = extend(elements, (index-elements.length)+1);

    elements[index] = element;
    return elements;
  }
  public static int[] store(int[] elements, int element, char index) {
    if (elements.length > index);
    else elements = extend(elements, (index-elements.length)+1);

    elements[index] = element;
    return elements;
  }
}
