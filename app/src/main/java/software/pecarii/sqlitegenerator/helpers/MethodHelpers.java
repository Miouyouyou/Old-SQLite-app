package software.pecarii.sqlitegenerator.helpers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class MethodHelpers {
  public static Method 
  get_method(String method_name, Class c, Class... signature) {
    try { return c.getMethod(method_name, signature); }
    catch (NoSuchMethodException e) { throw new RuntimeException(e); }
  }
  
  public static Object
  invoke_method(Method method, Object target, Object... arguments) {
    try { return method.invoke(target, arguments); }
    catch (IllegalAccessException | InvocationTargetException |
           ExceptionInInitializerError e) {
      throw new RuntimeException(e);
    }
  }
}
