package software.pecarii.sqlitegenerator.helpers;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import software.pecarii.sqlitegenerator.helpers.ScoredObject;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.helpers.sqltypes.PrimaryKey;

public final class SQLiteHelpers {

  public static final CursorFactory cursor_factory =
    new FabriqueDeCurseurs();
  public static final String tables_names_list_query =
    "SELECT name FROM sqlite_master WHERE type = 'table'";
  public static final String table_schema_query =
    "SELECT sql FROM sqlite_master WHERE name = ?";
  public static final int[] write_modes = new int[]
    {SQLiteDatabase.OPEN_READONLY, SQLiteDatabase.OPEN_READWRITE};
  public static final int TI_NAME = 1;
  public static final int TI_TYPE = 2;
  public static final int TI_PK = 5;
  public static final byte INTEGER = 0;
  public static final byte TEXT    = 1;
  public static final byte BLOB    = 2;
  public static final byte REAL    = 3;
  public static final byte NUMERIC = 4;
  public static final StringBuilder quote_builder = new StringBuilder(32);

  public static class FabriqueDeCurseurs implements CursorFactory {
    @Override
    public Cursor newCursor(SQLiteDatabase db, 
                            SQLiteCursorDriver masterQuery,
                            String editTable,
                            SQLiteQuery query) {
      /*System.err.println(query.toString());*/
      return new SQLiteCursor(masterQuery, editTable, query);
    }
  }

  protected static class SBLock {}

  public static String[] list_tables_names_in(SQLiteDatabase database) {
    Cursor cursor = database.rawQuery(tables_names_list_query, null);
    int names_count = cursor.getCount();
    String[] tables_names = new String[names_count];
    int tn_index = 0;
    
    while (cursor.moveToNext()) {
      tables_names[tn_index++] = cursor.getString(0).intern();
    }
    cursor.close();
    return tables_names;
  }
  
  public static final byte DATABASE_CREATED = 0;
  public static final byte FILENAME_ALREADY_USED = 1;
  public static final byte INVALID_PATH = 2;
  public static final byte COULD_NOT_REOPEN_DATABASE = 3;

  /**
   Create an empty SQLite Database at the path provided.
  
   @param path The full path of the SQLite database, including its name.
   
   @return DATABASE_CREATED if the SQLiteDatabase was created 
                            successfully.
           FILENAME_ALREADY_USED if a file with an identical name was 
                                 found, before creating the database.
           COULD_NOT_REOPEN_DATABASE if the SQLiteDatabase was created
                                     but could not be re-opened.
  */
  public static byte create(String path) {
  
    /* 
     The temporary_reference is used to hold a temporary reference
     of the Database, that will be provided when calling 
     openOrCreateDatabase.
       
     The point is to create the database, close it and the reopen-it
     to ensure that the database is in an usable state. 
    */

    SQLiteDatabase temporary_reference;
    SQLiteDatabase created_db;
    boolean can_reopen;
    byte result;

    File provided_file = new File(path);
    if (!provided_file.getParentFile().exists())
      result = INVALID_PATH;
    else if (provided_file.exists())
      result = FILENAME_ALREADY_USED;
    else {
      temporary_reference =
              SQLiteDatabase.openOrCreateDatabase(path, cursor_factory);
      temporary_reference.close();

      created_db =
              SQLiteDatabase.openDatabase(path, cursor_factory,
                      SQLiteDatabase.OPEN_READWRITE);
      can_reopen = created_db.isOpen();
      created_db.close();
      result = (can_reopen ? DATABASE_CREATED : COULD_NOT_REOPEN_DATABASE);
    }

    return result;
  }


 
  public static PrimaryKey[] 
    all_primary_keys_in(SQLiteDatabase db, String[] tables) {
     
    PrimaryKey[] returned_pks = new PrimaryKey[tables.length];
    Cursor cursor = null;

    /* 
       Format of 'pragma table_info()' :
       column_pos | name | type | not_null? | default_value | pk_pos
       
       pk_pos will be : 
       - 0 if the column is NOT a primary key
       - a positive integer if the column IS part of the primary key
       The number will also represent the position in the the primary
       key composition, after version 3.7.??
       
       Example : In a table with 3 TEXT columns 'a, b, c', if the 
       primary key is composed like this 'PRIMARY KEY (c,b,a)', the 
       result of pragma table_info will be :
       < SQLite 3.7.?? : >= SQLITE 3.7.??
       ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻   ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻
       0|a|TEXT|0||1     0|a|TEXT|0||3
       1|b|TEXT|0||1     1|a|TEXT|0||2
       2|c|TEXT|0||1     2|a|TEXT|0||1
       
    */
    String current_table = null;
    ScoredObject[] scored_column_list = null;
    Column[] primary_key_columns = null;
    int positions[] = null;
    int n_pks = 0;
    int primary_key_position = 0;
    
    for (int i = 0; i < tables.length; i++) {
      n_pks = 0;
      current_table = tables[i];
      
      cursor = 
        db.rawQuery(
          String.format("pragma table_info(%s)", quote(current_table)),
          null);

      scored_column_list = 
        new ScoredObject[cursor.getCount()];

      while(cursor.moveToNext()) {
        primary_key_position = cursor.getInt(TI_PK);
        if (primary_key_position > 0) {
          scored_column_list[n_pks++] =
            new ScoredObject(
              new Column(
                cursor.getString(TI_NAME),
                cursor.getString(TI_TYPE)
              ),
              primary_key_position
            );
        }
      }
      cursor.close();

      Arrays.sort(scored_column_list, 0, n_pks);
      
      primary_key_columns = new Column[n_pks];
      positions = new int[n_pks];
      for (int c = 0; c < n_pks; c++) {
        primary_key_columns[c] = (Column) scored_column_list[c].o;
        positions[c] = scored_column_list[c].position;
      }
        
      returned_pks[i] = new PrimaryKey(primary_key_columns, positions);
    }

    return returned_pks;
  }

  public static String schema_of(String table, SQLiteDatabase bdd) {
    Cursor c =
      bdd.rawQuery(table_schema_query.replace("?", quote(table)), null);
    String schema;
    if(c.getColumnCount() != 0) {
      c.moveToNext();
      schema = c.getString(0);
    }
    else schema = "";
    c.close();

    return schema;
  }

  public static String quote(String name) {
    return String.format("'%s'", name.replace("'", "''")).intern();
  }

  public static String unquote(String quoted_string) {
    if (quoted_string == null) return null;
    return quoted_string.replaceAll("^'|'$", "").replace("''", "'");
  }

  public static String quote(String[] names) {
    synchronized(SBLock.class) {
      quote_builder.delete(0, 0x7fffffff);
      quote_builder.append('\'');
      quote_builder.append(names[0].replace("'", "''"));
      quote_builder.append('\'');
      for (char i = 1; i < names.length; i++) {
        quote_builder.append(',');
        quote_builder.append('\'');
        quote_builder.append(names[i].replace("'", "''"));
        quote_builder.append('\'');
      }
      return quote_builder.toString();
    }
  }
  
  /**
   Open the SQLite Database pointed by the provided path.
   
   Do not check anything.
   
   @param path The path of the SQLite Database to open.
   @param write Must be odd if you want to write in the database.
   
   @return the database opened.
   */
  
  public static SQLiteDatabase open(String path, int write) {  
    return SQLiteDatabase.openDatabase(path, cursor_factory,
                                       write_modes[(write & 1)]);
  }

  public static void
  drop_table(SQLiteDatabase database, String table_name) {
    database.execSQL(
      String.format("DROP TABLE %s;", quote(table_name))
    );
  }

  public static Column[]
  table_info(SQLiteDatabase database, String table_name) {
    Cursor table_infos = database.rawQuery(
      String.format("pragma table_info(%s)", table_name), null
    );
    char n_columns = (char) table_infos.getCount();

    Column[] columns = new Column[n_columns];

    char i = 0;
    while(table_infos.moveToNext()) {
      columns[i] =
        new Column(table_infos.getString(1), table_infos.getString(2));
      columns[i].not_null      = table_infos.getInt(3) == 1;
      columns[i].default_value = unquote(table_infos.getString(4));
      columns[i].primary_key   = (char) table_infos.getInt(5);
      columns[i].position      = i;
      ++i;
    }

    table_infos.close();

    return columns;
  }

  public static byte text_type_to_number(String provided_type) {

    String tested_type = provided_type.toUpperCase(Locale.ROOT);
    if (tested_type.contains("INT")) return INTEGER;
    if (tested_type.contains("CHAR") ||
        tested_type.contains("CLOB") ||
        tested_type.contains("TEXT")) return TEXT;
    if (tested_type.contains("BLOB") ||
        tested_type.trim().isEmpty()) return BLOB;
    if (tested_type.contains("REAL") ||
        tested_type.contains("FLOAT") ||
        tested_type.contains("DOUB")) return REAL;
    return NUMERIC;

  }
 
}
