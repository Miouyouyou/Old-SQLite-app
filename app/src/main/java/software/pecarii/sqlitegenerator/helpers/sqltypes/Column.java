package software.pecarii.sqlitegenerator.helpers.sqltypes;

import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;

import static software.pecarii.sqlitegenerator.helpers.SQLiteHelpers.quote;

public class Column {

  public static final byte INTEGER = 0;
  public static final byte TEXT    = 1;
  public static final byte BLOB    = 2;
  public static final byte REAL    = 3;
  public static final byte NUMERIC = 4;
  public static final int NOT_NULL      = 0;
  public static final int DEFAULT_VALUE = 1;
  public static final String[] types =
    new String[] { "INTEGER", "TEXT", "BLOB", "REAL", "NUMERIC" };;
  public static final String[] constraints =
    new String[] { "NOT NULL ", "DEFAULT " };

  public boolean not_null;
  public char position;
  public char primary_key;
  public byte type;
  public final String name;
  public final String quoted_name;
  public String default_value;
  public final StringBuilder constraint_builder;
  public final String str_type;

  public Column(String name, byte type) {
    this(name, types[type]);
    this.type = type;
  }
  
  public Column(String name, String type) {
    this.name          = name.intern();
    this.quoted_name   = quote(name);
    this.str_type      = type;
    this.type          = SQLiteHelpers.text_type_to_number(type);
    this.not_null      = false;
    this.default_value = null;
    this.constraint_builder = new StringBuilder(64);
  }

  public String toString() {
    return String.format("%s %s %s", 
                         this.quoted_name, this.str_type, constraints());
  }
  
  public String constraints() {
    constraint_builder.delete(0, 0x7fffffff);
    if (not_null) constraint_builder.append(constraints[NOT_NULL]);
    if (default_value != null) {
      constraint_builder.append(constraints[DEFAULT_VALUE]);
      constraint_builder.append(default_value);
    }
    return constraint_builder.toString();
  }
  
  public String type() { return this.str_type; }
}
