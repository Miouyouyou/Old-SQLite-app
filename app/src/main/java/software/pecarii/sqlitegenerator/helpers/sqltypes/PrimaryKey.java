package software.pecarii.sqlitegenerator.helpers.sqltypes;

import static software.pecarii.sqlitegenerator.helpers.StringHelpers.list;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;

public class PrimaryKey {

  public final Column[] colonnes_composantes;
  public final int positions[];
  public boolean autoincrement;

  public PrimaryKey(Column[] composantes, int[] positions) {
    this.colonnes_composantes = composantes;
    this.positions = positions;
  }
  
  public PrimaryKey(Column[] composantes) {
    this.colonnes_composantes = composantes;
    this.positions = new int[composantes.length];
    for (int i = 0; i < composantes.length; i++)
      positions[i] = i+1;
  }

  public Column[] composantes() { return colonnes_composantes; }
  
  public String toString() {
    String[] noms_colonnes = new String[this.colonnes_composantes.length];
    for (int i = 0; i < this.colonnes_composantes.length; i++)
      noms_colonnes[i] = this.colonnes_composantes[i].quoted_name;

    return String.format("PRIMARY KEY (%s)", list(",", noms_colonnes));
  }
    
}



