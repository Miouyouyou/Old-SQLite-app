package software.pecarii.sqlitegenerator.helpers;

public class ScoredObject implements Comparable<ScoredObject> {
  
  public Object o;
  public int position;

  public ScoredObject(Object o, int position) {
    this.o = o;
    this.position = position;
  }
    
  @Override
  public int compareTo(ScoredObject o) {
    return this.position - o.position;
  }
  
  
}
