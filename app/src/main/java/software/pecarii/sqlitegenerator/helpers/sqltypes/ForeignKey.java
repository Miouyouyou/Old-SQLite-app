package software.pecarii.sqlitegenerator.helpers.sqltypes;

import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;

public class ForeignKey {

  public static final String[] clauses;
  public static final String[] triggers;
  
  static {
    triggers = new String[] { "ON UPDATE ", "ON DELETE " };
    clauses = new String[] {
      "NO ACTION ",
      "SET NULL ", 
      "SET DEFAULT ", 
      "CASCADE ", 
      "RESTRICT "
    };
  }

  public Column[] child_columns;
  public int child_columns_number;
  public Column[] parent_columns;
  public String parent_name;
  public int[] set_clauses;
  public StringBuilder builder;
  public boolean future_table;
  
  public ForeignKey(Column[] childs, 
                    String parent_name, PrimaryKey parent) {
    set(childs, parent_name, parent);
    set_clauses(new int[2]);
    this.builder = new StringBuilder(50);
    this.future_table = false;
  }
  
  public void update(Column[] childs, String parent_name, 
                     PrimaryKey parent) {
    set(childs, parent_name, parent);
  }
  
  public void set(Column[] childs, String parent_name, 
                  PrimaryKey parent) {
    this.child_columns = childs;
    this.parent_columns = parent.colonnes_composantes;
    this.parent_name = parent_name.intern();
  }
  
  public void set_clauses(int[] new_clauses) {
    this.set_clauses = new_clauses;
  }
  
  /**
    Determines if the foreign key references a table that does not
    currently exist.
    
    @param state  set to true to indicate that this table references
                  a future table
  */
  public void references_future_table(boolean state) {
    this.future_table = true;
  }
  
  public String list(Column[] columns) {
    this.builder.delete(0, 65535);
    this.builder.append(columns[0].quoted_name);
    for (int i = 1; i < columns.length; i++) {
      this.builder.append(',');
      this.builder.append(columns[i].quoted_name);
    }
    return this.builder.toString();
  }
  
  public String clauses() {
    return String.format("%s %s %s %s", 
                         triggers[0], clauses[set_clauses[0]],
                         triggers[1], clauses[set_clauses[1]]);
  }
  
  public String toString() {
    return String.format("FOREIGN KEY (%s) REFERENCES %s(%s) %s",
                         list(child_columns),
                         parent_name,
                         list(parent_columns),
                         clauses());
  }
                  
      
}
