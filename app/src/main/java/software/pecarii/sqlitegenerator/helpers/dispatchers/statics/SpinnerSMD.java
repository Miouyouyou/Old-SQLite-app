package software.pecarii.sqlitegenerator.helpers.dispatchers.statics;


import android.view.View;
import android.widget.AbsSpinner;
import android.widget.AdapterView;

import java.lang.reflect.Method;

import software.pecarii.sqlitegenerator.helpers.MethodHelpers;

public class SpinnerSMD implements AdapterView.OnItemSelectedListener {
  public Method method;
  public Object target;

  public SpinnerSMD(String method_name, Object target) {
    this.target = target;
    this.method =
      MethodHelpers.get_method(method_name, target.getClass(),
                               boolean.class, AdapterView.class,
                               View.class, int.class, long.class);
  }

  @Override
  public void
  onItemSelected(AdapterView<?> a, View v, int p, long i) {
    MethodHelpers.invoke_method(method, target, true, a, v, p, i);
  }

  @Override
  public void
  onNothingSelected(AdapterView<?> a) {
    MethodHelpers.invoke_method(method, target, false, a, null, 0, 0);
  }
}
