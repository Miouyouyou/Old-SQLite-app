package software.pecarii.sqlitegenerator.helpers.dispatchers.statics;

import android.content.DialogInterface;

import java.lang.reflect.Method;

import software.pecarii.sqlitegenerator.helpers.MethodHelpers;

public class DialogMCSMD implements DialogInterface.OnMultiChoiceClickListener {
  public Method method;
  public Object target;

  public DialogMCSMD(String method_name, Object target) {
    this.target = target;
    this.method = MethodHelpers.get_method(
      method_name, target.getClass(),
      DialogInterface.class, int.class, boolean.class);
  }

  @Override
  public void
  onClick(DialogInterface dialog, int which, boolean isChecked) {
    MethodHelpers.invoke_method(method, target, dialog, which, isChecked);
  }
}
