package software.pecarii.sqlitegenerator.helpers.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import static android.view.View.OnClickListener;
import android.widget.Checkable;
import android.support.v7.widget.AppCompatTextView;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;

public class LabelledCheckbox extends AppCompatTextView implements Checkable {

  public final static int start;
  public final static int end;
  public final static int top;
  public final static int bottom;
  public final static int checked;
  public final static int unchecked;
  public final static int[] checkbox_drawable_res;
  public final static int[][] states;

  static {
    start = 0;
    end = 1;
    top = 2;
    bottom = 3;
    checked = 1;
    unchecked = 0;
    states = new int[][] {
      {-android.R.attr.state_checked},
      {android.R.attr.state_checked}
    };
    checkbox_drawable_res =
      new int[] {android.R.attr.listChoiceIndicatorMultiple};

  }

  public boolean check;
  public int check_int;
  public final int checkbox_position;
  public final Drawable[] drawable_at_position;
  public final Drawable drawable;

  public LabelledCheckbox(Context context) { this(context, 0); }
  public LabelledCheckbox(Context context, int position) {
    this(context, position, true);
  }
  public LabelledCheckbox(Context context, int position, 
                          boolean show_checkbox) {
    super(context);
    this.drawable =
      Application.drawable_depuis_attributs(checkbox_drawable_res, 0);
    this.drawable_at_position = new Drawable[4];
    this.checkbox_position = position;
    this.setClickable(true);
    this.check = false;
    this.check_int = 0;
    setOnClickListener(DefaultOnClickListener.instance);
    show_check(show_checkbox);
  }
  
  public LabelledCheckbox(Context context, AttributeSet attrSet) {
    super(context, attrSet);

    boolean show_checkbox = true;
    boolean clickable = true;

    this.drawable_at_position = new Drawable[4];
    this.drawable =
      Application.drawable_depuis_attributs(checkbox_drawable_res, 0);

    TypedArray a =
      context.obtainStyledAttributes(attrSet,
                                     R.styleable.LabelledCheckbox,0,0);
    this.checkbox_position =
      a.getInt(R.styleable.LabelledCheckbox_checkbox_position, start);
    show_checkbox =
      a.getBoolean(R.styleable.LabelledCheckbox_show_checkbox, true);
    clickable =
      a.getBoolean(R.styleable.LabelledCheckbox_clickable, true);
    a.recycle();

    // This should be parsed from the attributes, defaulting to true...
    check = false;
    check_int = unchecked;
    this.setClickable(clickable);
    if (clickable) setOnClickListener(DefaultOnClickListener.instance);
    show_check(show_checkbox);
  }
  
  public void show_check(boolean show) {
    if (show)
      this.drawable_at_position[checkbox_position] = this.drawable;
    else this.drawable_at_position[checkbox_position] = null;
    this.setCompoundDrawablesWithIntrinsicBounds(
      this.drawable_at_position[start],
      this.drawable_at_position[top],
      this.drawable_at_position[end],
      this.drawable_at_position[bottom]
    );
  }

  @Override
  public boolean isChecked() { return this.check; }

  @Override
  public void setChecked(boolean new_check) {
    this.check = new_check;
    if (new_check) this.check_int = checked;
    else this.check_int = unchecked;
    refreshDrawableState();
  }

  @Override
  public void toggle() { setChecked(!this.check); }

  @Override
  public boolean performClick() { return super.performClick(); }

  @Override
  public boolean callOnClick() { return super.callOnClick(); }

  @Override
  protected int[] onCreateDrawableState(int space) {
    final int[] drawable_state = super.onCreateDrawableState(space + 1);
    mergeDrawableStates(drawable_state, states[check_int]);
    return drawable_state;
  }

  public static class DefaultOnClickListener 
    implements OnClickListener {
    public static final DefaultOnClickListener instance;
    static { instance = new DefaultOnClickListener(); }
    
    private DefaultOnClickListener() { }
    
    @Override
    public void onClick(View v) { ((LabelledCheckbox) v).toggle(); }

  }
}
