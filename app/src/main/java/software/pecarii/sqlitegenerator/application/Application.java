package software.pecarii.sqlitegenerator.application;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.ads.Eticilbup;

public final class Application extends MultiDexApplication {
  private static final int ressource_theme = R.style.MyTheme;
  private static Context app_contexte;
  private static DisplayMetrics metriques;
  private static Theme theme;

  public static byte refCounter = 0;

  public static void addRef() {}

  public static void removeRef() {}

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override
  public void onCreate() {
    super.onCreate();
    app_contexte = getApplicationContext();
    app_contexte.setTheme(ressource_theme);
    theme = app_contexte.getTheme();
    metriques = app_contexte.getResources().getDisplayMetrics();
    Eticilbup.preps(this);
  }

  public static void watch(Object o) {}

  public static Context contexte() { return app_contexte; }
  public static Theme theme() { return theme; }
  public static Drawable
  drawable_depuis_attributs(int[] attributs, int position_drawable) {
    final Drawable returned_drawable;
    final TypedArray ta = theme().obtainStyledAttributes(attributs);
    returned_drawable = ta.getDrawable(position_drawable);
    ta.recycle(); // <- La verbosité de cette méthode est due à ça !
    return returned_drawable;
  }

  public static DisplayMetrics metriques() { return metriques; }

}

