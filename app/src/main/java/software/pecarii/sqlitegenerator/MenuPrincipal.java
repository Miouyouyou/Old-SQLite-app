package software.pecarii.sqlitegenerator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MenuPrincipal extends AppCompatActivity
{
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }
}
