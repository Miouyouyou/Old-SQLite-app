package software.pecarii.sqlitegenerator;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Map;

import software.pecarii.sqlitegenerator.MenuPrincipal;
import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.ConsoleSQL;
import software.pecarii.sqlitegenerator.fragments.ToolbarFragment;
import software.pecarii.sqlitegenerator.fragments.VueSchema;
import software.pecarii.sqlitegenerator.fragments.VueTable;
import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;

import software.pecarii.sqlitegenerator.helpers.StringHelpers;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.helpers.sqltypes.PrimaryKey;
import software.pecarii.sqlitegenerator.fragments.SelectionTables;
import software.pecarii.sqlitegenerator.fragments.Wizard;
import software.pecarii.sqlitegenerator.fragments.WizardCaller;
import software.pecarii.sqlitegenerator.fragments.WizardPage;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;

/**
 Cette activité gère la base de données manipulée par tous ses sous
 fragments.
 
 Les sous-fragments étant, actuellement :
 - La vue des tables
 - La vue du schéma de la table sélectionnée
 - La vue des données de la table sélectionnée
 
 Cette gestion centralisée permet de libérer les ressources aisément,
 et de les récupérer tout aussi facilement, ce qui n'est pas aussi
 facile avec des activités séparées.
 
 Le problème des activités séparées étant qu'il faut pouvoir gérer deux
 cas :
 - La mise en pause de l'application
 - Le passage à une autre vue
 
 La mise en pause étant, par exemple, l'appui sur le bouton 'Home' pour
 retourner à l'écran principal d'Android.
 
 Le démarrage de l'applicatif entraine l'exécution de onCreate()
 Le passage à une autre vue entraine l'exécution de onCreate() 
 sur l'autre vue.
 La mise en pause de l'applicatif entraine l'exécution de onStop().
 Le passage à une autre vue entraine aussi l'exécution de onStop() sur
 la vue actuelle !
 
 Libérer les données via SQLiteDatabase#close() retire toutes les
 références à la base de données représentée par cette instance.
 
 Il devient alors compliquer de passer les références à la BDD entre
 les différentes vues, tout en s'assurant de bien libérer les 
 ressources lorsque l'utilisateur quitte l'applicatif...
 Le seul moyen est alors de réouvrir la base de données dans chaque vue
 et de la refermer lors du passage à une autre vue...
 
 Avec l'utilisation de différents fragments, c'est différent.
 L'activité est unique. On peut donc allouer les ressources lorsque
 l'utilisateur démarre l'activité et libérer les ressources lorsqu'il
 la quitte.
 
 Les fragments, eux, contacteront l'activité pour obtenir les 
 informations nécessaires depuis la base de donnée allouée. Cependant,
 aucune référence à la base de données n'est passée aux fragments.
 
 Ainsi cette activité gère :
 - l'allocation des ressources,
 - la gestion de la base de données;
 et fournit les méthodes nécessaires pour communiquer avec la base de
 données.
 
 Une troisième option serait de libérer la mémoire utilisée par la base
 de données tant qu'elle n'est pas utilisée et la réallouer dès qu'une 
 action doit être effectuée. Cependant, l'impossibilité d'allouer la 
 mémoire nécessaire à la base de données au moment opportun est un 
 problème qui ne peut être ignoré...
 
 */
public class ManipulationBDD extends AppCompatActivity {

  public final static int VUE_TABLES   = 0;
  public final static int VUE_SCHEMA   = 1;
  public final static int WIZARD_TABLE = 0;
  public final static String nom_intention = 
    "software.pecarii.sqlitegenerator.fragments.SelectionFichiers";
  public final static String nom_backstack = "start";
  public final static ContentValues parametres_requete =
    new ContentValues(5);
  
  public final static Intent main;

  static { 
    main = new Intent(Application.contexte(), MenuPrincipal.class); 
  }

  public int fragment_actuel; 
  public int lecture_ecriture; // 0 : RO - 1 : RW
  public int[] resolution;
  public boolean fragment_ajouté;
  public StringBuilder sb;
  public FragmentManager gestionnaire_fragments;
  public ToolbarFragment[] fragments;
  public SQLiteDatabase bdd;
  public String chemin_bdd;
  public String table_editee;
  public String[] tables_bdd;
  public Wizard assistant_actuel;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(null);

    this.resolution               = ViewHelpers.resolution(this);
    this.fragment_ajouté          = false;
    this.lecture_ecriture         = 1;
    this.fragment_actuel          = VUE_TABLES;
    this.assistant_actuel         = null;
    this.gestionnaire_fragments   = getSupportFragmentManager();
    this.fragments                = new ToolbarFragment[] {
      new SelectionTables(), new VueTable()
    };
    this.chemin_bdd               = getIntent().getStringExtra(nom_intention);
    this.sb                       = new StringBuilder(64);
    if (savedInstanceState != null) { 
      /* L'activité est entrain d'être restaurée... */
      if (this.chemin_bdd != null);
      else {
        this.chemin_bdd = 
          savedInstanceState.getString("chemin_bdd", "?!");
      }
    }
    obtenir_bdd();
    Application.addRef();
  }

  public void retourner_a_ecran_principal() {
    //if (gestionnaire_fragments.getBackStackEntryCount() != 0)
      finish();
    //else startActivity(main);
  }

  public void obtenir_bdd() {
    try {
      this.bdd = SQLiteHelpers.open(this.chemin_bdd, lecture_ecriture);
    }
    catch (RuntimeException e) {
      Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
      retourner_a_ecran_principal();
    }
  }

  @Override
  public void onRestart() {
    obtenir_bdd();
    super.onRestart();
  }

  @Override
  public void onStart() {
    super.onStart();
    if (!fragment_ajouté) {
      charge_fragment(fragment_actuel);
      fragment_ajouté = true;
    }
  }
  
  @Override
  public void onStop() {
    this.bdd.close();
    this.bdd = null;
    super.onStop();
  }

  @Override
  public void onDestroy() {
    Application.removeRef();
    for (ToolbarFragment fragment : fragments) Application.watch(fragment);
    super.onDestroy();
  }

  @Override
  public void onSaveInstanceState(Bundle etat_sauvegarde) {
    etat_sauvegarde.putString("chemin_bdd", this.chemin_bdd);
    etat_sauvegarde.putBoolean("fragment_charge", true);
    super.onSaveInstanceState(etat_sauvegarde);
  }

  public void demarrer_creation_table() {
    this.assistant_actuel =
      new CreationTable(this.gestionnaire_fragments,
                        new WizardCallBack());
    ((CreationTable) this.assistant_actuel).noms_tables =
      liste_nom_tables();
    ((CreationTable) this.assistant_actuel).pks_tables =
      liste_clefs_primaires();
    
    this.assistant_actuel.start();
  }

  /*public void afficher_console_sql() {
    charge_fragment(VUE_CONSOLE, null);
  }*/

  public void executer_sql(CharSequence text) {
    String requete = text.toString();
  }

  public void charge_fragment(int fragment) {
    this.
      gestionnaire_fragments.
      beginTransaction().
      replace(android.R.id.content, this.fragments[fragment]).
      commit();
  }

  public void charge_fragment(int fragment, String nom_transaction) {
    this.
      gestionnaire_fragments.
      beginTransaction().
      addToBackStack(nom_transaction).
      replace(android.R.id.content, this.fragments[fragment], "start").
      commit();
  }
  
  public String[] liste_nom_tables() {
    this.tables_bdd = SQLiteHelpers.list_tables_names_in(this.bdd);
    return this.tables_bdd;
  }
  
  public PrimaryKey[] liste_clefs_primaires(String[] tables) {
    return SQLiteHelpers.all_primary_keys_in(this.bdd, tables);
  }
  
  public PrimaryKey[] liste_clefs_primaires() {
    return SQLiteHelpers.all_primary_keys_in(this.bdd, 
                                             liste_nom_tables());
  }

  public void creer_table(String schema) {
    this.bdd.execSQL(schema);
  }

  public void supprimer_tables(String[] tables, boolean[] tables_cochees) {
    for (char i = 0; i < tables.length; i++) {
      try {
        if(tables_cochees[i])
          SQLiteHelpers.drop_table(this.bdd, tables[i]);
      }
      catch (android.database.sqlite.SQLiteException e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
      }
    }
  }

  public void interface_liste_tables() {
    charge_fragment(VUE_TABLES, null);
  }

  public Column[] informations_table(String table) {
    return SQLiteHelpers.table_info(this.bdd, SQLiteHelpers.quote(table));
  }

  public void inserer_nouvelle_ligne(ContentValues parametres_requete) {
    this.bdd.insertOrThrow(table_editee, null, parametres_requete);
  }

  public void supprimer_ligne(long id) {
    this.bdd.delete(table_editee, "rowid = ?", new String[] { String.valueOf(id) });
  }

  public class WizardCallBack implements WizardCaller {
    @Override
    public void end(int code) { assistant_actuel = null; }
  }

  public String schema_table_actuelle() {
    return SQLiteHelpers.schema_of(table_editee, this.bdd);
  }

  public void afficher_schema_table(String table) {
    this.table_editee = table;
    charge_fragment(VUE_SCHEMA, null);
  }

  public Cursor enregistrements(String table, long depart, int limite) {

    String requete;
    synchronized (ManipulationBDD.class) {
      sb.delete(0, 0x7fffffff);
      sb.append("SELECT rowid,* FROM ");
      sb.append(SQLiteHelpers.quote(table));
      sb.append(" ORDER BY rowid ASC");
      sb.append(" LIMIT ");
      sb.append(limite);
      sb.append(" OFFSET ");
      sb.append(depart);

      requete = sb.toString();
    }
    System.out.println(requete);
    return this.bdd.rawQuery(requete, null);
  }

  public void update_table_actuelle(String row, ContentValues valeurs) {
    int updated = bdd.update(this.table_editee, valeurs, "rowid = ?", new String[] { row });
    /*System.err.println(updated);*/
  }

  public void update_table_actuelle(String row, String colonne, String valeur) {
    String requete = null;
    synchronized (ManipulationBDD.class) {
      sb.delete(0, 0x7fffffff);
      sb.append("UPDATE ");
      sb.append(SQLiteHelpers.quote(this.table_editee));
      sb.append(" SET ");
      sb.append(SQLiteHelpers.quote(colonne));
      sb.append("=");
      sb.append(valeur);
      sb.append(" WHERE rowid=");
      sb.append(row);
      requete = sb.toString();
    }
    /*System.err.println(requete);*/
    bdd.execSQL(requete);
  }

}
