package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import static android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.lang.ref.WeakReference;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import static software.pecarii.sqlitegenerator.helpers.SQLiteHelpers.quote;
import static software.pecarii.sqlitegenerator.helpers.ViewHelpers.set_entries_of;
import static software.pecarii.sqlitegenerator.helpers.ViewHelpers.standard_spinner_adapter_with;

public final class ContrainteCheck extends LinearLayout {

  static public byte[][] affichages;
  static public byte col_gauche;
  static public byte col_operateur;
  static public byte col_droite;
  static public byte zone_droite;
  static public byte zone_manuelle;
  static public byte gone;
  static public byte visible;
  static public byte mode_col_texte;
  static public byte mode_col_col;
  static public byte mode_manuel;
  static public int[] modes_editions;
  
  static {
    gone          = (byte) View.GONE;
    visible       = (byte) View.VISIBLE;
    col_gauche    = 0;
    col_operateur = 1;
    col_droite    = 2;
    zone_droite   = 3;
    zone_manuelle = 4;
    affichages = new byte[][] {
     {visible, visible, gone,    visible, gone},
     {visible, visible, visible, gone,    gone},
     {gone,    gone,    gone,    gone,    visible}
    };
    modes_editions = CreationTable.modes_editions;
    mode_col_texte = 0;
    mode_col_col   = 1;
    mode_manuel    = 2;
  }

  public int mode_actuel;
  public Spinner choix_mode;
  public CreationTable assistant;
  public final View[] vues;
  public byte[] types_colonnes;
  
  public ContrainteCheck(Context contexte) { 
    super(contexte);
    this.vues = new View[5]; 
    parametrage_ressources();
                   
  }
  public ContrainteCheck(Context contexte, AttributeSet attributs) {
    super(contexte, attributs);
    this.vues = new View[5]; 
  }
  
  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    parametrage_ressources();
  }
  
  public void parametrage_ressources() {
    
    LayoutInflater.from(Application.contexte())
      .inflate(R.layout.vue_contrainte_check, this, true);
    this.choix_mode =
      (Spinner) findViewById(R.id.choix_mode_saisie_check);
    vues[col_gauche]    = findViewById(R.id.choix_colonne_check);
    vues[col_operateur] = findViewById(R.id.choix_operateur_check);
    vues[col_droite]    = findViewById(R.id.choix_seconde_colonne_check);
    vues[zone_droite]   = findViewById(R.id.zone_saisie_assistee_check);
    vues[zone_manuelle] = findViewById(R.id.zone_saisie_manuelle_check);
    
    set_entries_of((Spinner) vues[col_operateur],
                   CheckConstraint.operateurs);
    
    choix_mode.setOnItemSelectedListener(new ChoixMode());
    ((Spinner) this.vues[col_gauche]).setOnItemSelectedListener(new ChoixColonneGauche());

  }
  
  public void assistant(CreationTable wizard) {
    this.assistant = wizard;
    preparations_avec_assistant();
  }
  
  public void preparations_avec_assistant() {
    this.types_colonnes = assistant.types_colonnes_ajoutees;
    ((Spinner) this.vues[col_gauche]).
      setAdapter(assistant.adapter_noms_colonnes_ajoutees);
    ((Spinner) this.vues[col_droite]).
      setAdapter(assistant.adapter_noms_colonnes_ajoutees);
  }
    
  public void reset() {
    mode(mode_col_texte);
    ((Spinner) this.vues[col_gauche]).setSelection(0);
    ((Spinner) this.vues[col_operateur]).setSelection(0);
  }
  
  public class ChoixMode implements OnItemSelectedListener {
    @Override
    public void onItemSelected(AdapterView<?> parent, View v, 
                               int pos, long id) { mode(pos); }
    @Override
    public void onNothingSelected(AdapterView<?> a) {}
  }
  
  public class ChoixColonneGauche implements OnItemSelectedListener {
    @Override
    public void onItemSelected(AdapterView<?> parent, View v,
                               int pos, long id) {
      ((EditText) vues[zone_droite]).setInputType(
        modes_editions[types_colonnes[pos]]
      );
    }
    @Override
    public void onNothingSelected(AdapterView<?> a) {}
  }
  
  public void mode(int mode) {
    this.mode_actuel = mode;
    for (int i = 0; i < 5; i++) {
      vues[i].setVisibility(affichages[mode][i]);
    }
  }
  
  public void afficher(CheckConstraint contrainte) {
    mode(contrainte.mode);
    if (contrainte.mode != mode_manuel) {
      ((Spinner) this.vues[col_gauche]).
        setSelection(contrainte.position_colonne_gauche);
      ((Spinner) this.vues[col_operateur]).
        setSelection(contrainte.operateur_selectionne);
      if (contrainte.mode == mode_col_texte)
        ((EditText) this.vues[zone_droite]).setText(contrainte.texte);
      else
        ((Spinner) this.vues[col_droite]).
          setSelection(contrainte.position_colonne_droite);
    }
    else 
      ((EditText) this.vues[zone_manuelle]).setText(contrainte.texte);
  }
  
  public CheckConstraint generer_objet() {
    
    int operateur_selectionne =
      ((Spinner) this.vues[col_operateur]).getSelectedItemPosition();

    switch(this.mode_actuel) {
      case 0:
        return new CheckConstraint(this.mode_actuel,
                                   this.assistant.nom_de_la_table,
                                   (Spinner) this.vues[col_gauche], 
                                   operateur_selectionne, 
                                   ((EditText) this.vues[zone_droite]).
                                                getText().toString()
                                  );
      case 1: 
        return new CheckConstraint(this.mode_actuel,
                                   this.assistant.nom_de_la_table,
                                   (Spinner) this.vues[col_gauche], 
                                   operateur_selectionne, 
                                   (Spinner) this.vues[col_droite]
                                  );
      case 2:
        return new CheckConstraint(this.mode_actuel, 
                                   ((EditText) this.vues[zone_manuelle]).
                                                getText().toString()
                                  );
    }
    return null;
  }
  
  public static class CheckConstraint {
  
    public static String[] operateurs;
    
    static {
      operateurs = new String[] { "=", "!=", ">", ">=", "<", "<=" };
    }
  
    public int mode;
    public int operateur_selectionne;
    public int position_colonne_gauche;
    public int position_colonne_droite;   
    public String colonne_gauche;   
    public String texte;
    public String nom_table;
    
    public CheckConstraint(int mode, String nom_table,
                           Spinner colonne_gauche,
                           int operateur, Spinner colonne_droite) {
      this.mode                    = mode;
      this.operateur_selectionne   = operateur;
      this.nom_table               = nom_table;
      
      this.position_colonne_gauche = 
        colonne_gauche.getSelectedItemPosition();
      this.position_colonne_droite  =
        colonne_droite.getSelectedItemPosition();
      this.colonne_gauche = (String) colonne_gauche.getSelectedItem();
      this.texte          = (String) colonne_droite.getSelectedItem();
      
    }
    
    public CheckConstraint(int mode, 
                           String nom_table,
                           Spinner colonne_gauche,
                           int operateur, String texte_zone_droite) {
      this.mode                    = mode;
      this.operateur_selectionne   = operateur;
      this.nom_table               = nom_table;
      this.texte                   = texte_zone_droite;
      this.position_colonne_gauche = 
        colonne_gauche.getSelectedItemPosition();
      this.colonne_gauche = (String) colonne_gauche.getSelectedItem();      
                          
    }
     
    public CheckConstraint(int mode, String texte) {
      this.mode           = mode;
      this.texte          = texte;
      this.colonne_gauche = null;
    }                      
    
    public String toString() {
      if (mode != 2)
        return String.format("CHECK (%s.%s %s %s)",
                             quote(this.nom_table),
                             quote(this.colonne_gauche),
                             operateurs[this.operateur_selectionne],
                             quote(this.texte));
      return String.format("CHECK (%s)", this.texte);
    }
    
  }
}
