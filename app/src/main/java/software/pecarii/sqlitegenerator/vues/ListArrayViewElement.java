package software.pecarii.sqlitegenerator.vues;

public interface ListArrayViewElement<T> {
  void display(T element);
}

