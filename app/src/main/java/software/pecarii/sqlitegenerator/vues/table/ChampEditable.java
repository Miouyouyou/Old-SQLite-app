package software.pecarii.sqlitegenerator.vues.table;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.widget.EditText;

import java.lang.ref.WeakReference;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;

public final class ChampEditable extends EditText {

  public static final int[] fonds_colonnes =
    new int[] { 0xffffffff, 0xfff2f2f2 };
  public static final int fond_warning = 0x77ff0000;

  public static final InputFilter[] aucun_filtre = new InputFilter[0];
  public static WeakReference<Callback> callback_saisie;
  public static void callback_saisie(Callback nouvel_interlocuteur) {
    callback_saisie= new WeakReference<Callback>(nouvel_interlocuteur);
  }

 /* static {
    padding = (int) TypedValue.applyDimension(
      TypedValue.COMPLEX_UNIT_DIP, 3,
      Application.contexte().getResources().getDisplayMetrics()
    );
  }*/

  public boolean valeur_null;
  public boolean nouveau;
  public int fond_initial;
  public int fond_colonne;
  public String valeur_initiale;

  public final PremiereModification[] premiere_modification;

  public Column colonne;

  public ChampEditable(Context context) {
    super(context);
    premiere_modification = new PremiereModification[] {
      new PremiereModification()
    };
  }
  public ChampEditable(Context context, AttributeSet attrs) {
    super(context, attrs);
    premiere_modification = new PremiereModification[] {
      new PremiereModification()
    };
  }
  public ChampEditable(Context context, AttributeSet attrs, int style) {
    super(context, attrs, style);
    premiere_modification = new PremiereModification[] {
      new PremiereModification()
    };
  }

  public void colonne(Column colonne) {
    this.colonne = colonne;
    this.fond_colonne = fonds_colonnes[colonne.position % 2];
    this.fond_initial = fond_colonne;
  }

  public void set_null() {
    setHint("NULL");
    setText("");
    valeur_null = true;
    setFilters(premiere_modification);
  }

  public void reinitialiser() {
    valeur_initiale(valeur_initiale);
  }

  public void valeur_initiale(String valeur) {
    this.valeur_initiale = valeur;
    if (valeur != null) this.setText(valeur);
    else {
      valeur_null = true;
      this.setHint("NULL");
      if (!colonne.not_null); else fond_initial = fond_warning;
    }
    setBackgroundColor(fond_initial);
    setFilters(premiere_modification);
  }
  public String valeur() {
    if (!valeur_null) return this.getText().toString();
    else return null;
  }

  public class PremiereModification implements InputFilter {
    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {
      setHint("");
      setFilters(aucun_filtre);
      valeur_null = false;
      setBackgroundColor(fond_colonne);
      return null;
    }
  }

  @Override
  public boolean onKeyDown(int touche, KeyEvent evenement) {
    if (touche == KeyEvent.KEYCODE_ENTER) {
      if (!retour_chariot_force_en_mode_texte(evenement)) return true;
      else
        return super.onKeyDown(touche, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
    }
    return super.onKeyDown(touche, evenement);
  }

  @Override
  public boolean onKeyUp(int touche, KeyEvent evenement) {
    if (touche == KeyEvent.KEYCODE_ENTER || touche == KeyEvent.KEYCODE_TAB) {

      if (!retour_chariot_force_en_mode_texte(evenement)) {
        ViewHelpers.move_focus(this, FOCUS_FORWARD);
        return true;
      }
      evenement =
        new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER);
    }
    return super.onKeyUp(touche, evenement);
  }

  public boolean mode_texte() {
    return (this.getInputType() & InputType.TYPE_CLASS_TEXT)
            == InputType.TYPE_CLASS_TEXT;
  }

  /* L'objectif de cette méthode est de pouvoir déterminer si
     l'utilisateur est entrain d'éditer une zone de texte, avec un
     clavier physique, et qu'il essaye de saisir un retour chariot de
     force, étant donné que la touche "Entrée" est liée à
     Callback#champ_saisi .
     Le seul problème est que ça ne fonctionne pas sous l'émulateur...
     Les touches Alt, Super, Shift et Ctrl ne sont jamais pris en compte...
     Difficile de tester tout ça...
   */
  public boolean retour_chariot_force_en_mode_texte(KeyEvent evenement) {
    /*boolean mode_texte = mode_texte();
    boolean alt = evenement.isAltPressed();
    boolean shift = evenement.isShiftPressed();
    boolean resultat = (mode_texte && (alt || shift));
    return resultat;*/
    return (mode_texte() &&
            (evenement.isAltPressed() || evenement.isShiftPressed())
           );
  }

  public interface Callback {
    void champ_saisi(ChampEditable champ);
  }

}