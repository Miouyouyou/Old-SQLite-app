package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.vues.ListArrayViewElement;

public class NouvelleColonne extends TextView
  implements ListArrayViewElement<Column>, Checkable {
  
  public final static String format_colonne;
  public final static int[] icones_checkbox;
  
  static { 
    format_colonne = "%s %s";
    icones_checkbox = new int[] {
      android.R.drawable.checkbox_off_background,
      android.R.drawable.checkbox_on_background };
  }
  
  public boolean coche;
  public int coche_int;
  
  public NouvelleColonne(Context contexte) { 
    super(contexte);
    this.setCompoundDrawablesWithIntrinsicBounds(0, 0, 
                                                 icones_checkbox[coche_int],
                                                 0);
    this.coche = false;
  }

  public NouvelleColonne(Context contexte, AttributeSet jeu_attributs) 
  { 
    super(contexte, jeu_attributs); 
    this.setCompoundDrawablesWithIntrinsicBounds(0, 0, 
                                                 icones_checkbox[coche_int],
                                                 0);
    this.coche = false;
  }
  
  @Override
  public void display(Column colonne) {
    this.setText(
      String.format(format_colonne, colonne.name, colonne.type())
    );
  }
  
  @Override
  public String toString() { return this.getText().toString(); }

  @Override
  public boolean isChecked() { return this.coche; }
  
  @Override
  public void setChecked(boolean check) { 
    this.coche = check;
    if (check) this.coche_int = 1; else this.coche_int = 0;
    this.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                                 icones_checkbox[coche_int],
                                                 0);
  }
  
  @Override
  public void toggle() { setChecked(!this.coche); }
  
}
