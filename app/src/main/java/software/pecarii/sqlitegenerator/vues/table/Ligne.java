package software.pecarii.sqlitegenerator.vues.table;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TableRow;

public final class Ligne extends TableRow {

  /*
     Les marges réglées via le fichier XML n'ont aucun effet mais
     celles-ci fonctionnent...
     En attendant de trouver une méthode pour rendre les marges
     indiquées dans les fichiers XML fonctionnelles, ce hack restera
  */
  public final static TableRow.LayoutParams hack_marges =
    new TableRow.LayoutParams(
      TableRow.LayoutParams.MATCH_PARENT,
      TableRow.LayoutParams.MATCH_PARENT
    );
  //static { hack_marges.setMargins(0,0,1,0); }

  public long row_id;
  public int position;
  public Ligne(Context contexte) { super(contexte); }
  public Ligne(Context contexte, AttributeSet attributeSet) {
    super(contexte, attributeSet);
  }
  public void ajouter(View vue) {
    vue.setLayoutParams(hack_marges);
    this.addView(vue);
  }
}