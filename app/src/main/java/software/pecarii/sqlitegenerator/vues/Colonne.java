package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.vues.ListArrayViewElement;

public class Colonne extends TextView
  implements ListArrayViewElement<Column> {
  public Colonne(Context contexte) { super(contexte); }
  public Colonne(Context contexte, AttributeSet jeu_attributs) {
    super(contexte, jeu_attributs);
  }
  
  public void display(Column col) { this.setText(col.toString()); }
}
