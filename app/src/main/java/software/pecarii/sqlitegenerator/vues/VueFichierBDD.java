package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;

import static software.pecarii.sqlitegenerator.helpers.FileHelpers.readable_filesize;
import software.pecarii.sqlitegenerator.R;

public final class VueFichierBDD extends RecyclerView.ViewHolder {

  public LinearLayout conteneur;
  public TextView zone_nom_fichier;
  public TextView zone_taille_fichier;
  public TextView zone_chemin_fichier;

  /* Not inflated version */
  public VueFichierBDD(View v) {
    super(v);
    this.conteneur = (LinearLayout) v;
    this.zone_nom_fichier =
      (TextView) v.findViewById(R.id.nom_du_fichier);
    this.zone_taille_fichier =
      (TextView) v.findViewById(R.id.taille_du_fichier);
    this.zone_chemin_fichier =
      (TextView) v.findViewById(R.id.chemin_du_fichier);
  }

  public void concernant(File fichier_bdd) {
    this.conteneur.setTag(fichier_bdd);
    this.zone_nom_fichier.setText(fichier_bdd.getName());
    this.zone_taille_fichier.setText(readable_filesize(fichier_bdd.length()));
    this.zone_chemin_fichier.setText(fichier_bdd.getAbsolutePath());
  }

}

