package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.vues.ListArrayViewElement;

public class VueTableBDD extends LinearLayout 
  implements ListArrayViewElement<String> {
  
  public TextView zone_nom_table;
  
  public VueTableBDD(Context contexte) { super(contexte); }
  public VueTableBDD(Context contexte, AttributeSet jeu_attributs) {
    super(contexte, jeu_attributs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.zone_nom_table = 
      (TextView) findViewById(R.id.nom_de_la_table);    
  }
  
  @Override
  public void display(String table) {
    this.concernant(table);
  }
  
  public void concernant(String table) {
    this.zone_nom_table.setText(table);
  }
  
  public String nom_de_la_table() {
    return (String) this.zone_nom_table.getText();
  }
}
