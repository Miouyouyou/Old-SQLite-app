package software.pecarii.sqlitegenerator.vues;
/*
import android.app.Context;
import android.database.Cursor;

import java.lang.ref.WeakReference;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;

public class CursorLayout {

  public final static int[] modes_editions = {
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED,
    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE,
    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE,
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED,
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED
  };
  public final static String clause_where = "rowid = ?";
  
  public static final Method[] methodes_recuperation;
  public static final MethodeAffichage[] creations_affichages;
  
  static {
    blob = new TextView(Application.contexte());
    blob.setText("BLOB");
    try {
      methodes_recuperation = new Method[] {
        Cursor.class.getMethod("getInt",    int.class),
        Cursor.class.getMethod("getString", int.class),
        Cursor.class.getMethod("getBlob",   int.class),
        Cursor.class.getMethod("getDouble", int.class),
        Cursor.class.getMethod("getFloat",  int.class)
      };
      
      affichages_et_arguments = new MethodeAffichage[] {
        new MethodeAffichage("vue_standard", 0),
        new MethodeAffichage("vue_standard", 1),
        new MethodeAffichage("blob",         2),
        new MethodeAffichage("vue_standard", 3),
        new MethodeAffichage("vue_standard", 4)
      }
    }
    catch (NoSuchMethodException e) { throw new RuntimeException(e); }
  }
  
  public final class MethodeAffichage throws NoSuchMethodException {
    public Method creation_vue;
    public byte type;
    public MethodeAffichage(String method_name, byte type) {
      CursorLayout.class.getMethod(method_name, byte.class, CharSequence.class);
    }
    public TextView affiche_contenu(CharSequence contenu)
      throws (IllegalAccessException | InvocationTargetException |
              ExceptionInInitializerError) {
      creation_vue.invoke(null, type, contenu);
    }    
  }
  
  byte[] types;
  WeakReference<SQLiteDatabase> database;
  String table;
  Context contexte;
  Method[] recuperation;

  public DatabaseLayout(Context contexte, SQLiteDatabase database,
                        String table) {
    this.table    = table;
    this.database = new WeakReference<String> database;
    this.types    = SQLiteHelpers.columns_types(database, table);
    this.contexte = contexte;
    this.recuperation = new Method[types.length];
    this.affichages = new Object[types.length][2];
    generer_methodes();

  }

  
  void recuperer_methodes() {
    for (char i = 0, n_types = this.types.length; i < n_types; ++i) {
      this.recuperation[i] = methodes_recuperation[this.types[i]];
    }
  }
  
  SQLiteDatabase database() {
    return database.get();
  }

  static TextView blob(byte not_used, CharSequence not_used_too) {
    TextView blob = new TextView(contexte);
    blob.setText("BLOB");
    return blob;
  }

  static EditText vue_standard(byte type, CharSequence contenu) {
    TextView vue = new TextView(contexte);
    if (contenu != null) vue.setText(contenu);
    vue.setInputType(modes_editions[type]);
    return vue;
  }

  public class Ligne extends TableRow {
    public int row_id;
    public TableRow(Context context) { super(context); }
    public TableRow(Context context, AttributeSet attrs) { super(context, attrs); }
  }

  public int generer_update(TextView view) {
    String tag = view.getTag();
    switch (tag) {
    case "BLOB":
    case null: return -1;
    default:
      
    }
  }
  
  
}
*/