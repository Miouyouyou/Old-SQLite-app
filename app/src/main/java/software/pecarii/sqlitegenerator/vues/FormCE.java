package software.pecarii.sqlitegenerator.vues;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import software.pecarii.sqlitegenerator.R;

import static software.pecarii.sqlitegenerator.helpers.ViewHelpers.set_entries_of;

public class FormCE extends LinearLayout {

  public final static LinearLayout.LayoutParams params_spinner_enfants =
    new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 4);

  public Spinner choix_colonne_enfant;
  public Spinner choix_colonne_parente;
  public EditText zone_colonne_parente;

  public static FormCE base(LayoutInflater inflater, Spinner enfants) {
    FormCE formulaire = (FormCE)
      inflater.inflate(R.layout.liste_formulaire_clef_etrangere_element,
                       null, false);
    formulaire.addView(enfants, 0, params_spinner_enfants);
    formulaire.choix_colonne_enfant = enfants;
    return formulaire;    
  }

  // new FormCE(main_inflater, wizard.colonnes)
  public static FormCE pour(LayoutInflater inflater, Spinner enfants) {
    FormCE formulaire = base(inflater, enfants);
    formulaire.zone_colonne_parente.setVisibility(View.VISIBLE);
    /*System.err.println("Called ie");*/
    return formulaire;
  }

  //  new FormCE(main_inflater, wizard.colonnes,
  //             fk.enfants[i], fk.parents[i])
  public static FormCE pour(LayoutInflater inflater, Spinner enfants,
                            String parent) {
    FormCE formulaire = pour(inflater, enfants);
    /*System.err.println("called iep");*/
    formulaire.zone_colonne_parente.setText(parent);
    return formulaire;
  }

  // new FormCE(main_inflater, colonnes, parents
  //             fk.enfants[i], fk.parents[i])
  public static FormCE pour(LayoutInflater inflater, Spinner enfants,
                            String[] parents, char parent) {
    FormCE formulaire = base(inflater, enfants);
    /*System.err.println("Called iepp");*/
    set_entries_of(formulaire.choix_colonne_parente, parents);
    formulaire.choix_colonne_parente.setSelection(parent);
    formulaire.choix_colonne_parente.setVisibility(View.VISIBLE);
    return formulaire;
  }

  public FormCE(Context contexte) { 
    super(contexte);
    this.setSaveEnabled(false);
  } 
  public FormCE(Context contexte, AttributeSet attributs) {
    super(contexte, attributs);
    this.setSaveEnabled(false);
  }
   
  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    choix_colonne_parente =
      (Spinner) this.findViewById(R.id.choix_colonne_parente);
    zone_colonne_parente =
      (EditText) this.findViewById(R.id.zone_colonne_parente);
  }

  public char enfant_selectionne() {
    return (char) choix_colonne_enfant.getSelectedItemPosition();
  }
  
  public char parent_selectionne() {
    return (char) choix_colonne_parente.getSelectedItemPosition();
  }

  public String parent() {
    return zone_colonne_parente.getText().toString();
  }
  
}
