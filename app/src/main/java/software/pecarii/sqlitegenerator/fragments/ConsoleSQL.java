package software.pecarii.sqlitegenerator.fragments;

import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ViewSMD;

public class ConsoleSQL extends ToolbarFragment {

  public ArrayAdapter<CharSequence> affichage_resultats;
  public Button bouton_executer_sql;
  public EditText zone_sql;
  public ListView liste_sortie;
  public View.OnClickListener requete_dispatcher;
  public CharSequence requete;
  public CharSequence resultat_requete;

  public ConsoleSQL() {
    this.requete_dispatcher  = new ViewSMD("exec_sql", this);
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater gonfleur) {
    inflate_in_content(R.layout.fragment_console_sql);

    this.liste_sortie =
      (ListView) find_in_content(R.id.resultats_requetes_sql);
    this.zone_sql =
      (EditText) find_in_content(R.id.zone_requete_sql);
    this.bouton_executer_sql =
      (Button) find_in_content(R.id.bouton_executer_sql);
  }

  @Override
  public void setup_content_views() {
    this.affichage_resultats =
      new ArrayAdapter<CharSequence>(
        getActivity(),
        R.layout.abc_list_menu_item_layout,
        new ArrayList<CharSequence>(50)
      );
    this.bouton_executer_sql.setOnClickListener(requete_dispatcher);
    this.liste_sortie.setAdapter(this.affichage_resultats);
  }

  public void exec_sql(View v) {
    this.requete =
      ViewHelpers.span_text(
        this.zone_sql.getText(),
        new TextAppearanceSpan(getActivity(), R.style.texte_attribut)
      );
    this.resultat_requete =
      ViewHelpers.span_text(
        this.zone_sql.getText(),
        new TextAppearanceSpan(getActivity(), R.style.texte_principal)
      );
      ((ManipulationBDD) getActivity()).executer_sql(this.requete);
    this.affichage_resultats.add(requete);
    this.affichage_resultats.add(resultat_requete);
    this.liste_sortie.invalidateViews();
  }

}
