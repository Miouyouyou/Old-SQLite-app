package software.pecarii.sqlitegenerator.fragments;

/*
   TODO : Vue du schéma de la table selectionnée
 */

import android.content.DialogInterface;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.adapters.ListArrayAdapter;
import software.pecarii.sqlitegenerator.ads.Eticilbup;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ListSMD;
import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;

import android.widget.Toast;

public final class SelectionTables extends ToolbarFragment {

  public static DialogProperties props_popup_suppression =
    new DialogProperties(
      new int[] {R.string.titre_suppression_tables},
      new int[] {R.string.supprimer_tables_selectionnees,
                 R.string.ne_rien_faire}
    );
  public static DialogProperties props_popup_avertissement =
    new DialogProperties(
      new int[] {R.string.dernier_avertissement},
      new int[] {R.string.supprimer_tables_selectionnees,
                 R.string.ne_rien_faire}
    );

  public AlertDialog suppression_tables;
  public AlertDialog dernier_avertissement;
  public LabelledCheckbox coche_avertissement_compris;
  public LinearLayout vue_popup_avertissement;
  public ListView liste_des_tables;
  public String sqlite_database_name;
  public View bup;

  public boolean[] tables_cochees;
  public String[] tables;

  @Override
  public boolean action_clicked(@IdRes int id_action) {
    switch(id_action) {
      case R.id.ajouter_table:
        ((ManipulationBDD) getActivity()).demarrer_creation_table();
        return true;
      case R.id.supprimer_tables:
        afficher_popup_suppression_tables();
        return true;
      default: return false;
    }
  }

  @Override
  public void navigation(View v) {
    ((ManipulationBDD) getActivity()).retourner_a_ecran_principal();
  }

  @Override
  public void resources_setup() {
    this.main_menu = R.menu.actions_liste_tables;
    this.title     = R.string.titre_liste_tables;
    this.navigation_drawable = R.drawable.ic_arrow_back_white_24dp;
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater gonfleur) {
    inflate_in_content(R.layout.fragment_selection_tables);

    View[] vues_popup_avertissement = ViewHelpers.warning_popup_views(
      gonfleur, R.string.contenu_avertissement_suppresion_tables,
      R.string.coche_avertissement_suppression_tables
    );
    this.vue_popup_avertissement = 
      (LinearLayout) vues_popup_avertissement[0];
    this.coche_avertissement_compris = 
      (LabelledCheckbox) vues_popup_avertissement[2];

    this.liste_des_tables =
      (ListView) find_in_content(R.id.selecteur_de_tables);
    this.liste_des_tables.setItemsCanFocus(true);
    this.bup = find_in_content(R.id.bup);

  }
  
  @Override
  public void setup_content_views() {
    lister_tables();
    this.liste_des_tables.setOnItemClickListener(
      new ListSMD("table_selectionnee", this)
    );
    this.dernier_avertissement =
      create_dialog(this.vue_popup_avertissement,
                    props_popup_suppression,
                    "supprimer_tables");
    Eticilbup.tutup(bup, "SQL");
  }

  @Override
  public void cleanup() { Eticilbup.dling(bup); }

  public void lister_tables() {
    this.tables =
      ((ManipulationBDD) this.getActivity()).liste_nom_tables();
    this.liste_des_tables.setAdapter(
      new ListArrayAdapter<String>(
        tables, R.layout.liste_tables_bdd_element, this.getActivity()
      )
    );
  }

  public void table_selectionnee(AdapterView liste, View e, int p, long i) {
    ((ManipulationBDD) getActivity()).afficher_schema_table(tables[p]);
  }

  public void afficher_popup_suppression_tables() {
    this.tables_cochees = new boolean[this.tables.length];
    create_dialog(this.tables, props_popup_suppression,
      "demande_suppression", "a_supprimer").show();
  }

  public void demande_suppression(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) this.dernier_avertissement.show();
  }

  public void a_supprimer(DialogInterface dialog, int element,
                          boolean selectionne) {
    this.tables_cochees[element] = selectionne;
  }

  public void supprimer_tables(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) {
      if (this.coche_avertissement_compris.check) {
        ((ManipulationBDD) getActivity()).
          supprimer_tables(this.tables, this.tables_cochees);
        lister_tables();
      }
      else
        Toast.makeText(getActivity(),
          R.string.avertissement_suppression_tables_non_compris,
          Toast.LENGTH_LONG
        ).show();
    }
  }

}
