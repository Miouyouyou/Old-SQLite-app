package software.pecarii.sqlitegenerator.fragments.wizards;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import static android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import static android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

// Because Java cannot consider any objects array as Object[]
import java.lang.reflect.Array;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.fragments.WizardPage;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;
import software.pecarii.sqlitegenerator.helpers.sqltypes.ForeignKey;
import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;

public abstract class PageListe extends WizardPage<CreationTable> {
  
  public boolean mode_suppression;
  public int coches;
  public int texte_bouton_ajout;
  public int texte_bouton_retrait;
  //public int texte_description_liste;
  public int layout_complet;
  public int layout_element_liste;
  public int id_liste;
  public ListView liste_des_elements;
  public boolean[] elements_coches;

  public PageListe() {
    super();
    //this.texte_description_liste = R.string.liste_clefs_etrangeres_ajoutees;
    this.layout_element_liste = R.layout.liste_clefs_etrangeres_element;
    this.layout_complet = R.layout.assistant_creation_table_liste_clefs_etrangeres;
    this.id_liste = R.id.liste_elements;
    this.title      = R.string.ct_wizard;
    //this.main_menu = R.menu.wizard_default_list_menu;
  }

  @Override
  public void lower_action_clicked(View v) {
    /*System.err.println("called");*/
    switch(v.getId()) {
      case R.id.nouvelle_colonne:
        vers_interface_creation_edition(-1);
        break;
      case R.id.bascule_mode:
        passage_autre_mode();
    }
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {

    this.mode_suppression = false;
    inflate_in_content(this.layout_complet);
  
    this.liste_des_elements =
      (ListView) find_in_content(this.id_liste);
    this.liste_des_elements.setOnItemClickListener(new ActionElement());

  }

  @Override
  public void setup_content_views() {
    super.setup_content_views();
    afficher_liste_elements();
  }

  abstract public CharSequence[] elements();
  
  abstract public void vers_interface_creation_edition(int element);
  
  abstract public void supprimer_elements();
  
  
  public void afficher_liste_elements() {
    this.liste_des_elements.setAdapter(
      new ArrayAdapter(getActivity(), this.layout_element_liste,
                       elements())
    );
    reset_etat();
  }

  public void reset_etat() {
    this.elements_coches = new boolean[elements().length];
    wizard.element_edite = -1;
    this.coches = 0;
  }

  public class ActionElement implements OnItemClickListener {
    @Override
    public void onItemClick(AdapterView<?> liste, View v, 
                            int position, long id) {
      edite_ou_coche((LabelledCheckbox) v, position);
    }
  }
  
  public void edite_ou_coche(LabelledCheckbox v, int position) {   
    if (!mode_suppression) vers_interface_creation_edition(position);
    else cocher(v, position);
  }
  
  public void cocher(LabelledCheckbox v, int position) {
    v.toggle();
    elements_coches[position] = v.check;
    if (elements_coches[position]) coches++; else coches--;
  }
  
  public void passage_autre_mode() {
    /* Si l'on doit supprimer, supprimons et reaffichons la liste */
    if (mode_suppression && coches != 0) {
      supprimer_elements();     
      afficher_liste_elements();
    }
      
    mode_suppression = !mode_suppression;
    activer_coches(mode_suppression);
  }
  
  public void activer_coches(boolean etat) {
    for (int i = 0; i < liste_des_elements.getChildCount(); i++) {
      ((LabelledCheckbox) liste_des_elements.getChildAt(i)).show_check(etat);
    }
  }
}
