package software.pecarii.sqlitegenerator.fragments.wizards;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import static android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.Arrays;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.fragments.WizardPage;

import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable.ClefSecondaire;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable.ClefSecondairePresente;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable.ClefSecondaireFuture;
import static software.pecarii.sqlitegenerator.helpers.ViewHelpers.set_entries_of;

import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.SpinnerSMD;
import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;
import software.pecarii.sqlitegenerator.vues.FormCE;


public class CreationTableClefEtrangere
  extends WizardPage<CreationTable> {

  public static DialogProperties props_popup_clauses =
    new DialogProperties(new int[] {R.string.titre_clauses_ce},
                         new int[] {R.string.popup_clauses_modifier,
                                    R.string.ne_rien_faire});

  public boolean mode_table_futur;
  public boolean en_plein_edition;
  public char position_table_futur;
  public char vues_ajoutees;
  public char table;
  public byte[] clauses;
  public Dialog popup_reglage_clauses;
  public EditText zone_nom_table_inconnue;
  public LabelledCheckbox coche_defere;
  public LinearLayout parametrage_clef_etrangere;
  public RelativeLayout interface_popup_reglage_clauses;
  public OnItemSelectedListener dispatcher_choix_table;
  public Spinner choix_clause_delete;
  public Spinner choix_clause_update;
  public Spinner choix_tables_connues;
  public String[] elements_choix_table;
  public View[] boutons_table_inconnue;

  public CreationTableClefEtrangere() {
    super();
    this.subtitle = R.string.ct_page_ajout_colonne_etrangere;
    this.lower_menu = R.menu.creation_table_clef_etrangere;
  }

  @Override
  public void resources_setup() {
    super.resources_setup();
    this.vues_ajoutees = 0;
    this.table         = 0;
    this.clauses       = new byte[2];
    this.position_table_futur = (char) wizard.noms_tables.length;
    this.elements_choix_table = 
      Arrays.copyOf(wizard.noms_tables, this.position_table_futur + 1);
    this.elements_choix_table[this.position_table_futur] =
      getString(R.string.table_inconnue);
    this.dispatcher_choix_table = new SpinnerSMD("choix_table", this);
    this.mode_table_futur = false;
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {
    inflate_in_content(R.layout.assistant_creation_table_clef_etrangere);

    this.choix_tables_connues = 
      (Spinner) find_in_content(R.id.choix_liste_tableaux);
    this.zone_nom_table_inconnue = 
      (EditText) find_in_content(R.id.zone_nom_tableau_inconnu); 
    this.parametrage_clef_etrangere =
      (LinearLayout) find_in_content(R.id.liste_colonnes_etrangeres);

    this.interface_popup_reglage_clauses = 
      (RelativeLayout)
        inflater.inflate(
          R.layout.popup_clauses_clefs_etrangeres, null, false
        );
    this.choix_clause_update = 
      (Spinner) 
        find(R.id.choix_clause_update, interface_popup_reglage_clauses);       
    this.choix_clause_delete =
      (Spinner)
        find(R.id.choix_clause_delete, interface_popup_reglage_clauses);
  }

   @Override
   public void setup_content_views() {
     reset_liste_clefs_etrangeres();
     boutons_table_inconnue = new View[2];
     boutons_table_inconnue[0] =
       find(R.id.ajouter_col_composante, this.lower_toolbar_container);
     boutons_table_inconnue[1] =
       find(R.id.retirer_col_composante, this.lower_toolbar_container);
     set_entries_of(this.choix_tables_connues, this.elements_choix_table);
     this.choix_tables_connues
       .setOnItemSelectedListener(this.dispatcher_choix_table);
     this.popup_reglage_clauses =
       create_dialog(this.interface_popup_reglage_clauses,
                     props_popup_clauses, "clauses");
     if (this.wizard.en_mode_edition()) {
       this.en_plein_edition = true;
       afficher(this.wizard.clef_etrangere_actuelle());
       this.clauses = 
         Arrays.copyOf(this.wizard.clef_etrangere_actuelle().clauses, 2);
       regler_popup_clauses();
       this.en_plein_edition = false;
     }      
   }

   @Override
   public void cleanup() {
     this.popup_reglage_clauses            = null;
     this.zone_nom_table_inconnue          = null;
     this.coche_defere                     = null;
     this.parametrage_clef_etrangere       = null;
     this.interface_popup_reglage_clauses  = null;
     this.dispatcher_choix_table           = null;
     this.choix_clause_delete              = null;
     this.choix_clause_update              = null;
     this.choix_tables_connues             = null;
     this.elements_choix_table             = null;
     this.boutons_table_inconnue           = null;
   }

   @Override
   public void lower_action_clicked(View v) {
     switch(v.getId()) {
       case R.id.parametrer_clauses:
         this.popup_reglage_clauses.show();
         break;
       case R.id.ajouter_col_composante:
         formulaire_manuel();
         break;
       case R.id.retirer_col_composante:
         retirer_dernier_formulaire();
         break;
     }
   }
  
  public void afficher(ClefSecondaire clef_etrangere) {
    if (!clef_etrangere.table_future) {
      ClefSecondairePresente fk = (ClefSecondairePresente) clef_etrangere;
      afficher(fk);
    }
    else {
      ClefSecondaireFuture fk = (ClefSecondaireFuture) clef_etrangere;
      afficher(fk);
    }
  }

  public void afficher(ClefSecondairePresente fk) {
    this.table = fk.table_parente;
    this.choix_tables_connues.setSelection(table);
    mode_tables_connues();
    for (byte i = 0; i < fk.enfants.length; i++)
      ajouter( 
        FormCE.pour(this.main_inflater,
                    this.wizard.spinner_colonnes(fk.enfants[i]),
                    this.wizard.noms_pks_tables[table],
                    fk.parents[i])
      );
  }

  public void afficher(ClefSecondaireFuture fk) {
    this.choix_tables_connues.setSelection(this.position_table_futur);
    mode_table_inconnue();
    this.zone_nom_table_inconnue.setText(fk.table_parente);
    for (byte i = 0; i < fk.enfants.length; i++)
      ajouter( 
        FormCE.pour(this.main_inflater,
                    this.wizard.spinner_colonnes(fk.enfants[i]),
                    fk.parents[i])
      );   
  }

  public void afficher_clef_primaire_table() {
    reset_liste_clefs_etrangeres();

    for (int i = 0; i < this.wizard.noms_pks_tables[table].length; i++)
      ajouter(
        FormCE.pour(this.main_inflater,
                    this.wizard.spinner_colonnes(0),
                    this.wizard.noms_pks_tables[table],
                    (char) i)
      );
  }

  public void mode_tables_connues() {
    mode_table_futur = false;
    visibilite_boutons_table_inconnue(View.GONE);
    reset_liste_clefs_etrangeres();
    this.zone_nom_table_inconnue.setVisibility(View.GONE);
  }

  public void mode_table_inconnue() {
    mode_table_futur = true;
    reset_liste_clefs_etrangeres();
    this.zone_nom_table_inconnue.setVisibility(View.VISIBLE);
    visibilite_boutons_table_inconnue(View.VISIBLE);
  }

  public void visibilite_boutons_table_inconnue(int visibilite) {
    for(View v : this.boutons_table_inconnue) v.setVisibility(visibilite);
  }

  public void ajouter(FormCE v) {
    this.parametrage_clef_etrangere.addView(v);
    this.vues_ajoutees++;
  }

  public FormCE formulaire(int position) {
    return ((FormCE) parametrage_clef_etrangere.getChildAt(position));
  }

  public void 
  choix_table(boolean selection, AdapterView liste, View element,
              int position, long id) {

    if (position != position_table_futur) {
      if (!mode_table_futur); else mode_tables_connues();
      if (this.table != position) {
        this.table = (char) position;
        afficher_clef_primaire_table();
      }
    }
    else { 
      if (!mode_table_futur) mode_table_inconnue();
      if (!en_plein_edition) formulaire_manuel();
    }    
  }
  
  public void clauses(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) {
      this.clauses[0] =
        (byte) choix_clause_update.getSelectedItemPosition();
      this.clauses[1] =
        (byte) choix_clause_delete.getSelectedItemPosition();
    }
  }
  
  public void regler_popup_clauses() {
    this.choix_clause_update.setSelection(this.clauses[0]);
    this.choix_clause_delete.setSelection(this.clauses[1]);
  }
  
  public void reset_liste_clefs_etrangeres() {
    this.parametrage_clef_etrangere.removeAllViews();
    this.vues_ajoutees = 0;
  }
  
  public void formulaire_manuel() {
    ajouter(
      FormCE.pour(this.main_inflater, this.wizard.spinner_colonnes(0))
    );
  }
  
  public void retirer_dernier_formulaire() {
     if (this.vues_ajoutees > 0)
       this.parametrage_clef_etrangere.removeViewAt(--this.vues_ajoutees);
  }  
  
  @Override
  public void onNext() {
    wizard.clef_etrangere_actuelle(generer_clef_etrangere());
  }

  public ClefSecondaire generer_clef_etrangere() {

    final char[] enfants = new char[vues_ajoutees];
    FormCE formulaire_actuel = null;
    ClefSecondaire nouvelle_clef_etrangere;

    if(!mode_table_futur) {
      final char table_parente =
        (char) choix_tables_connues.getSelectedItemPosition();
      final char[] parents = new char[vues_ajoutees];

      for (byte i = 0; i < vues_ajoutees; i++) {
        formulaire_actuel = formulaire(i);
        enfants[i] = formulaire_actuel.enfant_selectionne();
        parents[i] = formulaire_actuel.parent_selectionne();
      }

      nouvelle_clef_etrangere =
        wizard.new ClefSecondairePresente(enfants, table_parente, parents);
    }
    else {
      final String table_parente =
        zone_nom_table_inconnue.getText().toString();
      final String[] parents = new String[vues_ajoutees];

      for (byte i = 0; i < vues_ajoutees; i++) {
        formulaire_actuel = formulaire(i);
        enfants[i] = formulaire_actuel.enfant_selectionne();
        parents[i] = formulaire_actuel.parent();
      }

      nouvelle_clef_etrangere =
        wizard.new ClefSecondaireFuture(enfants, table_parente, parents);
    }

    nouvelle_clef_etrangere.clauses    = clauses;
    //nouvelle_clef_etrangere.deferrable = coche_defere.check;

    return nouvelle_clef_etrangere;
  }

  @Override
  public boolean canGoNext() { return vues_ajoutees > 0; }

  @Override
  public void onSaveInstanceState(Bundle bundle) {
    bundle.clear();
    super.onSaveInstanceState(null);
  }
}

