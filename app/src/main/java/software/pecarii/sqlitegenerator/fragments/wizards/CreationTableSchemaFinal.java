package software.pecarii.sqlitegenerator.fragments.wizards;

import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.WizardPage;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;

import software.pecarii.sqlitegenerator.ManipulationBDD;

public class CreationTableSchemaFinal 
  extends WizardPage<CreationTable> {
  
  public TextView zone_schema;
  public String exception_message;
  
  public CreationTableSchemaFinal() { 
    super();
    this.title = R.string.ct_page_schema_final;
  }

  @Override
  public boolean canGoNext() {
    try {
      ((ManipulationBDD) getActivity()).creer_table(wizard.schema());
    }
    catch (android.database.sqlite.SQLiteException e) {
      this.exception_message = e.getMessage();
      return false;
    }
    return true;
  }

  @Override
  public void tellWhyCantGoNext() {
    Toast.makeText(Application.contexte(), this.exception_message,
      Toast.LENGTH_SHORT).show();
  }

  @Override
  public void initialise_content_views(LayoutInflater inflater) {
    inflate_in_content(R.layout.assistant_schema_final);
    this.zone_schema = (TextView) find_in_content(R.id.zone_schema);
  }

  @Override
  public void setup_content_views() {
    this.zone_schema.setText(wizard.schema());
  }
}
