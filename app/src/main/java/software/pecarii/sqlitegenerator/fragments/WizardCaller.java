package software.pecarii.sqlitegenerator.fragments;

public interface WizardCaller {
  int CANCELLED = 0;
  int SUCCESS = 1;
  void end(int return_code);
}
