package software.pecarii.sqlitegenerator.fragments.wizards;

import android.view.View;
import android.widget.Spinner;

import software.pecarii.sqlitegenerator.R;

public class CreationTableAjoutContrainteUnique 
  extends PageDumbMenuList {
  
  public int[] positions_colonnes_selectionnees;
  
  public CreationTableAjoutContrainteUnique() {
    super();
    this.title = R.string.ct_page_ajout_contrainte_unique;
  }
  
  @Override
  public View menu(int elem_selectionne) {
    return wizard.spinner_colonnes(elem_selectionne);
  }
  
  @Override
  public void parametrage_ressources() {  
    this.nombre_limite_de_menus = wizard.colonnes.length;
  }
  
  @Override
  public boolean canGoNext() {
    if (vues_ajoutees != 0) {
      this.reason_cant_go_next = 
        R.string.doublon_selection_colonnes;
      this.positions_colonnes_selectionnees = new int[vues_ajoutees];
      boolean[] deja_selectionnees = new boolean[nombre_limite_de_menus];
      int colonne_selectionnee = 0;
      for (int i = 0; i < vues_ajoutees; i++) {
        colonne_selectionnee = 
          ((Spinner) menu_position(i)).getSelectedItemPosition();
        if (deja_selectionnees[colonne_selectionnee]) return false;
        deja_selectionnees[colonne_selectionnee] = true;
        positions_colonnes_selectionnees[i] = colonne_selectionnee;
      }
      return true;
    }
    this.reason_cant_go_next = 
      R.string.selectionne_ou_revient;
    return false;
  }
  
  @Override
  public void onNext() {    
    wizard.contrainte_unique(this.positions_colonnes_selectionnees);
  }
}
