package software.pecarii.sqlitegenerator.fragments;

import software.pecarii.sqlitegenerator.fragments.WizardCaller;

public interface Wizard {
  void start();
  void stop();
  void previous_step();
  void next_step();
  void current_step(int current_step);
  void on_end(WizardCaller callback);
}
