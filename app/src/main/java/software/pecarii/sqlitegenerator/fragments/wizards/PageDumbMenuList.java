package software.pecarii.sqlitegenerator.fragments.wizards;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import static android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.WizardPage;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;

public abstract class PageDumbMenuList extends WizardPage<CreationTable> {

  public int vues_ajoutees;
  public int texte_raison_limite;
  public int nombre_limite_de_menus;
  @LayoutRes public int layout_res;
  @IdRes     public int liste_id;
  public LinearLayout liste_des_elements;

  public PageDumbMenuList() {
    super();
    this.title                = R.string.ct_wizard;
    this.texte_raison_limite  = R.string.limite_ajout_col_composantes;
    this.layout_res           = R.layout.page_liste_banale;
    this.liste_id             = R.id.liste_des_elements;
  }

  @Override
  public void lower_action_clicked(View v) {
    switch(v.getId()) {
      case R.id.ajouter_col_composante:
        ajouter_un_menu(0);
        return;
      case R.id.retirer_col_composante:
        retirer_le_dernier_menu();
    }
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {
    parametrage_ressources();
    inflate_in_content(layout_res);
  
    this.liste_des_elements = (LinearLayout) find(liste_id, this.content);
        
    this.vues_ajoutees = 0;
  }
  
  abstract public View menu(int elem_selectionne);
  
  abstract public void parametrage_ressources();
  
  public void retirer_le_dernier_menu() {
    if (vues_ajoutees > 0)
      liste_des_elements.removeViewAt(--vues_ajoutees);
  }
  
  public void ajouter_plusieurs_menus(int n_menus) { 
    ajouter_plusieurs_menus(new int[n_menus]); 
  }

  public void ajouter_plusieurs_menus(int[] elements_selectionnes) {
    for (int element : elements_selectionnes) ajouter_un_menu(element);
  }
  
  public void ajouter_un_menu(int elem_selectionne) {
    /*System.out.println(nombre_limite_de_menus);*/
    if (vues_ajoutees < nombre_limite_de_menus) {
      this.liste_des_elements.addView(menu(elem_selectionne));
      vues_ajoutees++;
    }
    else afficher_raison_limite();
  }
  
  public void afficher_raison_limite() {
    Toast.makeText(Application.contexte(), 
                   this.texte_raison_limite, 
                   Toast.LENGTH_SHORT).show();
  }
  
  public View menu_position(int pos) {
    return this.liste_des_elements.getChildAt(pos);
  }
  
}
