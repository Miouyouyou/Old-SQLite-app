package software.pecarii.sqlitegenerator.fragments.wizards;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;
import software.pecarii.sqlitegenerator.fragments.wizards.PageListe;
import static software.pecarii.sqlitegenerator.helpers.StringHelpers.toString_array;

public class CreationTableListeContraintesUnique extends PageListe {
  public CreationTableListeContraintesUnique() {
    super();
    this.title = R.string.ct_page_liste_contraintes_unique;
  }
  
  public String[] elements() { 
    return toString_array((Object) wizard.contraintes_unique);
  }
  
  @Override
  public void resources_setup() {
    super.resources_setup();
    this.texte_bouton_ajout   = R.string.ajouter_contrainte_unique;
    this.texte_bouton_retrait = 
      R.string.supprimer_contraintes_selectionees;
  }
  
  @Override
  public void vers_interface_creation_edition(int element) {
    wizard.element_edite(element);
    wizard.detour(CreationTable.P_ACU, 0);
  }
  
  public void supprimer_elements() {
    wizard.supprimer_contraintes_unique(elements_coches, coches);
  }
}
