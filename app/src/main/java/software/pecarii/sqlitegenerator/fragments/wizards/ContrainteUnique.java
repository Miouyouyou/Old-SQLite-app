package software.pecarii.sqlitegenerator.fragments.wizards;

import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;

public class ContrainteUnique {
  
  public static StringBuilder sb;
    
  static { sb = new StringBuilder(64); } 
  
  public static synchronized String 
  generer_contrainte(CreationTable wizard, int[] colonnes) {
    sb.delete(0, 4096);
    sb.append("UNIQUE (");
    sb.append(wizard.colonnes[colonnes[0]].nom_traité);
    for (int i = 1; i < colonnes.length; i++) {
      sb.append(',');
      sb.append(wizard.colonnes[colonnes[i]].nom_traité);
    }
    sb.append(")");
    return sb.toString();
  }
   
  public int[] colonnes;
  public CreationTable wizard;

  public ContrainteUnique(CreationTable wizard, 
                          int[] colonnes_choisies) {
    this.wizard   = wizard;
    this.colonnes = colonnes_choisies;
  }
  
  public void colonnes(int[] nouvelles_colonnes) {
    this.colonnes = nouvelles_colonnes;
  }

  public String toString() { 
    return generer_contrainte(wizard, this.colonnes); 
  }
}
