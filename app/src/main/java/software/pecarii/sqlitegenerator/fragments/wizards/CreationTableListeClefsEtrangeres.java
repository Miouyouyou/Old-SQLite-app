package software.pecarii.sqlitegenerator.fragments.wizards;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.fragments.wizards.PageListe;
import static software.pecarii.sqlitegenerator.helpers.StringHelpers.toString_array;

public class CreationTableListeClefsEtrangeres extends PageListe {
  
  public CreationTableListeClefsEtrangeres() { 
    super();
    this.subtitle = R.string.ct_page_liste_colonnes_etrangeres;
    this.lower_menu = R.menu.ct_liste_clef_etrangeres;
  }
  
  @Override
  public void resources_setup() {
    super.resources_setup();
    this.layout_element_liste = R.layout.liste_clefs_etrangeres_element;
    this.layout_complet = R.layout.assistant_creation_table_liste_clefs_etrangeres;
    this.id_liste = R.id.liste_elements;
  }
  
  @Override
  public CharSequence[] elements() {
   return wizard.descriptions(wizard.clefs_etrangeres);
  }
  
  @Override
  public void vers_interface_creation_edition(int element) {
    wizard.element_edite(element);
    wizard.detour(CreationTable.P_AFK, 0);
  }

  @Override
  public void supprimer_elements() {
    wizard.supprimer_clefs_etrangeres(elements_coches, coches);
  }

}
