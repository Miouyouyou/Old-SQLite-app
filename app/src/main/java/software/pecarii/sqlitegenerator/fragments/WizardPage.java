package software.pecarii.sqlitegenerator.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.IdRes;
import android.view.View;
import static android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.Wizard;
import software.pecarii.sqlitegenerator.R;

public class WizardPage<WizardClass extends Wizard> 
  extends ToolbarFragment {

  public static final int default_cant_go_next_reason;
  
  static {
    default_cant_go_next_reason = R.string.default_cant_go_next_reason;
  }

  public int reason_cant_go_next;
  public int page_number;
  public int actions_menu_res;
  public int explanations;
  public int page_header;
  public TextView next_button;
  public TextView warning;
  public WizardClass wizard;
  

  public WizardPage() {
    super();

    this.title               = R.string.default_wizard_title;
    this.subtitle            = R.string.default_wizard_header;
    this.reason_cant_go_next = default_cant_go_next_reason;
    this.main_menu           = R.menu.wizard_navigation;
    this.navigation_drawable = R.drawable.ic_arrow_back_white_24dp;
  }

  @Override
  public void navigation(View v) {
    onPrevious();
    wizard.previous_step();
  }

  @Override
  public boolean action_clicked(@IdRes int action_cliquee) {
    switch(action_cliquee) {
      case R.id.wizard_next:
        if (canGoNext()) {
          onNext();
          wizard.next_step();
        }
        else tellWhyCantGoNext();
        return true;
      default: return false;
    }
  }

  @Override
  public void special_setup() { wizard.current_step(this.page_number); }

  public void wizard(WizardClass wizard) { this.wizard = wizard; }

  public void page_number(int page_num) {this.page_number = page_num;}

  public void onPrevious() { }

  public void onNext() { }
  
  public boolean canGoNext() { return true; }
  
  public void tellWhyCantGoNext() {
    Toast.makeText(context, this.reason_cant_go_next, 
                   Toast.LENGTH_SHORT).show();
  }

  public AlertDialog 
    create_dialog(View dialog_view, int[] buttons_labels,
                  DialogInterface.OnClickListener buttons_action) {
    AlertDialog.Builder dialog_builder = 
      new AlertDialog.Builder(getActivity());
    dialog_builder.setView(dialog_view);
    dialog_builder.setPositiveButton(buttons_labels[0], buttons_action);
    dialog_builder.setNegativeButton(buttons_labels[2], buttons_action);
    if (buttons_labels[1] != 0)
    dialog_builder.setNeutralButton(buttons_labels[1], buttons_action);
    return dialog_builder.create();
  }
  
}
