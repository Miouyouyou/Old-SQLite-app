package software.pecarii.sqlitegenerator.fragments;

import android.support.annotation.ArrayRes;
import android.support.v7.app.AlertDialog;
import static android.support.v7.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.Arrays;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.helpers.MethodHelpers;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.DialogMCSMD;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ViewSMD;

public class ToolbarFragment extends Fragment {

  public static final Context        context;
  public static final byte positive_button =
    (byte) DialogInterface.BUTTON_POSITIVE;
  public static final byte negative_button =
    (byte) DialogInterface.BUTTON_NEGATIVE;
  public static final byte neutral_button =
    (byte) DialogInterface.BUTTON_NEUTRAL;
  public static final byte dismiss = 0;
  public static final LinearLayout.LayoutParams lower_element_layout =
    new LinearLayout.LayoutParams(
      0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f
    );

  static { context = Application.contexte(); }

  public boolean magic_restore;
  @StringRes public int title;
  @StringRes public int subtitle;
  @StringRes public int flash;
  @MenuRes   public int main_menu;
  @MenuRes   public int lower_menu;
  @DrawableRes public int navigation_drawable;
  public int flash_background;
  public String navigation_method;
  public LayoutInflater main_inflater;
  public LinearLayout container;
  public LinearLayout content;
  public LinearLayout lower_toolbar_container;
  public MenuDispatcher menu_dispatcher;
  public TextView flash_area;
  public Toolbar main_toolbar;
  public Toolbar lower_toolbar;
  public View separator;

  public ToolbarFragment() {
    this.magic_restore = true;
    this.menu_dispatcher = new MenuDispatcher();
    this.main_menu = R.menu.empty_menu;
    this.title     = R.string.app_name;
    this.navigation_method = "navigation";
    this.flash_background = 0xFFA00000;
  }


  public void add_toolbar_menu(@MenuRes int menu_res) {
    this.main_toolbar.inflateMenu(menu_res);
  }

  public void setup_toolbar() {
    this.main_toolbar.setOnMenuItemClickListener(this.menu_dispatcher);
    this.main_toolbar.setTitle(this.title);

    if (this.subtitle == 0);
    else this.main_toolbar.setSubtitle(this.subtitle);

    if (this.navigation_drawable == 0);
    else setup_toolbar_navigation();

    add_toolbar_menu(this.main_menu);
    if (this.lower_menu == 0);
    else add_lower_menu(this.lower_menu);
  }

  public void setup_toolbar_navigation() {
    this.main_toolbar.setNavigationIcon(this.navigation_drawable);
    this.main_toolbar.setNavigationOnClickListener(
      new ViewSMD(this.navigation_method, this)
    );
  }

  public void show_lower_toolbar() {
    this.lower_toolbar.setOnMenuItemClickListener(this.menu_dispatcher);
    this.lower_toolbar.setVisibility(View.VISIBLE);
  }

  public void add_lower_menu(@MenuRes int menu_res) {
    show_lower_toolbar();
    TextView[] menu_elements =
      ViewHelpers.convert_menu(menu_res, this, "lower_action_clicked");
    this.lower_toolbar_container =
      (LinearLayout)
        find(R.id.lower_toolbar_items_container, this.lower_toolbar);
    for (View element : menu_elements) {
      element.setLayoutParams(lower_element_layout);
      this.lower_toolbar_container.addView(element);
    }
  }

  public void setup_flash() {
    this.flash_area.setBackgroundColor(this.flash_background);
    if (this.flash == 0);
    else this.flash_area.setText(this.flash);
  }

  public void show_flash_area_with(@StringRes int string_id) {
    this.flash_area.setText(string_id);
    this.flash_area.setVisibility(View.VISIBLE);
  }

  public void hide_flash_area() {
    this.flash_area.setVisibility(View.GONE);
  }

  public boolean action_clicked(@IdRes int id) { return false; }

  public void navigation(View v) {}

  public void lower_action_clicked(View v) {
    /*System.out.println(((TextView) v).getText().toString());*/
  }

  public void dialog_clicked(@IdRes int dialog_id, int clicked_button) {}

  public void inflate_in_content(@LayoutRes int id) {
    this.main_inflater.inflate(id, this.content, true);
  }

  public View find_in_content(@IdRes int id) {
    return this.content.findViewById(id);
  }

  @Override
  public View onCreateView(LayoutInflater inflater,
                           @Nullable ViewGroup group,
                           @Nullable Bundle savedState) {
    this.main_inflater = inflater;
    this.container =
      (LinearLayout)
        inflater.inflate(R.layout.standard_fragment_layout, group, false);
    main_toolbar  = (Toolbar)  find(R.id.main_toolbar,  this.container);
    lower_toolbar = (Toolbar)  find(R.id.lower_toolbar, this.container);
    flash_area    = (TextView) find(R.id.area_alert,    this.container);
    separator     =
      find(R.id.separator_before_container, this.container);
    content       =
      (LinearLayout)
        find(R.id.standard_fragment_content, this.container);

    if(savedState == null) first_resources_setup();
    else first_restoration(savedState);
    resources_setup();
    initialise_content_views(inflater);

    setup_toolbar();
    setup_flash();
    special_setup();
    return container;
  }

  public void first_resources_setup() {}
  public void first_restoration(@NonNull Bundle savedState) {}
  public void resources_setup() {}
  public void initialise_content_views(@NonNull LayoutInflater inflater) {}
  public void special_setup() {}

  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    if (magic_restore) super.onActivityCreated(savedInstanceState);
    else super.onActivityCreated(null);
    if (savedInstanceState == null) last_resources_setup();
    else last_restoration(savedInstanceState);
    setup_content_views();
  }

  public void last_resources_setup() {}
  public void last_restoration(@NonNull Bundle savedInstanceState) {}
  public void setup_content_views() {}

  @Override
  public void onStart() {
    super.onStart();
    late_setup();
  }

  public void late_setup() {}

  // Utilities
  public View find(@IdRes int id, @NonNull View container) {
    return container.findViewById(id);
  }

  public void onDestroyView() {
    this.main_toolbar            = null;
    this.lower_toolbar_container = null;
    this.lower_toolbar           = null;
    this.flash_area              = null;
    this.separator               = null;
    this.content                 = null;
    this.container.removeAllViews();
    this.container = null;
    cleanup();
    super.onDestroyView();
  }

  public void cleanup() {}

  public <T extends DialogInterface.OnClickListener & 
                    DialogInterface.OnDismissListener>
  AlertDialog create_dialog(View view, DialogProperties properties,
                            T dispatcher) {
    @StringRes final int dialog_content = 
      properties.heading_and_content[1];
    AlertDialog result = null;
    Builder builder = prebuilt_dialog(properties, dispatcher);

    if (dialog_content == 0) builder.setView(view, 0, 0, 0, 0);
    else builder.setMessage(dialog_content);

    result = builder.create();
    result.setOnDismissListener(dispatcher);
    return result;
  }

  AlertDialog create_dialog(CharSequence[] elements, 
                            DialogProperties properties,
                            String dispatcher_method_name,
                            String checks_dispatcher_method_name) {
    StandardReflectionDispatcher dispatcher =
      new StandardReflectionDispatcher(dispatcher_method_name, this);
    DialogMCSMD checks_dispatcher =
      new DialogMCSMD(checks_dispatcher_method_name, this);
    AlertDialog result = null;
    Builder builder = prebuilt_dialog(properties, dispatcher);
    builder.setMultiChoiceItems(elements, null, checks_dispatcher);
    result = builder.create();
    result.setOnDismissListener(dispatcher);
    return result;
  }

  AlertDialog create_dialog(@ArrayRes int choices,
                            String choice_dispatcher_method_name) {
    StandardReflectionDispatcher dispatcher_choice =
      new StandardReflectionDispatcher(choice_dispatcher_method_name, this);
    AlertDialog dialog =
      new Builder(getActivity()).
        setItems(choices, dispatcher_choice).create();
    dialog.setOnDismissListener(dispatcher_choice);
    return dialog;
  }

  public <T extends DialogInterface.OnClickListener &
                    DialogInterface.OnDismissListener>
  AlertDialog.Builder
  prebuilt_dialog(DialogProperties properties, T dispatcher) {
    final int[] buttons_text = properties.buttons_text;
    @StringRes final int dialog_heading = properties.heading_and_content[0];

    AlertDialog result = null;
    Builder builder = new Builder(getActivity());
    builder.
      setTitle(dialog_heading).
      setPositiveButton(buttons_text[0], dispatcher).
      setNegativeButton(buttons_text[1], dispatcher);
    if (buttons_text[2] == 0);
    else builder.setNeutralButton(buttons_text[2], dispatcher);
    return builder;    
  }
  

  public AlertDialog
  create_dialog(View view, DialogProperties properties,
                String handler_method) {
    return create_dialog(
       view, properties,
       new StandardReflectionDispatcher(handler_method, this)
    );

  }

  public static class DialogProperties {
    public int[] buttons_text;
    public int[] heading_and_content;
    public DialogProperties(int[] heading_and_content,
                            int[] buttons_text) {
      this.buttons_text        = Arrays.copyOf(buttons_text, 3);
      this.heading_and_content = Arrays.copyOf(heading_and_content, 2);
    }
    public void buttons_text(int[] references) {
      this.buttons_text = Arrays.copyOf(references, 3);
    }
    public void heading_and_content(int[] references) {
      this.heading_and_content = Arrays.copyOf(references, 2);
    }
  }

  public class MenuDispatcher implements Toolbar.OnMenuItemClickListener {

    @Override
    public boolean onMenuItemClick(MenuItem item) {
      return action_clicked(item.getItemId());
    }
  }
  
  public static class StandardReflectionDispatcher
    implements DialogInterface.OnClickListener,
               DialogInterface.OnDismissListener {
    public Method invoked_method;
    public Object target;

    public
    StandardReflectionDispatcher(final String method_name, Object me) {
      this.target = me;
      this.invoked_method = MethodHelpers.get_method(
        method_name, me.getClass(), DialogInterface.class, int.class
      );
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
      invoke_with(dialog, which);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
      invoke_with(dialog, dismiss);
    }

    public void invoke_with(DialogInterface dialog, int which) {
      MethodHelpers.invoke_method(invoked_method, this.target, dialog, which);
    }
  }

}
