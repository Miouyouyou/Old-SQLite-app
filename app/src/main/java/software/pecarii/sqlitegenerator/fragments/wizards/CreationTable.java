package software.pecarii.sqlitegenerator.fragments.wizards;

  /*
    TODO : Paufinage du tout
    TODO : - Icônes à côté des colonnes ajoutées, des PK, FK et contraintes
    TODO : - Gestion de multiples détours !
    TODO : - Mise en place d'une page permettant de choisir les contraintes
    TODO : - Mise en place d'une page récapitulative
  */

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import static android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.support.v7.widget.AppCompatSpinner;

import gnu.trove.map.hash.TObjectIntHashMap;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.Arrays;

import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.application.Application;

import software.pecarii.sqlitegenerator.fragments.Wizard;
import software.pecarii.sqlitegenerator.fragments.WizardCaller;
import software.pecarii.sqlitegenerator.fragments.WizardPage;

import software.pecarii.sqlitegenerator.helpers.StringHelpers;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.helpers.sqltypes.ForeignKey;
import software.pecarii.sqlitegenerator.helpers.sqltypes.PrimaryKey;
import static software.pecarii.sqlitegenerator.helpers.StringHelpers.list;
import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;

import static software.pecarii.sqlitegenerator.vues.ContrainteCheck.CheckConstraint;

public class CreationTable implements Wizard { 

  public static final byte P_BASE = 0;
  public static final byte P_PK   = 1;
  public static final byte P_LFK  = 2;
  public static final byte P_AFK  = 4;
  public static final byte P_LCC  = 4;
  public static final byte P_LCU  = 5;
  public static final byte P_ACU  = 6;
  public static final byte P_SF   = 3;

  public static final byte ANNULER = 0;
  public static final byte OK      = 1;
  
  public static final byte derniere_page_par_defaut = P_SF;

  public static final byte aucun_element_edite = -1;

  public static final byte principal = 0;
  public static final byte attributs = 1;

  public static final String detour   = "DETOUR";
  public static final String tag_page = "Page";

  public final static int[] modes_editions = {
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED,
      InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE,
      InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE,
      InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED,
      InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED
  };

  public final static String[] clauses_clef_etrangere = {
    "", "SET NULL ", "SET DEFAULT ", "CASCADE ", "RESTRICT ", "NO ACTION "
  };

  public static final Object[] styles_texte = {
    new TextAppearanceSpan(Application.contexte(), R.style.texte_principal),
    new TextAppearanceSpan(Application.contexte(), R.style.texte_attribut)
  };


  public boolean navigation_standard;
  public int page_actuelle;
  public int derniere_page;
  public int page_de_retour;
  public int element_edite;
  public String nom_de_la_table;
  public WizardCaller rappel_fin;
  public String[] noms_tables;
  public PrimaryKey[] pks_tables;
  public byte[] types_colonnes_ajoutees;
  public WizardPage[] pages_assistant;
  public WeakReference<FragmentManager> gestionnaire_fragment;

  public CTColumn[] colonnes;
  public char[][] clef_primaire;
  public ClefSecondaire[] clefs_etrangeres;
  public ContrainteUnique[] contraintes_unique;
  public CheckConstraint[] contraintes_check;
  
  public ArrayAdapter<String> adapter_noms_colonnes_ajoutees;
  public TObjectIntHashMap<String> positions_colonnes;
  public String tag_actuel;
  public String[][] noms_pks_tables;

  @SuppressWarnings("unchecked")
  public CreationTable() {
    pages_assistant = new WizardPage[] {
      new CreationTableNom(),
      new CreationTableClefsPrimaires(),
      new CreationTableListeClefsEtrangeres(),
/*      new CreationTableListeContraintesUnique(),
      new CreationTableListeContraintesCheck(),*/
      new CreationTableSchemaFinal(),
      new CreationTableClefEtrangere()/*,
      new CreationTableAjoutContrainteUnique()*/
    };
    this.rappel_fin              = null;
    this.gestionnaire_fragment   = null;
    this.derniere_page           = P_SF;
    this.page_de_retour          = 99;
    this.element_edite           = -1;
    this.navigation_standard     = true;
    this.nom_de_la_table         = "";
    this.colonnes                = new CTColumn[0];
    this.clef_primaire           = new char[2][0];
    this.clefs_etrangeres        = new ClefSecondaire[0];
    this.contraintes_unique      = new ContrainteUnique[0];
    this.contraintes_check       = new CheckConstraint[0];
    this.positions_colonnes      = new TObjectIntHashMap<String>(5);
    this.tag_actuel              = tag_page;
    for (int i = 0; i < pages_assistant.length; i++) {
      pages_assistant[i].wizard(this);
      pages_assistant[i].page_number(i);
    }
  }

  public void parametrage_demarrage() {
    Column[] composantes_pk = null;
    this.noms_pks_tables = new String[pks_tables.length][0];
    for (char i = 0; i < pks_tables.length; i++) {
      composantes_pk = pks_tables[i].composantes();
      if (composantes_pk.length != 0) {
        this.noms_pks_tables[i] = new String[composantes_pk.length];
        for (byte j = 0; j < composantes_pk.length; j++)
          this.noms_pks_tables[i][j] = composantes_pk[j].name;
      }
    }
  }

  public CreationTable(FragmentManager fm, WizardCaller callback) {
    this();
    this.rappel_fin = callback;
    fm(fm);
  }

  public void nouvelle_colonne(CTColumn colonne) {
    this.colonnes = ajouter_element(colonnes, colonne);
    traitement_colonnes_ajoutees();
  }

  public void remplacer_colonne(int element, CTColumn colonne) {
    this.colonnes[element] = colonne;
    traitement_colonnes_ajoutees();
  }

  public void supprimer_colonnes(int a_supprimer, boolean[] selections) {
    this.colonnes =
      supprimer_elements(selections, a_supprimer, this.colonnes);
    traitement_colonnes_ajoutees();
  }

  public void nom_de_la_table(String nouveau_nom) {
    this.nom_de_la_table = nouveau_nom;
  }
  
  public Spinner spinner_colonnes(int element_selectionne) {
    Spinner spinner = new AppCompatSpinner(Application.contexte());
    spinner.setAdapter(this.adapter_noms_colonnes_ajoutees);
    spinner.setSelection(element_selectionne);
    return spinner;
  }

 public void traitement_colonnes_ajoutees() {
    int n_colonnes = this.colonnes.length;
    String[] noms  = new String[n_colonnes];
    this.positions_colonnes.clear();
    this.types_colonnes_ajoutees = new byte[n_colonnes];
    for (int i = 0; i < n_colonnes; i++) {
      this.types_colonnes_ajoutees[i] = 
        (byte) this.colonnes[i].type;
      noms[i] = this.colonnes[i].nom;
      this.positions_colonnes.put(this.colonnes[i].nom, i);
    }
    
    this.adapter_noms_colonnes_ajoutees =
      ViewHelpers.standard_spinner_adapter_with(noms);
  }

  public void clef_primaire(char[] composantes, 
                                  char[] contraintes_choisies) {
    this.clef_primaire[0] = composantes;
    this.clef_primaire[1] = contraintes_choisies;
  }

  public CharSequence[] descriptions(GetText[] elements) {
    CharSequence[] descriptions = new CharSequence[elements.length];
    for (char i = 0; i < elements.length; i++)
      descriptions[i] = elements[i].getText();
    return descriptions;
  }

  public void clefs_etrangeres(ClefSecondaire[] nouvelles_fk) {
    this.clefs_etrangeres = nouvelles_fk;
  }
  
  public void supprimer_clefs_etrangeres(boolean[] a_supprimer,
                                         int n_a_supprimer) {
    clefs_etrangeres(
      supprimer_elements(a_supprimer, n_a_supprimer, 
                         this.clefs_etrangeres)
    );
  }
  
  public void supprimer_contraintes_unique(boolean[] a_supprimer,
                                           int n_a_supprimer) {
    this.contraintes_unique = 
      supprimer_elements(a_supprimer, n_a_supprimer,
        this.contraintes_unique);
  }

  public void modifier_contrainte_check(CheckConstraint contrainte,
                                        int element) {
    this.contraintes_check[element] = contrainte;
  }

  public void ajouter_contrainte_check(CheckConstraint contrainte) {
    this.contraintes_check = 
      ajouter_element(this.contraintes_check, contrainte);
  }
  
  public void supprimer_contraintes_check(boolean[] a_supprimer, 
                                          int n_a_supprimer) {
    this.contraintes_check =
      supprimer_elements(a_supprimer, n_a_supprimer,
        this.contraintes_check);
  }

  public static <T> T[] ajouter_element(T[] elements, T element) {
    int n_elements = elements.length;
    T[] nouveaux_elements = Arrays.copyOf(elements, n_elements+1);
    nouveaux_elements[n_elements] = element;
    return nouveaux_elements;
  }
  
  public static <T> T[] supprimer_elements(boolean[] a_supprimer,
                                           int n_a_supprimer,
                                           T[] elements) {
    int n_elements = a_supprimer.length;
    T[] nouveaux_elements =
      (T[]) Array.newInstance(elements.getClass().getComponentType(), 
                              n_elements - n_a_supprimer);

    int j = 0;
    for (int i = 0; i < n_elements; i++) 
      if (!a_supprimer[i]) nouveaux_elements[j++] = elements[i];

    return nouveaux_elements;
  }


  public boolean en_mode_edition() { 
    return element_edite != aucun_element_edite; 
  }

  public void element_edite(int nouvel_element) {
    this.element_edite = nouvel_element;
  }

  public void clef_etrangere_actuelle(ClefSecondaire nouvelle_fk) {
    if (element_edite == -1)
      this.clefs_etrangeres =
        ajouter_element(clefs_etrangeres, nouvelle_fk);
    else this.clefs_etrangeres[element_edite] = nouvelle_fk;
  }

  public ClefSecondaire clef_etrangere_actuelle() {
    if (en_mode_edition()) return this.clefs_etrangeres[element_edite];
    else return null;
  }
  
  public void contrainte_unique(int[] positions_colonnes) {

    if (!en_mode_edition())
      this.contraintes_unique = 
        ajouter_element(this.contraintes_unique, 
                        new ContrainteUnique(this, positions_colonnes));
    else
      this.contraintes_unique[element_edite].colonnes(positions_colonnes);
  }

  public String schema() {
    StringBuilder schema_sb = new StringBuilder(256);
    schema_sb.append("create table ");
    schema_sb.append(SQLiteHelpers.quote(this.nom_de_la_table));
    schema_sb.append("(\n");
    schema_sb.append(list(", ", this.colonnes));
    if (this.clef_primaire[0].length != 0) {
      schema_sb.append(",\n");
      schema_sb.append("PRIMARY KEY (");
      schema_sb.append(colonnes[clef_primaire[0][0]].nom_traité);
      if (this.clef_primaire[1][0] == 1)
        schema_sb.append(" AUTOINCREMENT");
      if (this.clef_primaire[0].length != 1) {
        schema_sb.append(",");
        schema_sb.append(
          liste_traitee(
            colonnes,
            Arrays.copyOfRange(this.clef_primaire[0],
                               1, this.clef_primaire[0].length)
          )
        );
      }
      schema_sb.append(")");

    }
    schema_sb.append(sub_list(this.clefs_etrangeres));
    schema_sb.append(sub_list(this.contraintes_unique));
    schema_sb.append(sub_list(this.contraintes_check));
    schema_sb.append("\n);");
    return schema_sb.toString();
  }

  public <T> String sub_list(T[] array) {   
    StringBuilder list_sb = new StringBuilder(32);
    for (int i = 0; i < array.length; i++) {
      list_sb.append(",\n");
      list_sb.append(array[i].toString());
    }
    return list_sb.toString();
  }

  public void fm(FragmentManager fm) {
    this.gestionnaire_fragment = 
      new WeakReference<FragmentManager>(fm);
  }

  public FragmentManager fm() { return gestionnaire_fragment.get(); }

  public void aller_a_la(WizardPage page, String nom_transaction) {
    fm().
      beginTransaction().addToBackStack(nom_transaction).
      replace(android.R.id.content, page, tag_actuel).commit();
  }
  
  public void aller_a_la(int page, String nom_de_la_transaction) {
    this.aller_a_la(pages_assistant[page], nom_de_la_transaction);
  }

  public void detour(int page, int etapes) {
    navigation_standard = false;
    this.page_de_retour = page_actuelle;
    this.tag_actuel  = detour;
    aller_a_la(page, detour);
    this.derniere_page = page + etapes;
  }

  public void revenir_a_une_navigation_standard() {
    revenir_avant(detour);
    this.tag_actuel     = tag_page;
  }

  public void revenir_avant(String nom_de_la_transaction) {
    fm().popBackStackImmediate(nom_de_la_transaction, FragmentManager.POP_BACK_STACK_INCLUSIVE);
  }
    
  public void navigation_standard() { 
    this.derniere_page = derniere_page_par_defaut;
    this.page_de_retour = this.pages_assistant.length+1;
    this.page_de_retour = 99;
    this.navigation_standard = true;
  }


  @Override
  public void on_end(WizardCaller callback) {
    this.rappel_fin = callback;
  }

  @Override
  public void next_step() {
    if (page_actuelle != derniere_page)
      aller_a_la(page_actuelle+1, null);
    else { 
      if (navigation_standard) assistance_terminee();
      else revenir_a_une_navigation_standard();
    }
  }

  @Override
  public void current_step(int page) {
    if (page == page_de_retour) navigation_standard();
    this.page_actuelle = page;
  }

  @Override
  public void previous_step() { fm().popBackStackImmediate(); }

  @Override
  public void stop() {}

  @Override
  public void start() { 
    aller_a_la(pages_assistant[0], "wizard_start");
    this.gestionnaire_fragment.get().
      addOnBackStackChangedListener(new SurveillanceBackStack());
    parametrage_demarrage();
  }

  public CharSequence[] descriptions_colonnes() {
    final int n_colonnes = this.colonnes.length;
    final CharSequence[] descriptions = new CharSequence[n_colonnes];
    for (int i = 0; i < n_colonnes; i++)
      descriptions[i] = this.colonnes[i].getText();

    return descriptions;
  }

  public class SurveillanceBackStack implements OnBackStackChangedListener {
  
    public char limite;
    public SurveillanceBackStack() {
      this.limite = (char) fm().getBackStackEntryCount();
    }

    @Override
    public void onBackStackChanged() {
      if (fm().getBackStackEntryCount() >= limite);
      else assistance_annulee();
    }
  }

  public void assistance_annulee() {
    this.rappel_fin.end(WizardCaller.CANCELLED); }

  public void assistance_terminee() {
    fm().popBackStackImmediate("wizard_start", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    this.rappel_fin.end(WizardCaller.SUCCESS);
  }

  public static final class CTColumn implements GetText {

    public static final byte not_null_i = 0;
    public static final byte default_i = 1;
    public static final String[] types =
      new String[]{ "INTEGER", "TEXT", "BLOB", "REAL", "NUMERIC" };;
    public static final String[] conflict_actions =
      new String[]{
        "",
        "ON CONFLICT ROLLBACK ", "ON CONFLICT ABORT ",
        "ON CONFLICT FAIL ", "ON CONFLICT IGNORE ",
        "ON CONFLICT REPLACE "
      };

    public boolean not_null;
    public int on_conflict;
    public byte type;
    public String valeur_par_defaut;
    public String nom;
    public String nom_traité;
    public String[] contraintes_etablies;

    public CTColumn(String nom, byte type) {
      this.type = type;
      this.nom  = nom;
      this.nom_traité = SQLiteHelpers.quote(this.nom);
      this.contraintes_etablies = new String[] {"", ""};
    }

    public String type() { return types[this.type]; }

    public void name(String new_name) {
      this.nom        = new_name;
      this.nom_traité = SQLiteHelpers.quote(this.nom);
    }

    public void not_null(boolean set, int not_null_action) {
      this.not_null = set;
      this.on_conflict = not_null_action;
      if (set)
        contraintes_etablies[not_null_i] = 
          StringHelpers.concat(
            "NOT NULL ", conflict_actions[not_null_action]
          );
      else contraintes_etablies[not_null_i] = "";
    }

    public void valeur_par_defaut(@Nullable String nouvelle_valeur) {
      this.valeur_par_defaut = nouvelle_valeur;
      if (nouvelle_valeur != null)
        this.contraintes_etablies[default_i] =
          StringHelpers.concat(
            "DEFAULT ",
            SQLiteHelpers.quote(nouvelle_valeur)
          );
      else this.contraintes_etablies[default_i] = "";
    }

    public String toString() {
      return String.format("%s %s %s",
                           this.nom_traité,
                           this.type(), this.contraintes());
    }

    public CharSequence getText() {

      final int taille_nom = this.nom.length();

      SpannableString spanou =
        new SpannableString(
          String.format("%s\n%s %s",
            this.nom, this.type(), this.contraintes())
        );

      spanou.setSpan(styles_texte[principal], 0, taille_nom, 0);
      spanou.setSpan(styles_texte[attributs],
                      taille_nom+1, spanou.length(), 0);

      return spanou;
    }

    public String contraintes() {
      return StringHelpers.concat(contraintes_etablies);
    }
  }

  public String liste_traitee(CTColumn[] elements, char[] elements_desires) {
    StringBuilder sb = new StringBuilder(elements[elements_desires[0]].nom_traité);

    for (int i = 1; i < elements_desires.length; i++) {
      sb.append(',');
      sb.append(elements[elements_desires[i]].nom_traité);
    }

    return sb.toString();
  }

  public String liste_traitee(Column[] elements, char[] elements_desires) {
    StringBuilder sb = new StringBuilder(elements[elements_desires[0]].quoted_name);

    for (int i = 1; i < elements_desires.length; i++) {
      sb.append(',');
      sb.append(elements[elements_desires[i]].quoted_name);
    }

    return sb.toString();
  }

  public String liste(CTColumn[] elements, char[] elements_desires) {
    StringBuilder sb = new StringBuilder(elements[elements_desires[0]].nom);

    for (int i = 1; i < elements_desires.length; i++) {
      sb.append(", ");
      sb.append(elements[elements_desires[i]].nom);
    }

    return sb.toString();
  }

  public String liste(Column[] elements, char[] elements_desires) {
    StringBuilder sb =
      new StringBuilder(elements[elements_desires[0]].name);

    for (int i = 1; i < elements_desires.length; i++) {
      sb.append(", ");
      sb.append(elements[elements_desires[i]].name);
    }

    return sb.toString();
  }



  public abstract class ClefSecondaire implements GetText {
    public boolean table_future;
    public boolean deferrable;
    public char[] enfants;
    public byte[] clauses;
    public abstract CharSequence getText();
    public StringBuilder clauses_sb;
    public ClefSecondaire() {
      clauses = new byte[2];
      clauses_sb = new StringBuilder(128);
    }
    public String clauses() {
      clauses_sb.delete(0, 0x7fffffff);
      if (clauses[0] == 0);
      else {
        clauses_sb.append("ON UPDATE ");
        clauses_sb.append(clauses_clef_etrangere[clauses[0]]);
      }
      if (clauses[1] == 0);
      else {
        clauses_sb.append("ON DELETE ");
        clauses_sb.append(clauses_clef_etrangere[clauses[1]]);
      }
      return clauses_sb.toString();
    }
  }

  public final class ClefSecondairePresente extends ClefSecondaire {
    public char table_parente;
    public char[] parents;

    public ClefSecondairePresente(char[] enfants, char table_parente,
                                  char[] parents) {
      this.table_future  = false;
      this.enfants       = enfants;
      this.table_parente = table_parente;
      this.parents       = parents;
    }

    public String toString() {
      return String.
        format("FOREIGN KEY (%s) REFERENCES %s(%s) %s",
          liste_traitee(colonnes, enfants),
          SQLiteHelpers.quote(noms_tables[table_parente]),
          liste_traitee(pks_tables[table_parente].composantes(), parents),
          clauses());
    }

    public CharSequence getText() {
      return ViewHelpers.span_lines(
        String.
          format("%s\n%s\n%s\n%s",
                 liste(colonnes, enfants),
                 noms_tables[table_parente],
                 liste(pks_tables[table_parente].composantes(), parents),
                 clauses()),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_principal),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_attribut),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_principal),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_attribut)
      );
    }
  }

  public final class ClefSecondaireFuture extends ClefSecondaire {
    public String table_parente;
    public String[] parents;

    public ClefSecondaireFuture(char[] enfants, String table_parente,
                                String[] parents) {
      this.table_future  = true;
      this.enfants       = enfants;
      this.table_parente = table_parente;
      this.parents       = parents;
    }

    public String toString() {
      return String.format("FOREIGN KEY (%s) REFERENCES %s(%s) %s",
                           liste_traitee(colonnes, enfants),
                           SQLiteHelpers.quote(table_parente),
                           SQLiteHelpers.quote(parents),
                           clauses());
    }

    public CharSequence getText() {
      return ViewHelpers.span_lines(
        String.format("%s\n%s\n%s\n%s",
                      liste(colonnes, enfants),
                      table_parente,
                      StringHelpers.list(", ", parents),
                      clauses()),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_principal),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_attribut),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_principal),
        new TextAppearanceSpan(Application.contexte(), R.style.texte_attribut)
      );
    }

  }
  public interface GetText {
    CharSequence getText();
  }
}
