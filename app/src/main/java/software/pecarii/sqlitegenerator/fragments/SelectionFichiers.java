package software.pecarii.sqlitegenerator.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import static android.content.DialogInterface.OnClickListener;
import static android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.ads.Eticilbup;
import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.adapters.FichiersBDD;
import software.pecarii.sqlitegenerator.helpers.ExternalDatabases;
import software.pecarii.sqlitegenerator.helpers.ExternalStorage;
import software.pecarii.sqlitegenerator.helpers.FileHelpers;
import software.pecarii.sqlitegenerator.helpers.FileScanner;
import software.pecarii.sqlitegenerator.helpers.StringHelpers;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;

public final class SelectionFichiers extends ToolbarFragment {

  public static final Intent manipulation_bdd;
  public static final String package_name =
    "software.pecarii.sqlitegenerator.fragments.SelectionFichiers";
  public static final String extension_bdd = ".sqlite";
  public static final File dossier_bdd;
  public static final String chemin_dossier_bdd;

  static {
    manipulation_bdd = 
      new Intent(Application.contexte(), ManipulationBDD.class);
    dossier_bdd =
      new File(ExternalStorage.directory().getAbsolutePath(), "SQL-DB");
    if (!dossier_bdd.exists()) { dossier_bdd.mkdir(); }
    chemin_dossier_bdd = dossier_bdd.getAbsolutePath();
  }

  public int coches;
  public byte prochain_etat;
  public boolean[] fichiers_selectionnés;
  public Button bouton_demarrer_recherche;
  public FichiersBDD adaptateur_liste_des_fichiers;
  public EditText zone_nom_nouvelle_bdd;
  public Dialog popup_creation_bdd;
  public Dialog popup_scan_sdcard;
  public Dialog popup_suppression_tables;
  public Dialog popup_dernier_avertissement;
  public Dialog popup_sdcard_necessaire;
  public Dialog popup_probleme_ecriture_sdcard;
  public LabelledCheckbox coche_accepte_suppression;
  public LinearLayout vue_popup_creation_bdd;
  public LinearLayout vue_popup_avertissement;
  public LinearLayout vue_popup_dossier_recherche;
  public RecyclerView liste_des_fichiers;
  public Spinner choix_dossier_recherche;
  public Thread thread_recherche;
  public SQLFileScanner chercheur_fichiers_sql;
  public TextView zone_dernier_avertissement;
  public TextView zone_chemin_recherche;
  public View bup;
  public File[] bdd_locales;
  public String[] noms_bdd_locales;
  public boolean tout_est_ok;

  @Override
  public boolean action_clicked(@IdRes int action_id) {
    switch(action_id) {
      case R.id.creer_nouvelle_bdd: 
        this.popup_creation_bdd.show(); 
        return true;
      case R.id.scan_complet_sdcard:
        this.vue_popup_dossier_recherche.setVisibility(View.VISIBLE);
        return true;
      case R.id.supprimer_bdds:
        afficher_popup_suppression_bdd();
        return true;
      default: return false;
    }
  }

  @Override
  public void resources_setup() {
    this.main_menu = R.menu.actions_liste_bdd;
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater gonfleur)
  {

    inflate_in_content(R.layout.fragment_selection_fichiers);

    this.liste_des_fichiers =
      (RecyclerView) find_in_content(R.id.selecteur_de_fichiers);
    this.vue_popup_creation_bdd =
      (LinearLayout)
        gonfleur.inflate(R.layout.popup_creation_bdd, null, false);
    this.vue_popup_avertissement =
      (LinearLayout)
        gonfleur.inflate(R.layout.popup_avertissement_avec_coche, null, false);
    this.zone_dernier_avertissement =
      (TextView)
        find(R.id.message_avertissement, vue_popup_avertissement);
    this.zone_nom_nouvelle_bdd =
      (EditText) find(R.id.zone_nom_nouvelle_bdd, vue_popup_creation_bdd);
    this.coche_accepte_suppression =
      (LabelledCheckbox)
        find(R.id.coche_avertissement_compris, vue_popup_avertissement);
    this.vue_popup_dossier_recherche =
      (LinearLayout) find_in_content(R.id.popup_dossier_recherche);
    this.bouton_demarrer_recherche =
      (Button)
        find(R.id.bouton_demarrer_recherche, vue_popup_dossier_recherche);
    this.choix_dossier_recherche =
      (Spinner)
        find(R.id.choix_dossier_recherche, vue_popup_dossier_recherche);
    this.zone_chemin_recherche =
      (TextView)
        find(R.id.zone_chemin_recherche, vue_popup_dossier_recherche);
    this.adaptateur_liste_des_fichiers =
      new FichiersBDD(gonfleur, new Nya());

    this.bup = find_in_content(R.id.bup);
  }

  @Override
  public void setup_content_views() {
    DialogProperties props_popup_sdcard =
      new DialogProperties(
        new int[] { R.string.titre_sdcard_necessaire,
                    R.string.explication_sdcard_necessaire },
        new int[] { R.string.reessayer,
                    R.string.quitter_application});
    DialogProperties props_popup_probleme_ecriture =
      new DialogProperties(
        new int[] { R.string.titre_probleme_ecriture,
                    R.string.explication_probleme_ecriture },
        new int[] { R.string.reessayer,
                    R.string.quitter_application });
    DialogProperties props_popup_scan_sd =
      new DialogProperties(
        new int[] { R.string.titre_scan_sd,
                    R.string.explication_full_scan_sd },
        new int[] { android.R.string.yes,
                   android.R.string.no });

    this.zone_dernier_avertissement.
      setText(R.string.avertissement_suppression_fichiers);
    this.coche_accepte_suppression.
      setText(R.string.coche_accepte_suppression);

    this.popup_creation_bdd =
      ViewHelpers.create_dialog(this.getActivity(),
                                vue_popup_creation_bdd,
                                new int[] {R.string.creer_bdd, 0,
                                           R.string.ne_rien_faire},
                                new PopupCreationBDD());
    this.popup_creation_bdd.setTitle(R.string.nouvelle_bdd);
      /*create_dialog(vue_popup_creation_bdd, new PopupCreationBDD(),
        props_popup_creation_bdd);*/

    this.popup_dernier_avertissement =
      ViewHelpers.create_dialog(this.getActivity(),
        vue_popup_avertissement,
        new int[]{R.string.supprimer_bdds_selectionnees, 0,
          R.string.ne_rien_faire},
        new SuppressionFichiers());
    this.popup_dernier_avertissement.setTitle(R.string.dernier_avertissement);

    this.popup_scan_sdcard =
      this.create_dialog(null, props_popup_scan_sd,
                         "demande_scan_sdcard");
    this.popup_sdcard_necessaire =
      this.create_dialog(null, props_popup_sdcard,
                         "probleme_lecture_sdcard");
    this.popup_probleme_ecriture_sdcard =
      this.create_dialog(null, props_popup_probleme_ecriture,
                         "probleme_ecriture_sdcard");

    liste_des_fichiers.setHasFixedSize(true);
    liste_des_fichiers.setLayoutManager(new LinearLayoutManager(getActivity()));
    liste_des_fichiers.setAdapter(adaptateur_liste_des_fichiers);


    if (ExternalStorage.readable()) {
      if (ExternalStorage.writeable()) tout_est_ok = true;
      else popup_probleme_ecriture_sdcard.show();
    }
    else popup_sdcard_necessaire.show();

    if (tout_est_ok); else return;

    SelectionDossier dispatcher_recherche_dossier =
      new SelectionDossier();
    this.choix_dossier_recherche.setOnItemSelectedListener(
      dispatcher_recherche_dossier
    );
    this.bouton_demarrer_recherche.setOnClickListener(
      dispatcher_recherche_dossier
    );

    this.chercheur_fichiers_sql = new SQLFileScanner();
    rechercher_bdd_dans(chemin_dossier_bdd);

    Eticilbup.tutup(bup, "SQL");
  }

  final class SelectionDossier
    implements Spinner.OnItemSelectedListener, View.OnClickListener {

    final String[] dossiers;
    byte selectionne;

    SelectionDossier() {
      dossiers = new String[] {
        chemin_dossier_bdd,
        ExternalStorage.path,
        "/data/data",
        "/"
      };
    }
    @Override
    public void onItemSelected(AdapterView a, View v, int p, long i) {
      selectionne = (byte) p;
      if (p != 3) zone_chemin_recherche.setVisibility(View.GONE);
      else zone_chemin_recherche.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNothingSelected(AdapterView a) {}

    @Override
    public void onClick(View v) {
      dossiers[3] = zone_chemin_recherche.getText().toString();
      rechercher_bdd_dans(dossiers[selectionne]);
    }
  }

  final public class Nya implements View.OnClickListener {
    public void ouvrir_bdd(final File fichier) {
      manipulation_bdd.putExtra(package_name,
                                fichier.getAbsolutePath());
      startActivity(manipulation_bdd);
      manipulation_bdd.removeExtra(package_name);
    }

    public void onClick(View v) { ouvrir_bdd((File) v.getTag()); }
  }

  @Override
  public void cleanup() {
    this.adaptateur_liste_des_fichiers = null;
    this.bdd_locales = null;
    this.noms_bdd_locales = null;
    Eticilbup.dling(bup);
  }

  class SQLFileScanner extends FileScanner {

    @Override
    public boolean good(File file) { return true; }

    @Override
    public void report(final File[] files) {
      final File[] sqlite_files =
        ExternalDatabases.sqlite_files_in(files);
      SelectionFichiers.this.getActivity().runOnUiThread(
        new Runnable() {
          @Override
          public void run() {
            adaptateur_liste_des_fichiers.ajouter(sqlite_files);
          }
        }
      );
    }

    @Override
    public void final_report(final File[] files) {
      report(files);
      traiter_bdd_locales();
    }
  }

  public void
  demande_scan_sdcard(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) scanner_sdcard();
  }

  public void rechercher_bdd_dans(final String chemin) {
    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        adaptateur_liste_des_fichiers.reset();
      }
    });
    this.thread_recherche = new Thread() {
      @Override
      public void run() {
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() {
            show_flash_area_with(R.string.recherche_bdd_en_cours);
          }
        });
        chercheur_fichiers_sql.scan(new File(chemin));
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() { hide_flash_area(); }
        });
      }
    };
    this.thread_recherche.setPriority(Thread.MIN_PRIORITY);
    this.thread_recherche.start();
  }

  public void stop_recherche_actuelle() {
    this.thread_recherche.interrupt();
  }

  public void lancer_nouvelle_recherche(final String chemin) {
    stop_recherche_actuelle();
    new Thread() {
      @Override
      public void run() {
        try {
          while (thread_recherche.getState() !=
            Thread.State.TERMINATED) {
            thread_recherche.interrupt();
            Thread.sleep(1000);
          }
        }
        catch (InterruptedException e) {}
        finally {
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              vider_la_liste();
            }
          });
          rechercher_bdd_dans(chemin);
        }
      }
    }.start();
  }

  public void scanner_dossier_bdd() {
    lancer_nouvelle_recherche(chemin_dossier_bdd);
  }

  public void scanner_sdcard() {
    lancer_nouvelle_recherche(ExternalStorage.path());
  }

  public void vider_la_liste() {
    this.adaptateur_liste_des_fichiers.reset();
  }

  void traiter_bdd_locales() {
    final File[] fichiers_sqlite =
      this.adaptateur_liste_des_fichiers.bdd_gérées();
    final File[] bdd_locales = new File[fichiers_sqlite.length];
    int n_locales = 0;
    for (File fichier_sql : fichiers_sqlite)
      if (fichier_sql.getAbsolutePath().startsWith(chemin_dossier_bdd))
        bdd_locales[n_locales++] = fichier_sql;

    final String[] noms_bdd_locales = new String[n_locales];
    for (int i = 0; i < n_locales; i++)
      noms_bdd_locales[i] = bdd_locales[i].getName();

    this.bdd_locales = bdd_locales;
    this.noms_bdd_locales = noms_bdd_locales;

  }

  public void afficher_popup_suppression_bdd() {
    this.fichiers_selectionnés =
      new boolean[this.noms_bdd_locales.length];
    this.coches = 0;
    this.popup_suppression_tables =
      ViewHelpers.create_dialog(this.getActivity(),
        this.noms_bdd_locales,
        new CompteCoches(),
        new int[] {R.string.supprimer, 0,
          R.string.ne_rien_faire},
        new ActionListeFichiersSuppression());
    this.popup_suppression_tables.setTitle(R.string.titre_popup_suppression_fichiers);
    this.popup_suppression_tables.show();
  }

  public void
  probleme_lecture_sdcard(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) {
      if (ExternalStorage.readable()) {
        if (ExternalStorage.writeable())
          scanner_dossier_bdd();
        else {
          dialog.dismiss();
          popup_probleme_ecriture_sdcard.show();
        }
      }
      else {
        dialog.dismiss();
        popup_sdcard_necessaire.show();
      }
    }
    else {
      this.getActivity().finish();
      System.exit(1);
    }
  }

  public void
  probleme_ecriture_sdcard(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) {
      if (ExternalStorage.writeable()) scanner_dossier_bdd();
      else popup_probleme_ecriture_sdcard.show();
    }
    else {
      this.getActivity().finish();
      System.exit(1);
    }
  }

  public static final int[] erreurs = new int[] {
    R.string.bdd_creee, R.string.bdd_non_creee_fichier_existe,
    R.string.bdd_creee_probleme_reouverture
  };

  public void creer_bdd(String nom_saisi) {
    String nom_avec_extension =
      StringHelpers.suffix(nom_saisi, extension_bdd);
    Toast.makeText(getActivity(),
      erreurs[ExternalDatabases.create_database(nom_avec_extension)],
      Toast.LENGTH_LONG).show();
  }

  public class PopupCreationBDD
    implements DialogInterface.OnClickListener {
    public void onClick(DialogInterface dialog, int bouton_clique) {
      if (bouton_clique == DialogInterface.BUTTON_POSITIVE) {
        final String nom_bdd = zone_nom_nouvelle_bdd.getText().toString();
        if (!nom_bdd.isEmpty()) {
          creer_bdd(zone_nom_nouvelle_bdd.getText().toString());
          scanner_dossier_bdd();
        }
        else Toast.makeText(getActivity(),
                            R.string.avertissement_nom_vide,
                            Toast.LENGTH_LONG).show();
      }
      zone_nom_nouvelle_bdd.setText("");
    }
  }

  public class CompteCoches implements OnMultiChoiceClickListener {
    @Override
    public void onClick(DialogInterface dialog, int position,
                        boolean coche) {
      fichiers_selectionnés[position] = coche;
      if (coche) coches++; else coches--;
    }
  }

  public class ActionListeFichiersSuppression implements OnClickListener {
    @Override
    public void onClick(DialogInterface dialog, int bouton_clique) {
      if (bouton_clique == positive_button)
          popup_dernier_avertissement.show();
    }
  }

  public class SuppressionFichiers implements OnClickListener {
    @Override
    public void onClick(DialogInterface dialog, int bouton_clique) {
      if (bouton_clique == positive_button) {
        if (coche_accepte_suppression.isChecked()) {

          File journal;
          File[] fichiers_a_supprimer = new File[coches*2];

          for (char i = 0, i_fichiers = 0,
               n_fichiers = (char) fichiers_selectionnés.length;
               i < n_fichiers; i++) {
            if (fichiers_selectionnés[i]) {
              fichiers_a_supprimer[i_fichiers++] = bdd_locales[i];
              journal = new File(
                StringHelpers.concat(
                  bdd_locales[i].getAbsolutePath()+"-journal"
                )
              );
              fichiers_a_supprimer[i_fichiers++] = journal;
            }
          }

          FileHelpers.delete_files(fichiers_a_supprimer);
          scanner_dossier_bdd();
        }
        else
          Toast.makeText(
            Application.contexte(),
            R.string.coche_avertissement_non_cochee,
            Toast.LENGTH_LONG
          ).show();
      }
      coche_accepte_suppression.setChecked(false);
    }
  }
}
