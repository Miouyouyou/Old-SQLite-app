package software.pecarii.sqlitegenerator.fragments.wizards;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable.CTColumn;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.SpinnerSMD;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ViewSMD;
import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;

public final class CreationTableNom extends PageListe {

  public int element_edite;
  public Dialog popup_parametrage_colonne;
  public RelativeLayout vue_popup_colonne;
  public EditText zone_nom_colonne;
  public EditText zone_valeur_par_defaut;
  public Spinner choix_type_colonne;
  public Spinner choix_action_not_null;
  public LabelledCheckbox coche_non_null;
  public LabelledCheckbox coche_valeur_par_defaut;
  public TextView zone_nom_nouvelle_table;

  public CreationTableNom() { 
    super();
    this.subtitle = R.string.ct_page_nom;
    this.layout_complet = R.layout.assistant_creation_table_nom;
    this.id_liste       = R.id.liste_colonnes;
    this.lower_menu = R.menu.creation_table_nom_table;
    this.reason_cant_go_next = R.string.notification_ct_colonnes_necessaires;
  }

  @Override
  public CharSequence[] elements() {
    return wizard.descriptions_colonnes();
  }

  @Override
  public void vers_interface_creation_edition(int element) {
    show_popup_colonne(element);
  }

  @Override
  public void supprimer_elements() {
    wizard.supprimer_colonnes(this.coches, this.elements_coches);
  }

  public void show_popup_colonne(int element) {
    this.element_edite = element;
    if (element == -1); // Most likely
    else parametrer_popup_parametrage(element); // Less likely
    popup_parametrage_colonne.show();
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {
    super.initialise_content_views(inflater);
    View.OnClickListener gestion_coches = new ViewSMD("coches", this);
    AdapterView.OnItemSelectedListener gestion_type =
      new SpinnerSMD("choix_type", this);

    this.zone_nom_nouvelle_table =
      (TextView)
        this.content.findViewById(R.id.zone_nom_nouvelle_table);

    this.vue_popup_colonne =
      (RelativeLayout)
        main_inflater.inflate(R.layout.popup_parametrage_colonne,
          null, false);
    this.zone_nom_colonne =
      (EditText) find(R.id.zone_nom_colonne, this.vue_popup_colonne);
    this.choix_type_colonne =
      (Spinner) find(R.id.choix_type_colonne, this.vue_popup_colonne);
    this.coche_non_null =
      (LabelledCheckbox) 
        find(R.id.coche_non_null, this.vue_popup_colonne);
    this.choix_action_not_null =
      (Spinner) find(R.id.choix_resolution_conflit, this.vue_popup_colonne);
    this.coche_valeur_par_defaut =
      (LabelledCheckbox)
        find(R.id.coche_valeur_par_defaut, this.vue_popup_colonne);
    this.zone_valeur_par_defaut =
      (EditText) 
        find(R.id.zone_valeur_par_defaut, this.vue_popup_colonne);

    this.zone_valeur_par_defaut.setInputType(CreationTable.modes_editions[0]);
    this.coche_valeur_par_defaut.setOnClickListener(gestion_coches);
    this.coche_non_null.setOnClickListener(gestion_coches);
    this.choix_type_colonne.setOnItemSelectedListener(gestion_type);

    reset_popup_parametrage();
    afficher_liste_elements();
  }

  @Override
  public void setup_content_views() {
    super.setup_content_views();
    
    DialogProperties props_popup = 
      new DialogProperties(
        new int[] {R.string.titre_parametrage_colonnes},
        new int[] {R.string.appliquer, R.string.ne_rien_faire}
      );
    this.popup_parametrage_colonne = 
      create_dialog(this.vue_popup_colonne, props_popup, 
                    "action_popup");


  }

  public void
  choix_type(boolean selection, AdapterView liste, View element,
               int position, long id) {
    if (selection) {
      this.zone_valeur_par_defaut
        .setInputType(CreationTable.modes_editions[position]);
    }
  }

  public void action_popup(DialogInterface popup, int bouton_clique) {
    switch(bouton_clique) {
      case positive_button: 
        CTColumn nouvelle_colonne =
          new CTColumn(zone_nom_colonne.getText().toString(),
                     (byte) choix_type_colonne.getSelectedItemPosition());
        /*System.err.println(nouvelle_colonne.nom);*/
        if (coche_non_null.isChecked())
          nouvelle_colonne.not_null(
            true, choix_action_not_null.getSelectedItemPosition()
          );
        if (coche_valeur_par_defaut.isChecked())
          nouvelle_colonne.valeur_par_defaut(zone_valeur_par_defaut.getText().toString());
        if (element_edite == -1) {
          wizard.nouvelle_colonne(nouvelle_colonne);
          reset_popup_parametrage();
          afficher_liste_elements();
          break;
        }
        else {
          wizard.remplacer_colonne(element_edite, nouvelle_colonne);
          afficher_liste_elements();
        }

      case dismiss:
        if (element_edite == -1) break;
      case negative_button:
        reset_popup_parametrage();
        break;
    }
  }
  
  public void coches(View v) {
    LabelledCheckbox coche = (LabelledCheckbox) v;
    switch(v.getId()) {
      case R.id.coche_non_null:
        coche_non_null.toggle();
        choix_action_not_null.setEnabled(coche.isChecked());
        break;
      case R.id.coche_valeur_par_defaut:
        coche_valeur_par_defaut.toggle();
        zone_valeur_par_defaut.setEnabled(coche.isChecked());
        break;
    }
  }
  
  public void reset_popup_parametrage() {
    zone_nom_colonne.setText("");
    zone_valeur_par_defaut.setText("");
    zone_valeur_par_defaut.setEnabled(false);
    zone_valeur_par_defaut.setInputType(CreationTable.modes_editions[0]);
    choix_type_colonne.setSelection(0);
    choix_action_not_null.setSelection(0);
    choix_action_not_null.setEnabled(false);
    coche_non_null.setChecked(false);
    coche_valeur_par_defaut.setChecked(false);
  }
  
  public void parametrer_popup_parametrage(int element) {
    CTColumn colonne = wizard.colonnes[element];
    zone_nom_colonne.setText(colonne.nom);
    choix_type_colonne.setSelection(colonne.type);
    coche_non_null.setChecked(colonne.not_null);
    choix_action_not_null.setEnabled(colonne.not_null);
    if (choix_action_not_null.isEnabled())
      choix_action_not_null.setSelection(colonne.on_conflict);
    if (colonne.valeur_par_defaut != null) {
      coche_valeur_par_defaut.setChecked(true);
      zone_valeur_par_defaut.setEnabled(true);
      zone_valeur_par_defaut.setText(colonne.valeur_par_defaut);
      zone_valeur_par_defaut.setInputType(CreationTable.modes_editions[colonne.type]);
    }
  }

  @Override
  public boolean canGoNext() {
    return wizard.colonnes.length > 0;
  }

  @Override
  public void onNext() {
    wizard.
      nom_de_la_table(this.zone_nom_nouvelle_table.getText().toString());
  }
}
