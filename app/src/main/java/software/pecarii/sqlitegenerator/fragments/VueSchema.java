package software.pecarii.sqlitegenerator.fragments;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.fragments.ToolbarFragment;

public class VueSchema extends ToolbarFragment {

  public TextView zone_schema;

  public VueSchema() {
    this.title = R.string.vue_schema;
    this.navigation_drawable = R.drawable.ic_arrow_back_white_24dp;
    this.main_menu = R.menu.test_menu;
  }

  @Override
  public void navigation(View v) {
    ((ManipulationBDD) getActivity()).interface_liste_tables();
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater gonfleur) {
    super.initialise_content_views(gonfleur);
    inflate_in_content(R.layout.fragment_vue_schema);

    this.zone_schema = (TextView) find_in_content(R.id.zone_schema);
  }

  @Override
  public void setup_content_views() {
    super.setup_content_views();
    this.main_toolbar.setSubtitle(
      ((ManipulationBDD) getActivity()).table_editee
    );
    this.zone_schema.setText(
      ((ManipulationBDD) getActivity()).schema_table_actuelle()
    );
  }
}
