package software.pecarii.sqlitegenerator.fragments.wizards;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.application.Application;
import software.pecarii.sqlitegenerator.fragments.wizards.CreationTable;
import software.pecarii.sqlitegenerator.fragments.wizards.PageListe;
import software.pecarii.sqlitegenerator.vues.ContrainteCheck;
import static software.pecarii.sqlitegenerator.vues.ContrainteCheck.CheckConstraint;
import static software.pecarii.sqlitegenerator.helpers.StringHelpers.toString_array;

public final class CreationTableListeContraintesCheck
  extends PageListe {

  public boolean edition;
  public int element_edite;
  public Dialog popup_ajout_contrainte;
  public ContrainteCheck interface_popup;
  

  public CreationTableListeContraintesCheck() {
    super();
    this.title = R.string.ct_page_contraintes_check;
  }
  
  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {
    super.initialise_content_views(inflater);
    this.interface_popup = 
      (ContrainteCheck)
        inflater.inflate(R.layout.popup_contrainte_check,
                         null, false);
    this.interface_popup.assistant(wizard);
    popup_ajout_contrainte =
      create_dialog(this.interface_popup,
                    new int[] {R.string.generer_contrainte, 0,
                               R.string.ne_rien_faire},
                    new DecisionsPopupContrainte());
  }
  
  @Override
  public void resources_setup() {
    super.resources_setup();
    this.texte_bouton_ajout = R.string.ajouter_contrainte_check;
    this.texte_bouton_retrait = R.string.supprimer_contraintes_selectionees;
  }
  
  public class DecisionsPopupContrainte 
    implements DialogInterface.OnClickListener {
    @Override
    public void onClick(DialogInterface dialog, int which) {
      switch(which) {
        case DialogInterface.BUTTON_POSITIVE :
          CheckConstraint contrainte = interface_popup.generer_objet();
          if (edition)
            wizard.modifier_contrainte_check(contrainte, element_edite);
          else wizard.ajouter_contrainte_check(contrainte);
          afficher_liste_elements();
        case DialogInterface.BUTTON_NEGATIVE :
          interface_popup.reset();
          break;
      }
    } 
  } 
       
  
  public String[] elements() { 
    return toString_array((Object) wizard.contraintes_check);
  }
  
  @Override
  public void vers_interface_creation_edition(int element) {
    this.edition = (element != -1);
    this.element_edite = element;
    if (this.edition) 
      interface_popup.afficher(wizard.contraintes_check[element]);
     
    popup_ajout_contrainte.show();
  }
  
  public void supprimer_elements() {
    wizard.supprimer_contraintes_check(elements_coches, coches);
  }
}
