package software.pecarii.sqlitegenerator.fragments.wizards;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

import software.pecarii.sqlitegenerator.R;

import software.pecarii.sqlitegenerator.helpers.views.LabelledCheckbox;

public final class CreationTableClefsPrimaires extends PageDumbMenuList {

  public boolean[] colonnes_selectionnees;
  public int[]     ordre_de_selection;
  public LabelledCheckbox coche_autoincrement;

  public CreationTableClefsPrimaires() { 
    super();
    this.subtitle   = R.string.ct_page_clef_primaire;
    this.lower_menu = R.menu.wizard_default_dumb_menu;
    this.layout_res = R.layout.assistant_creation_table_clef_primaire;
  }

  @Override
  public void initialise_content_views(LayoutInflater inflater) {
    super.initialise_content_views(inflater);
    this.coche_autoincrement =
      (LabelledCheckbox) find(R.id.coche_autoincrement, this.content);
    afficher_pk();
  }

  @Override
  public void parametrage_ressources() {
    this.nombre_limite_de_menus = wizard.colonnes.length;
    this.texte_raison_limite = 
      R.string.composants_limites_a_colonnes_cp;
    this.reason_cant_go_next  = R.string.doublons_cp;   
  }
  
  @Override
  public View menu(int elem_selectionne) {
    return wizard.spinner_colonnes(elem_selectionne);
  }
  
  @Override
  public boolean canGoNext() {
    this.colonnes_selectionnees = 
      new boolean[wizard.colonnes.length];
    this.ordre_de_selection = new int[vues_ajoutees];
    int element_selectionne = 0;

    for (int i = 0; i < vues_ajoutees; i++) {
      element_selectionne =
        ((Spinner) menu_position(i)).getSelectedItemPosition();
      if (this.colonnes_selectionnees[element_selectionne])
        return false;

      this.colonnes_selectionnees[element_selectionne] = true;
      this.ordre_de_selection[i] = element_selectionne;
    }
    return true;
  }
 
  public void afficher_pk() {
    final char n_composantes = (char) wizard.clef_primaire[0].length;
    if (n_composantes == 0) ajouter_un_menu(0);
    else {
      for (char i = 0; i < n_composantes; i++)
        ajouter_un_menu(wizard.clef_primaire[0][i]);
      coche_autoincrement.setChecked(wizard.clef_primaire[1][0] == 1);
    }
  }

  @Override
  public void onNext() {
    final char[] composantes = new char[vues_ajoutees];
    final char[] contraintes = new char[1];
    
    for (int i = 0; i < vues_ajoutees; i++)
      composantes[i] = (char) ordre_de_selection[i];
    if (!coche_autoincrement.check) contraintes[0] = 0;
    else contraintes[0] = 1;

    wizard.clef_primaire(composantes, contraintes);
  }
  
}
