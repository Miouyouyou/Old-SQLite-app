package software.pecarii.sqlitegenerator.fragments;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Stack;

import software.pecarii.sqlitegenerator.ManipulationBDD;
import software.pecarii.sqlitegenerator.R;
import software.pecarii.sqlitegenerator.ads.Eticilbup;
import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;
import software.pecarii.sqlitegenerator.helpers.ViewHelpers;
import software.pecarii.sqlitegenerator.helpers.dispatchers.statics.ViewSMD;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.vues.table.ChampEditable;
import software.pecarii.sqlitegenerator.vues.table.Ligne;


public class VueTable extends ToolbarFragment
  implements ChampEditable.Callback {

  public final static byte set_null    = 2;
  public final static byte set_default = 1;
  public final static byte insert_row  = 0;
  public final static int padding_action;
  public final static int padding_inferieur;
  public final static int[] modes_editions = {
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED,
    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS,
    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS,
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED,
    InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED
  };

  public static final int[] fonds_colonnes =
    new int[] { 0xffffffff, 0xffdddddd };

  public final static DialogProperties props_suppression_ligne =
    new DialogProperties(
      new int[] {R.string.titre_suppression_ligne,
                 R.string.avertissement_suppression_ligne},
      new int[] {android.R.string.yes, android.R.string.no});
  public final static DialogProperties props_selection_correspondance =
    new DialogProperties(
      new int[] {R.string.titre_choix_colonne_correspondante},
      new int[] {R.string.afficher_correspondance, R.string.ne_rien_faire});
  public final static String clause_where = "rowid = ?";
  public final static ContentValues parametres_requete =
    new ContentValues(5);
  public final static Method[] methodes_parametres_requete;

  static {
    try {
      /*
       Agencés en fonction des types déclarés dans SQLiteHelpers.
       */
      methodes_parametres_requete = new Method[] {
        Parametres.class.getMethod("putInt",    String.class, String.class),
        Parametres.class.getMethod("putString", String.class, String.class),
        Parametres.class.getMethod("putBlob",   String.class, String.class),
        Parametres.class.getMethod("putDouble", String.class, String.class),
        Parametres.class.getMethod("putDouble", String.class, String.class)
      };
    }
    catch (NoSuchMethodException e) { throw new RuntimeException(e); }
    padding_action = ViewHelpers.dp_to_px(9);
    padding_inferieur = ViewHelpers.dp_to_px(3);
  }

  public static class Parametres {
    public static void putInt(String colonne, String valeur) {
      if (valeur != null && valeur.matches("\\d+"))
        parametres_requete.put(colonne, Integer.valueOf(valeur));
      else parametres_requete.put(colonne, (Integer) null);
    }
    public static void putString(String colonne, String valeur) {
      parametres_requete.put(colonne, valeur);
    }
    public static void putBlob(String colonne, String valeur) {
      parametres_requete.put(colonne, valeur.getBytes());
    }
    public static void putDouble(String colonne, String valeur) {
      if (valeur != null && valeur.matches("\\d+"))
        parametres_requete.put(colonne, Double.valueOf(valeur));
      else parametres_requete.put(colonne, (Double) null);
    }
  }

  /*
    Toutes les méthodes get(TypePrimitif) ont un problème MAJEUR :
     ELLES NE PRENNENT PAS EN COMPTE LES NULL !
    C'est quand même pas un problème mineur ! Donc au lieu d'utiliser
    la méthode getX en fonction du type de la colonne, on sélectionne la
    méthode d'extraction en fonction du type du champ.
    Ça demande quand même de vérifier le type de chaque champ
    individuellement...
   */
    public static Object extraire_champ(Cursor curseur, int colonne) {
      switch(curseur.getType(colonne)) {
        case Cursor.FIELD_TYPE_NULL: return null;
        case Cursor.FIELD_TYPE_INTEGER: return curseur.getInt(colonne);
        case Cursor.FIELD_TYPE_FLOAT: return curseur.getDouble(colonne);
        case Cursor.FIELD_TYPE_STRING: return curseur.getString(colonne);
        case Cursor.FIELD_TYPE_BLOB: return curseur.getBlob(colonne);
        default: return curseur.getString(colonne);
      }
    }


  public volatile boolean options_affichees;
  public char n_colonnes;
  public char n_enregistrements;
  public char limite;
  public long dernier_id;
  public long selection_id;
  public ActionFocus focus_dispatcher;
  //public ActionDone dispatcher_clavier;
  public AlertDialog popup_suppression_ligne;
  public AlertDialog popup_actions_colonne;
  public AlertDialog popup_colonne_correspondante;
  public LayoutInflater inflater;
  public LinearLayout conteneur_actions_edition;
  public MenuItem bouton_suivant;
  public MenuItem bouton_precedent;
  public RelativeLayout interface_popup_correspondance;
  public Stack<Long> offsets;
  public String table_editee;
  public TableLayout vue_table;
  public ViewSMD rowid_click_dispatcher;
  public ViewSMD colonne_click_dispatcher;
  public boolean[] etats_champ_selectionne;
  public Column[] colonnes;
  public FabriqueVues[] fabriques;
  public TextView[] actions_edition;

  final class EtatVueTable {
    boolean mode_edition;
    WeakReference<ChampEditable> champ_selectionne;

    EtatVueTable() {
      champ_selectionne =
        new WeakReference<ChampEditable>((ChampEditable) null);
    }

    void mode_edition(boolean mode) {

      if (!mode) {
        if (this.champ_selectionne.get() != null)
          this.champ_selectionne.get().clearFocus();
        this.champ_selectionne.clear();
      }
      this.mode_edition = mode;
      regler_icones_edition();
    }

    void reset() { mode_edition(false); }
  }

  public EtatVueTable etats;

  public VueTable() {
    this.main_menu = R.menu.actions_vue_table;
    this.etats     = new EtatVueTable();
    this.navigation_drawable = R.drawable.ic_arrow_back_white_24dp;
  }

  public ManipulationBDD activity() {
    return (ManipulationBDD) getActivity();
  }

  @Override
  public boolean action_clicked(@IdRes int action_cliquee) {
    switch (action_cliquee) {
      case R.id.bascule_mode_edition :
        etats.mode_edition(!etats.mode_edition);
        afficher_lignes();
        if (etats.mode_edition)
             this.main_toolbar.setSubtitle(R.string.entrain_editer);
        else this.main_toolbar.setSubtitle(null);
        return true;
      case R.id.enregistrements_precedents:
        this.offsets.pop();
        afficher_lignes();
        return true;
      case R.id.enregistrements_suivants:
        this.offsets.push(this.offsets.peek() + this.limite);
        afficher_lignes();
        return true;
      default: return false;
    }
  }

  @Override
  public void navigation(View v) {
    this.activity().interface_liste_tables();
  }

  public void regler_icones_edition() {
    if (this.etats.mode_edition)
      this.conteneur_actions_edition.setVisibility(View.VISIBLE);
    else this.conteneur_actions_edition.setVisibility(View.GONE);
  }

  @Override
  public void setup_toolbar() {
    super.setup_toolbar();
    Menu actions = this.main_toolbar.getMenu();
    this.bouton_precedent =
      actions.findItem(R.id.enregistrements_precedents);
    this.bouton_suivant =
      actions.findItem(R.id.enregistrements_suivants);
  }

  @Override
  public void initialise_content_views(@NonNull LayoutInflater inflater) {
    inflate_in_content(R.layout.fragment_vue_table);
    //this.content.setBackgroundColor(0xffcccccc);
    this.vue_table = (TableLayout) find_in_content(R.id.vue_table);
    this.inflater  = inflater;
    this.separator.setBackgroundColor(0xff263686);
    ChampEditable.callback_saisie(this);
    this.conteneur_actions_edition =
      (LinearLayout) find_in_content(R.id.actions_edition);
    this.actions_edition = ViewHelpers.convert_menu(
      R.menu.actions_champs_bdd, this, "actions_edition"
    );
    this.etats_champ_selectionne =
      new boolean[this.actions_edition.length];
    for (TextView action : this.actions_edition) {
      action.setTextAppearance(activity(), R.style.texte_actions_champs);
      action.setPadding(padding_action,0,padding_action,padding_inferieur);
      conteneur_actions_edition.addView(action);
    }
    this.interface_popup_correspondance =
      (RelativeLayout)
        inflater.inflate(R.layout.popup_colonne_correspondante, null, false);

  }

  public void setup_content_views() {

    this.etats.reset();

    this.rowid_click_dispatcher =
      new ViewSMD("selectionner_pour_suppression", this);
    this.colonne_click_dispatcher =
      new ViewSMD("afficher_popup_colonne", this);
    this.popup_actions_colonne =
      create_dialog(R.array.actions_popup_colonne, "choix_popup_colonne");
    this.popup_suppression_ligne =
      create_dialog(null, props_suppression_ligne, "supprimer_ligne");
    this.popup_colonne_correspondante =
      create_dialog(this.interface_popup_correspondance,
                    props_selection_correspondance,
                    "afficher_colonne_correspondante");


    this.table_editee       = activity().table_editee;
    this.offsets            = new Stack<Long>();
    this.offsets.push(0L);
    this.limite             = 20;
    this.colonnes = activity().informations_table(this.table_editee);
    this.n_colonnes         = (char) colonnes.length;

    this.focus_dispatcher   = new ActionFocus();
    //this.dispatcher_clavier = new ActionDone();
    this.fabriques          = new FabriqueVues[n_colonnes];

    {
      Column colonne_actuelle;

      for (char i = 0; i < n_colonnes; i++) {
        colonne_actuelle = colonnes[i];

        if (colonne_actuelle.type != Column.BLOB)
             this.fabriques[i] = new EditTextFactory(i);
        else this.fabriques[i] = new StaticBlobFactory(i);
      }
    }

    afficher_lignes();
    this.main_toolbar.setTitle(this.table_editee);
  }

  @Override
  public void onStop() {
    this.focus_dispatcher.stopping = true;
    super.onStop();
  }

  public boolean selectionner_pour_suppression(View v) {
    if (etats.mode_edition) {
      this.selection_id = ((Ligne) v.getParent()).row_id;
      this.popup_suppression_ligne.show();
      return true;
    }
    return false;
  }

  public void
  supprimer_ligne(DialogInterface dialog, int bouton_clique) {
    if (bouton_clique == positive_button) {
      activity().supprimer_ligne(selection_id);
      afficher_lignes();
    }
  }

  public void afficher_lignes() {
    Cursor enregistrements =
      activity().enregistrements(table_editee, this.offsets.peek(), 20);
    this.n_enregistrements = (char) enregistrements.getCount();
    Object donnee;
    Ligne ligne;
    long oid;

    vue_table.removeAllViews();

    /* Affiche le titre des colonnes */
    ligne = new Ligne(activity());
    ligne.ajouter(titre_colonne("ROWID"));
    for (char colonne = 0; colonne < n_colonnes; colonne++)
      ligne.ajouter(titre_colonne(this.colonnes[colonne].name));
    vue_table.addView(ligne);

    /* Affiche une ligne vide, à remplir, servant à insérer une nouvelle
       ligne  */

    if (etats.mode_edition) {
      ligne = new Ligne(activity());
      ligne.ajouter(rowid("NEW"));

      for (char new_i = 0; new_i < n_colonnes; new_i++)
        ligne.ajouter(this.fabriques[new_i].nouveau_champ());
      vue_table.addView(ligne);
    }

    /* Affiche les enregistrements récupérés */
    while (enregistrements.moveToNext()) {
      ligne = new Ligne(activity());
      oid = enregistrements.getLong(0);
      ligne.ajouter(rowid(oid));
      ligne.row_id = oid;
      for (char index = 0; index < n_colonnes; index++) {
        // index + 1 car index ne prends pas en compte le rowid
        donnee = extraire_champ(enregistrements, index+1);
        ligne.ajouter(this.fabriques[index].fabriquer_avec(donnee, ligne));
      }

      ligne.position = this.vue_table.getChildCount();
      this.vue_table.addView(ligne);
    }
    this.dernier_id = ligne.row_id;

    /* Ajoute suffisamment d'espace pour pouvoir saisir dans les
       champs de la dernière ligne à l'aide du clavier virtuel
    */

    enregistrements.close();

    ligne = new Ligne(activity());
    ligne.addView(rembourrage());
    this.vue_table.addView(ligne);

    /* Pour finir la publicité ... */
    ajouter_poblocoto_dans(this.vue_table);

    ligne = new Ligne(activity());
    ligne.addView(rembourrage());
    this.vue_table.addView(ligne);

    bouton_suivant.setVisible(n_enregistrements == limite);
    bouton_precedent.setVisible(offsets.peek() != 0L);
  }

  public TextView titre_colonne(CharSequence titre) {
    TextView titre_colonne =
      (TextView)
        this.inflater.inflate(R.layout.titre_colonne, null, false);
    titre_colonne.setText(titre);
    //if (titre != "ROWID")
    //  titre_colonne.setOnLongClickListener(colonne_click_dispatcher);
    return titre_colonne;
  }

  public TextView rowid(long id) {
    TextView rowid = rowid(String.valueOf(id));
    rowid.setOnLongClickListener(rowid_click_dispatcher);
    return rowid;
  }

  public TextView rowid(String id) {
    TextView id_view =
      (TextView) this.inflater.inflate(R.layout.champ_rowid, null, false);
    id_view.setText(id);
    return id_view;
  }

  public interface FabriqueVues {
    View fabriquer_avec(Object contenu, ViewGroup conteneur);
    View nouveau_champ();
  }

  /* TODO : Factorisation */
  public ChampEditable nouveau_champ_editable(char colonne) {
    ChampEditable nouveau_champ =
      (ChampEditable) inflater.inflate(R.layout.champ_bdd, null, false);

    nouveau_champ.setTag("Nouveau");

    Column colonne_actuelle = colonnes[colonne];
    nouveau_champ.colonne(colonne_actuelle);
    nouveau_champ.nouveau   = true;

    nouveau_champ.valeur_initiale(colonne_actuelle.default_value);

    nouveau_champ.setInputType(modes_editions[colonnes[colonne].type]);
    nouveau_champ.setOnFocusChangeListener(focus_dispatcher);
    //nouveau_champ.setOnEditorActionListener(dispatcher_clavier);

    return nouveau_champ;
  }

  public final class EditTextFactory implements FabriqueVues {

    final char colonne;

    public EditTextFactory(char colonne) {
      this.colonne = colonne;
    }

    @Override
    public View nouveau_champ() {
      return nouveau_champ_editable(colonne);
    }

    @Override
    public View fabriquer_avec(Object contenu, ViewGroup conteneur) {

      ChampEditable champ =
        (ChampEditable) inflater.inflate(R.layout.champ_bdd, null, false);
      champ.colonne(colonnes[colonne]);

      if (contenu != null) champ.valeur_initiale(contenu.toString());
      else champ.valeur_initiale(null);

      if (!etats.mode_edition) champ.setEnabled(false);
      else {
        champ.setInputType(modes_editions[colonnes[colonne].type]);
        champ.setOnFocusChangeListener(focus_dispatcher);
        //champ.setOnEditorActionListener(dispatcher_clavier);
      }
      return champ;
    }
  }

  public final class StaticBlobFactory implements FabriqueVues {
    public char colonne;
    public StaticBlobFactory(char colonne) {
      this.colonne = colonne;
    }

    @Override
    public View nouveau_champ() {
      return nouveau_champ_editable(colonne);
    }

    @Override
    public View fabriquer_avec(Object contenu, ViewGroup conteneur) {
      TextView vue =
        (TextView)
          inflater.inflate(R.layout.champ_blob_text, null, false);
      vue.setTag(colonnes[colonne].name);
      return vue;
    }
  }

  /* Principalement utilisé pour rembourrer l'espace situé après le bas
     du tableau, ceci afin de permettre aux gens de pouvoir voir les
     champs inférieurs lors de la saisie via le clavier virtuel.
   */
  View rembourrage() {
    TableRow.LayoutParams dimensions =
      new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, activity().resolution[1]/4);
    dimensions.column = 0;
    dimensions.span   = n_colonnes;
    View rembourrage = new View(activity());
    rembourrage.setLayoutParams(dimensions);
    return rembourrage;
  }

  void ajouter_poblocoto_dans(TableLayout table) {
    /* Putain que c'est moche...
       Alors pour résumer, il est impératif que la publicité soit dans
       un conteneur faisant 320dp par 50dp.
       Donc l'idée est d'insérer une ligne faisant, la largeur de l'écran
       en largeur, la hauteur étant déterminée automatiquement.
       - Si je place directement la publicité avec la dimension
         parametrée via TableLayout.LayoutParams, la publicité s'affiche
         mais le défilement n'est plus possible horizontalement !?
       - Même chose si je place la publicité dans une frame, qui est
         directement placée dans la table.
       - Par contre, si je place la publicité, aux bonnes dimensions,
         placée dans la frame, aux bonnes dimensions, le tout placée
         dans une ligne aux dimensions variable : Ça s'affiche et le
         défilement fonctionne !
         Sauf que la publicité est à moitié bouffée...
       MAGIQUE !
     */
    View pob = this.inflater.inflate(R.layout.bup, null, false);
    FrameLayout frame = new FrameLayout(activity());
    Ligne ligne = new Ligne(activity());
    TableRow.LayoutParams dimensions = new TableRow.LayoutParams(
      activity().resolution[0],
      TableRow.LayoutParams.WRAP_CONTENT
    );
    dimensions.column = 0;
    // n_colonnes + rowid
    dimensions.span   = n_colonnes+1;
    frame.setLayoutParams(dimensions);
    pob.setLayoutParams(new FrameLayout.LayoutParams(activity().resolution[0], FrameLayout.LayoutParams.WRAP_CONTENT));
    frame.addView(pob);
    ligne.addView(frame);
    table.addView(ligne);
    Eticilbup.tutup(pob, "SQL");

  }

  public final class ActionFocus implements View.OnFocusChangeListener {

    public boolean stopping;

    /* Se charge de rajouter les actions 'Set Null' et 'Set Default'
       dans la seconde barre d'outils
     */
    @Override
    public synchronized void onFocusChange(View v, boolean focus) {
      if (!stopping) {
        if (focus)
          etats.champ_selectionne =
            new WeakReference<ChampEditable>((ChampEditable) v);
        if (focus && !options_affichees)
          afficher_options((ChampEditable) v);
        else retirer_options();
        if (!focus) champ_saisi((ChampEditable) v);

      }
    }
  }

  /* Putain que la gestion du clavier virtuel sous Android est
     IN-SUP-POR-TABLE !
     C'est quand même pas croyable ! Je ne sais pas quelle combinaison
     de 'imeActionLabel' et 'imeOptions' permet d'obtenir un bouton
     'Save' à la place de la touche Entrée dans n'importe quel cas !
     Parfois le bouton s'affiche en mode saisie numérique, parfois pas.
     En mode saisie de texte multiligne, c'est même pas la peine, la
     touche Entrée est intouchable, et le bouton 'Next' apparaît
     PARFOIS, suivant le type de clavier virtuel... Imbitable...
   */
  /*public final class ActionDone implements TextView.OnEditorActionListener {
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
      champ_saisi((ChampEditable) v);
      return false;
    }
  }*/

  @Override
  public void champ_saisi(ChampEditable champ) {
    if (champ.getTag() == null) sauvegarder_champ(champ);
    //else if (champ.getTag() == "Nouveau") {
      //if (champ.colonne.position != n_colonnes - 1);
      //else inserer_ligne_du(champ);
    //}
    //ViewHelpers.move_focus(champ, View.FOCUS_FORWARD);
  }

  public void sauvegarder_champ(ChampEditable champ) {
    try {
      Ligne ligne          = (Ligne) champ.getParent();

      Column colonne_champ = champ.colonne;
      String nom           = colonne_champ.name;
      String valeur        = champ.valeur();
      String rowid         = String.valueOf(ligne.row_id);

      /* SQLiteDatabase#update a besoin de 'ContentValues' pour
         connaître les champs à modifier et les nouvelles valeurs de
         ces champs.
         parametre_requete est réutiliser au lieu de recréer un
         objet à chaque fois...
      */
      parametres_requete.clear();
      methodes_parametres_requete[colonne_champ.type].invoke(
        null, nom, valeur
      );
      activity().update_table_actuelle(rowid, parametres_requete);
    }
    catch (IllegalAccessException | InvocationTargetException |
      ExceptionInInitializerError | SQLiteException e) {
      Toast.makeText(activity(), e.getCause().getMessage(),
                     Toast.LENGTH_LONG).show();
      e.printStackTrace();
    }
  }

  /* Les opérations d'enregistrement se déclenchent toujours avec
     le champ de la nouvelle ligne sélectionnée.
     TODO : Y'a peut être une meilleure solution.
     Gaffe cependant à ne pas enregistrer la référence de la ligne
     explicitement !
   */
  public void inserer_ligne_du(ChampEditable champ_selectionne) {
    champ_selectionne.clearComposingText();
    inserer_ligne((Ligne) champ_selectionne.getParent());
    afficher_lignes();
  }

  public void inserer_ligne(Ligne ligne) {
    ChampEditable champ;
    synchronized (VueTable.class) {
      try {
        parametres_requete.clear();
        for (char i = 0; i < n_colonnes; i++) {
          champ = (ChampEditable) ligne.getChildAt(i + 1);
          /*
            Si le champ concerné est une clef primaire de type
            INTEGER, il est fort probable que la personne ne l'ait
            pas remplie afin de bénéficier des fonctions
            d'auto-affectation de numéro, internes à SQLite.
            Ceci concerne tout aussi bien les clefs primaires entières
            simples, que les clefs primaires entières AUTOINCREMENT.
            Ainsi, si le champ de la clef primaire ne contient pas de
            nombre, on l'ignore (continue).
           */

          if (champ.colonne.primary_key == 0 ||
              champ.colonne.type != SQLiteHelpers.INTEGER);
          else if (champ.valeur() == null ||
                   !champ.valeur().matches("\\d+")) continue;

          methodes_parametres_requete[champ.colonne.type].invoke(
            null, champ.colonne.quoted_name, champ.valeur()
          );
        }
        activity().inserer_nouvelle_ligne(parametres_requete);
      }
      catch (IllegalAccessException | InvocationTargetException |
        ExceptionInInitializerError e) {
        Toast.makeText(activity(), e.getCause().getMessage(), Toast.LENGTH_LONG).show();
      }
      catch (SQLiteException e) {
        Toast.makeText(activity(), e.getMessage(), Toast.LENGTH_LONG).show();
      }
    }
  }

  // Appelée par le dispatcher associé aux boutons de la barre d'édition
  public void actions_edition(View v) {
    // v représente les boutons tels que 'Set NULL', 'Set DEFAULT'

    // Le champ peut être NULL si celui-ci a été recyclé par le GC
    // Peu probable cependant
    ChampEditable champ_selectionne =
      this.etats.champ_selectionne.get();
    if (champ_selectionne != null); else return;
    switch (v.getId()) {
      case R.id.action_rendre_null:
        champ_selectionne.clearComposingText();
        champ_selectionne.set_null();
        break;
      case R.id.action_inserer_valeur_defaut:
        champ_selectionne.clearComposingText();
        champ_selectionne.setText(
          champ_selectionne.colonne.default_value
        );
        break;
      case R.id.action_enregistrer_nouvelle_ligne:
        inserer_ligne_du(champ_selectionne);
    }

  }


  public void afficher_options(ChampEditable champ) {
    Column colonne_actuelle = champ.colonne;

    this.etats_champ_selectionne[insert_row]  = champ.nouveau;
    this.etats_champ_selectionne[set_default] =
      (colonne_actuelle.default_value == null);
    this.etats_champ_selectionne[set_null] =
      !colonne_actuelle.not_null;

    for (byte i = 0; i < 3; i++) {
      this.actions_edition[i].setVisibility(
        this.etats_champ_selectionne[i] ? View.VISIBLE : View.INVISIBLE
      );
    }

    this.options_affichees = true;
  }

  public void retirer_options() {
    this.options_affichees = false;
  }

  public void annuler() {
    this.etats.champ_selectionne.get().reinitialiser();
  }

  public boolean afficher_popup_colonne(View v) {
    popup_actions_colonne.show();
    return true;
  }

  public void choix_popup_colonne(DialogInterface popup, int choix) {
  }

  public void afficher_colonne_correspondante(DialogInterface popup, int choix) {
  }
}
