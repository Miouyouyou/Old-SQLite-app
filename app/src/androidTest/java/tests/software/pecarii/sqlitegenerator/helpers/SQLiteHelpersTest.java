package tests.software.pecarii.sqlitegenerator.helpers;

import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.Arrays;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

import software.pecarii.sqlitegenerator.helpers.ExternalStorage;
import static software.pecarii.sqlitegenerator.helpers.ExternalDatabases.sqlite_files_in;
import software.pecarii.sqlitegenerator.helpers.FileHelpers;
import software.pecarii.sqlitegenerator.helpers.SQLiteHelpers;
import software.pecarii.sqlitegenerator.helpers.sqltypes.Column;
import software.pecarii.sqlitegenerator.helpers.sqltypes.ForeignKey;
import software.pecarii.sqlitegenerator.helpers.sqltypes.PrimaryKey;

import static software.pecarii.sqlitegenerator.helpers.SQLiteHelpers.list_tables_names_in;

public class SQLiteHelpersTest extends TestCase {

  public final static String test_database_path;
  public final static File test_folder;
  public final static File test_database_file;

  public SQLiteDatabase test_database;
  public String[] test_database_table_queries;

  static {
    String test_folder_path = 
      ExternalStorage.path_from_external_path("SQLHelperTestFolder");
    test_folder = new File(test_folder_path);
    if (!(test_folder.exists())) test_folder.mkdir();
    
    test_database_path =
      FileHelpers.path_from_segments(test_folder.getAbsolutePath(),
                                     "test.db");
    test_database_file = new File(test_database_path);
    if (test_database_file.exists()) test_database_file.delete();

  }

  static class SchemaTable {
    String nom;
    Column[] colonnes;
    PrimaryKey pk;
    ForeignKey[] fks;

    String requete_creation() {
      StringBuilder builder = new StringBuilder(128);
      builder.
        append("CREATE TABLE ").
        append(SQLiteHelpers.quote(nom)).
        append('(');

      // Colonnes
      builder.
        append(colonnes[0].quoted_name).
        append(' ').
        append(colonnes[0].str_type);
      for (char i = 1, n_colonnes = (char) colonnes.length;
           i < n_colonnes; i++) {
        builder.append(',').
          append(colonnes[i].quoted_name).
          append(' ').
          append(colonnes[i].str_type);
      }

      // Clef primaire
      if (pk != null && pk.colonnes_composantes.length != 0) {
        builder.append(',').
          append("PRIMARY KEY (").
          append(pk.colonnes_composantes[0].quoted_name);
        for (char i = 1, n_composantes = (char) pk.colonnes_composantes.length;
             i < n_composantes; i++) {
          builder.append(',').
            append(pk.colonnes_composantes[i].quoted_name);
        }
        builder.append(')');
      }

      // Clefs etrangeres
      if (fks != null && fks.length != 0) {
        for (ForeignKey fk : fks) {
          // Prélude
          builder.append(',').
            append("FOREIGN KEY (").

          // Liste des colonnes enfantes
            append(fk.child_columns[0].quoted_name);
          char n_colonnes = (char) fk.child_columns_number;
          for (char i = 1; i < n_colonnes; i++)
            builder.append(',').
              append(fk.child_columns[0].quoted_name);

          // Fin des colonnes enfantes et début des colonnes parentes
          builder.append(") REFERENCES ").
            append(fk.parent_name).
            append('(').
            append(fk.parent_columns[0].quoted_name);
          for (char i = 1; i < n_colonnes; i++)
            builder.append(',').append(fk.parent_columns[0].quoted_name);
          builder.append(')');
        }
      }
      return builder.toString();
    }

    void creer_dans_table(SQLiteDatabase bdd) {
      bdd.execSQL(requete_creation());
    }
  }

  @Override
  public void setUp() throws Exception {
    SQLiteHelpers.create(test_database_path);
    test_database = SQLiteHelpers.open(test_database_path, 1);
    test_database_table_queries = new String[] {
            "CREATE TABLE super_test (TEXT name primary key);",
            "CREATE TABLE mega_tests (INTEGER id primary key, TEXT name);",
            "DROP TABLE android_metadata"};

    for (String query : test_database_table_queries)
      test_database.execSQL(query);

    super.setUp();
  }

  @Override
  public void tearDown() throws Exception {
    test_database.close();
    test_database_file.delete();
    super.tearDown();
  }
  
  public void testListTablesNamesOf() {
    final String[] expected_tables = {"super_test", "mega_tests"};
    final String[] tables_names_list = 
      list_tables_names_in(test_database);
      
    for (String name : tables_names_list) System.err.println(name);
    assertTrue(
      Arrays.equals(expected_tables,tables_names_list)
    );
  }
 
  public void testCreate() {
    String another_test_db_path;
    File another_test_db;
    String inexistant_path = "/inexistant/path/to/file.db";
    File[] databases_in_test_dir;

    /* Preparation */
    assertTrue(test_folder.exists());

    another_test_db_path = 
      FileHelpers.path_from_segments(test_folder.getAbsolutePath(),
                                     "CreationTests",
                                     "test_create.db");
    another_test_db = new File(another_test_db_path);
    
    another_test_db.getParentFile().mkdir();
    if (another_test_db.exists()) another_test_db.delete();

    assertFalse(another_test_db.exists());
    
    /* Tests */
    // The path exists so the creation should be successful
    assertEquals(SQLiteHelpers.DATABASE_CREATED, SQLiteHelpers.create(another_test_db_path));
    // create returned true, so the file MUST exists
    assertTrue(another_test_db.exists());
    // Trying to create a database with a path leading to an existing
    // file shoud fail
    assertEquals(SQLiteHelpers.FILENAME_ALREADY_USED, SQLiteHelpers.create(another_test_db_path));
    // Even then, the file MUST still exist !
    assertTrue(another_test_db.exists());
    // Using create with a non-existing path should fail 
    assertEquals(SQLiteHelpers.INVALID_PATH, SQLiteHelpers.create(inexistant_path));
    assertFalse(new File(inexistant_path).exists());
    
    databases_in_test_dir = 
      sqlite_files_in(another_test_db.getParentFile().listFiles());
    
    for (File db : databases_in_test_dir) {
      System.out.println(db.getAbsolutePath());
    }
    
    assertEquals(1, databases_in_test_dir.length);
    assertEquals(databases_in_test_dir[0].getAbsolutePath(),
                 another_test_db.getAbsolutePath());
  }
}
