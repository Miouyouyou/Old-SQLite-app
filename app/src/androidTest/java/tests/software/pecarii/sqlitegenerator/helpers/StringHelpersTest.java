package tests.software.pecarii.sqlitegenerator.helpers;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;

import software.pecarii.sqlitegenerator.helpers.StringHelpers;

public class StringHelpersTest extends TestCase {
  public void testList() {
    Object[][] expected_separator_and_lists = new Object[][] {
      {"",                      ",",      new String[0]},
      {"element",               "  ,",    new String[] {"element"}},
      {"element/element2",      "/",      new String[] {"element", "element2"}},
      {"element\"\"\"element2", "\"\"\"", new String[] {"element", "element2"}},
      {"a,b,c,d,e",             ",",      new String[] {"a","b","c","d","e"}}};
      
    for (Object[] esl : expected_separator_and_lists) {
      assertEquals(esl[0], StringHelpers.list((String) esl[1], 
                                              (String[]) esl[2]));
    }
  }

  public void testSuffix() {
    Object[][] expected_suffixes_list = new Object[][] {
      {"",  ".sqlite"},
      {".", "..sqlite"},
      {"a", "a.sqlite"},
      {"データーベース", "データーベース.sqlite"},
      {"鯖", "鯖.sqlite"},
    };

    for (Object[] expected_suffixes : expected_suffixes_list) {
      String expected_suffixed_string = (String) expected_suffixes[1];
      String provided_string = (String) expected_suffixes[0];
      assertEquals(expected_suffixed_string, StringHelpers.suffix(provided_string, ".sqlite"));
    }
  }
}
