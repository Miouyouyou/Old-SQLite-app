package tests.software.pecarii.sqlitegenerator.helpers;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;

import static software.pecarii.sqlitegenerator.helpers.FileHelpers.readable_filesize;
import static software.pecarii.sqlitegenerator.helpers.FileHelpers.path_from_segments;

public class FileHelpersTest extends TestCase {
  public void testReadableFileSize() {
    Object[][] sizes_and_expectations = {
      {0L, "0 B"},
      {1023L, "1023 B"},
      {1024L, "1 KiB"},
      {10240L, "10 KiB"},
      {1048575L, "1023 KiB"},
      {1048576L, "1 MiB"},
      {9223372036854775807L, "7 EiB"}};
    
    for (Object[] size_and_expectation : sizes_and_expectations) {
      assertEquals(size_and_expectation[1],
                   readable_filesize((Long) size_and_expectation[0]));
    }
  }
  
  public static void testPathFromSegments() {
    String[] passed_arguments = {"/mnt/sdcard", "path", "t o", "ファイル"};
    String expected_result = "/mnt/sdcard/path/t o/ファイル";
    
    assertEquals("", 
                 path_from_segments(new String[0]));
    
    assertEquals("mnt",
                 path_from_segments("mnt"));
                 
    assertEquals("/mnt",
                 path_from_segments("/mnt"));
    
    assertEquals("/mnt/sdcard",
                 path_from_segments("/mnt", "sdcard"));
                 
    assertEquals(expected_result,
                 path_from_segments(passed_arguments));
  }
}
