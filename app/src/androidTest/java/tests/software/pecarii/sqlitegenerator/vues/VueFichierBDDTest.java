package tests.software.pecarii.sqlitegenerator.vues;

import android.view.LayoutInflater;
import android.test.AndroidTestCase;

import java.io.File;

import static junit.framework.Assert.assertEquals;

import software.pecarii.sqlitegenerator.helpers.ExternalStorage;
import software.pecarii.sqlitegenerator.helpers.FileHelpers;
import software.pecarii.sqlitegenerator.vues.VueFichierBDD;
import software.pecarii.sqlitegenerator.R;

import tests.software.pecarii.helpers.FakeFileCreator;

public class VueFichierBDDTest extends AndroidTestCase {

  public static File fake_file;
  public static String fake_file_name;
  public static int size;
  public static String readable_size;
  
  static {
    fake_file_name = "fake_file.db";
    size = 7412471;
    readable_size = FileHelpers.readable_filesize(size);
    fake_file = 
      FakeFileCreator.create(ExternalStorage.path(), 
                             fake_file_name, size);
  }

  /*public void testConcernant() {
    VueFichierBDD vue = 
      (VueFichierBDD) 
        LayoutInflater.
          from(getContext()).
          inflate(R.layout.liste_fichiers_bdd_element);
    vue.concernant(fake_file);
    
    assertEquals(fake_file_name, vue.nom_du_fichier());
    assertEquals(readable_size, vue.taille_du_fichier());
    assertEquals(fake_file.getAbsolutePath(), vue.chemin_du_fichier());
  }*/
}
