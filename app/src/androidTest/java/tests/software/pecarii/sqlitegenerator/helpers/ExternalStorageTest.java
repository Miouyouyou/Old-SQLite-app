package tests.software.pecarii.sqlitegenerator.helpers;

import software.pecarii.sqlitegenerator.helpers.ExternalStorage;

import junit.framework.TestCase;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertEquals;

public class ExternalStorageTest extends TestCase {
  
  public static void testEcrireSurStockage() {
    assertTrue(ExternalStorage.writeable());
  }

  public static void testLireStockageExterne() {
    assertTrue(ExternalStorage.readable());
  }

  public static void testCheminStockageExterne() {
    assertTrue(ExternalStorage.directory().exists());
    System.err.println(ExternalStorage.directory().getAbsolutePath());
  }
  
  public static void testPathFromExternalPath() {
    final String external_path = ExternalStorage.path();
    final String[] segments = {"é", "1", "にゃ"};
    final String external_path_with_segments = 
      String.format("%s/%s", external_path, "é/1/にゃ");
    
    assertEquals(external_path, 
                 ExternalStorage.path_from_external_path((String[]) null));
    assertEquals(external_path,
                 ExternalStorage.path_from_external_path(new String[0]));
    assertEquals(external_path_with_segments,
                 ExternalStorage.path_from_external_path(segments));
    assertEquals(external_path_with_segments,
                 ExternalStorage.path_from_external_path("é", "1", "にゃ"));
                               
  }
}

