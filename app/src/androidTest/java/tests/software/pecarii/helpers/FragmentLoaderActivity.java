package tests.software.pecarii.helpers;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

/**
  Currently useless. I do not know how to register this activity
  in the AndroidManifest.xml and I'm growing tired of Android stupidity
 */

public class FragmentLoaderActivity extends Activity {

  public FragmentManager fm;
  public Fragment loaded_fragment;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.fm = getFragmentManager();
  }
    
  public FragmentTransaction ft() { return fm.beginTransaction(); }
  
  public void load_fragment(Fragment fragment, String tag) {
    ft().add(android.R.id.content, fragment, tag).commit();
    this.loaded_fragment = fragment;
  }
  
  public void detach() { ft().detach(loaded_fragment).commit(); }
  
  public void reattach() { ft().attach(loaded_fragment).commit(); }
  
}
