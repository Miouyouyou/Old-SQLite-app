package tests.software.pecarii.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public final class FakeFileCreator extends TestCase {
  public static File create(String path, String name, int size) {
    File file_path = new File(path);
    file_path.mkdirs();
    System.err.println(file_path.getUsableSpace());
    if (file_path.getUsableSpace() <= size) {
      System.err.println("No available space !");
      return null;
    }
    
    File file = new File(path, name);
    byte[] padding = new byte[size];

    if (file.exists()) file.delete();
    
    try {
      FileOutputStream file_writer = new FileOutputStream(file);
      file_writer.write(padding);
      file_writer.close();
    } catch (java.io.IOException e) { throw new RuntimeException(e); }
    
    return file;
  }
  
  public void testCreate() throws Exception {
    String ext_path = 
      android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + 
      "/testFakeFileCreator/nya";      
    int test_size = 1024*1023;
    String test_filename = "super_fichier";
    File test_dir = new File(ext_path);
    File test_file = null;
    
    if (test_dir.exists()) { 
      for (File existing_file : test_dir.listFiles())
        existing_file.delete();
      test_dir.delete(); 
    }
    assertFalse(test_dir.exists());
    
    test_file = 
      create(test_dir.getAbsolutePath(), test_filename, test_size);
    
    assertTrue(test_file.exists());
    assertEquals(test_size, test_file.length());
    assertEquals(String.format("%s/%s", ext_path, test_filename),
                 test_file.getAbsolutePath());
  }
  
}
