package software.pecarii.sqlitegenerator.ads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.mopub.common.util.Views;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.MoPubErrorCode;

import java.util.Map;

/**
 * Created by gamer on 15/12/10.
 */
public abstract class QuickMopub<AdClass extends View>
                      extends CustomEventBanner {

  public static final String TAG = "QuickMopub";

  public AdClass ad;
  public CustomEventBannerListener mopubListener;

  @Override
  protected void loadBanner(final Context context,
                            final CustomEventBannerListener customEventBannerListener,
                            final Map<String, Object> localExtras,
                            final Map<String, String> serverExtras) {
    Log.d(TAG, "Invoked loadBanner");
    mopubListener = customEventBannerListener;
    initialisation();
    ad = create_ad_view(context);
    if (ad != null) {
      configure_ad(ad);
      last_action();
    }
    else report_problem();
    Log.d(TAG, "End of loadBanner");
  }

  abstract public AdClass create_ad_view(Context context);

  public void initialisation() {}
  public void configure_ad(final AdClass ad) {}
  public void setLocalListener(final AdClass ad) {}
  public void last_action() {}
  public void cleanup() {}
  public void show_ad() { mopubListener.onBannerLoaded(ad); }
  public void report_problem() {
    mopubListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
  }

  @Override
  protected void onInvalidate() {
    cleanup();
    Views.removeFromParent(ad);
  }
}
