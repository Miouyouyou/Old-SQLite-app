package software.pecarii.sqlitegenerator.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class Buptainer extends FrameLayout {

  public Buptainer(Context contexte) { super(contexte); }

  public Buptainer(Context contexte, AttributeSet attrs) {
    super(contexte, attrs);
  }

  public Buptainer(Context contexte, AttributeSet attrs, int sAttrs) {
    super(contexte, attrs, sAttrs);
  }

}
