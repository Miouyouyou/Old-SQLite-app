package software.pecarii.sqlitegenerator.ads;

/* First version : 2015-10-10
  That's quick and very dirty way to add AppBrain ads through MoPub.
  Remember to add the appropriate rights in your local
  AndroidManifest.xml
  The rights are (inside <application>) :
  <!-- AppBrain SDK -->
  <activity
    android:name="com.appbrain.AppBrainActivity"
    android:configChanges="keyboard|keyboardHidden|orientation|
                           screenLayout|uiMode|screenSize|
                           smallestScreenSize" />
  <service android:name="com.appbrain.AppBrainService" />
  <receiver
    android:name="com.appbrain.ReferrerReceiver"
    android:exported="true" >
    <intent-filter>
      <action android:name="com.android.vending.INSTALL_REFERRER" />
    </intent-filter>
  </receiver>
 */

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;

public class MopubAmazon extends QuickMopub<AdLayout> {

  @Override
  public void initialisation() {
    AdRegistration.enableLogging(true);
    AdRegistration.enableTesting(true);
    AdRegistration.setAppKey("e683ad998b1749d99eed832fdb42ff7b");
  }

  @Override
  public AdLayout create_ad_view(final Context context) {
    return new AdLayout((Activity) context);
  }
  @Override
  public void setLocalListener(final AdLayout ad) {
    ad.setListener(new Listener());
  }
  @Override
  public void last_action() {
    ad.setLayoutParams(new FrameLayout.LayoutParams(
      FrameLayout.LayoutParams.MATCH_PARENT,
      FrameLayout.LayoutParams.MATCH_PARENT));
    ad.loadAd();
    mopubListener.onBannerLoaded(ad);
  }

  @Override
  public void cleanup() { ad.destroy(); }

  final class Listener implements AdListener {
    @Override
    public void onAdCollapsed(Ad adv) {
      mopubListener.onBannerCollapsed();
    }

    @Override
    public void onAdLoaded(Ad adv, AdProperties adProperties) {

    }

    @Override
    public void onAdFailedToLoad(Ad adv, AdError adError) {
      Log.e(TAG, adError.getMessage());
      report_problem();
    }

    @Override
    public void onAdExpanded(Ad adv) {
      mopubListener.onBannerExpanded();
    }

    @Override
    public void onAdDismissed(Ad adv) {}
  }

}