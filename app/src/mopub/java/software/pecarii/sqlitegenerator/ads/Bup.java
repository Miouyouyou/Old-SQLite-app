package software.pecarii.sqlitegenerator.ads;

import android.app.Activity;
import android.view.ViewGroup;

/**
 * Created by gamer on 15/12/30.
 */
public abstract class Bup {

  public abstract void
  preparations_protegees(ViewGroup conteneur, Activity contexte);

  public abstract void
  detruire_protege(ViewGroup conteneur, Activity contexte);

  public void preparer(ViewGroup conteneur, Activity contexte) {
    try { preparations_protegees(conteneur, contexte); }
    catch (Exception e) {}
  }
  public void detruire(ViewGroup conteneur, Activity contexte) {
    try { detruire_protege(conteneur, contexte); }
    catch (Exception e) {}
  }
}
