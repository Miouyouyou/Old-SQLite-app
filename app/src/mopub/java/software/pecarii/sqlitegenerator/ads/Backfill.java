package software.pecarii.sqlitegenerator.ads;

import android.app.Activity;
import android.view.ViewGroup;

import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;

import software.pecarii.sqlitegenerator.R;

final public class Backfill extends Bup {

  @Override
  public void
  preparations_protegees(ViewGroup conteneur, Activity contexte) {
    ViewGroup.inflate(contexte, R.layout.buback, conteneur);
    MoPubView adView = (MoPubView) conteneur.findViewById(R.id.buback);
    adView.setAdUnitId("7077b7d15cfa4650952e06b32ddb3083");
    adView.setBannerAdListener(A.gimme());
    adView.loadAd();
  }

  @Override
  public void
  detruire_protege(ViewGroup conteneur, Activity contexte) {
   ((MoPubView) conteneur.getChildAt(0)).destroy();
    conteneur.removeAllViews();
  }

  static class A implements MoPubView.BannerAdListener {
    final static A instance;

    static { instance = new A(); }

    private A() {}

    public static A gimme() { return instance; }

    @Override
    public void onBannerClicked(MoPubView moPubView) {
      System.out.println("Clicked");
    }

    @Override
    public void onBannerCollapsed(MoPubView moPubView) {
      System.out.println("Collapsed");
    }

    @Override
    public void onBannerExpanded(MoPubView moPubView) {
      System.out.println("Expanded");
    }

    @Override
    public void onBannerFailed(MoPubView moPubView, MoPubErrorCode moPubErrorCode) {
      System.out.println("Failed");
      System.out.println(moPubErrorCode.toString());
    }

    @Override
    public void onBannerLoaded(MoPubView moPubView) {
      System.out.println("Loaded");
    }
  }
}
