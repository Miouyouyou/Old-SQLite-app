/*package software.pecarii.sqlitegenerator.ads.dumbynou;


import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import com.mopub.common.AdReport;
import com.mopub.common.ClientMetadata;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.event.BaseEvent.Name;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.WebViewAdUrlGenerator;
import com.mopub.mraid.MraidNativeCommandHandler;
import com.mopub.network.AdRequest;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubRequestQueue;
import com.mopub.network.Networking;
import com.mopub.network.TrackingRequest;
import com.mopub.network.AdRequest.Listener;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class MadController {
  static public final int DEFAULT_REFRESH_TIME_MILLISECONDS = 60000;
  static public final int MAX_REFRESH_TIME_MILLISECONDS = 600000;
  static public final double BACKOFF_FACTOR = 1.5D;
  public static final LayoutParams WRAP_AND_CENTER_LAYOUT_PARAMS;
  public static final WeakHashMap<View, Boolean> sViewShouldHonorServerDimensions;

  static {
    WRAP_AND_CENTER_LAYOUT_PARAMS = new LayoutParams(-2, -2, 17);
    sViewShouldHonorServerDimensions = new WeakHashMap<>();
  }

  public boolean autoRefreshEnabled = true;
  public boolean previousAutoRefreshSetting = true;
  public boolean isTesting;
  public boolean adWasLoaded;
  public boolean isLoading;
  public boolean isDestroyed;
  @VisibleForTesting int backoffPower;
  public int refreshTimeMillis;
  public int timeoutMilliseconds;
  public final long broadcastIdentifier;

  @Nullable public AdRequest activeRequest;
  public AdResponse adResponse;
  @Nullable public Context context;
  public Handler handler;
  @Nullable public Location location;
  @NonNull public final Listener adListener;
  public Map<String, Object> localExtras;
  @Nullable public MoPubView moPubView;
  public final Runnable refreshRunnable;
  public String url;
  public String keywords;
  @Nullable public String adUnitId;
  @Nullable public WebViewAdUrlGenerator urlGenerator;


  public static void setShouldHonorServerDimensions(View view) {
    sViewShouldHonorServerDimensions.put(view, true);
  }

  private static boolean getShouldHonorServerDimensions(View view) {
    return sViewShouldHonorServerDimensions.get(view) != null;
  }

  class StayAWhile implements Listener {
    @Override
    public void onSuccess(AdResponse response) {
      onAdLoadSuccess(response);
    }

    @Override
    public void onErrorResponse(VolleyError response) {
      onAdLoadError(response);
    }
  }

  public MadController(@NonNull Context context, @NonNull MoPubView view) {

    this.context = context;
    this.moPubView = view;
    this.timeoutMilliseconds = -1;
    this.localExtras = new HashMap<String,Object>(5);

    this.broadcastIdentifier = Utils.generateUniqueId();
    this.urlGenerator = new WebViewAdUrlGenerator(this.context.getApplicationContext(), MraidNativeCommandHandler.isStorePictureSupported(this.context));
    this.adListener = new StayAWhile();
    this.refreshRunnable = new Runnable() {
      @Override public void run() {
        internalLoadAd();
      }
    };
    this.refreshTimeMillis = DEFAULT_REFRESH_TIME_MILLISECONDS;
    this.handler = new Handler();
  }

  @VisibleForTesting
  void onAdLoadSuccess(@NonNull AdResponse adResponse) {
    this.backoffPower = 1;
    this.adResponse = adResponse;
    this.timeoutMilliseconds = this.adResponse.getAdTimeoutMillis() == null?this.timeoutMilliseconds :this.adResponse.getAdTimeoutMillis();
    Integer fuck_null = this.adResponse.getRefreshTimeMillis();
    this.refreshTimeMillis =
      fuck_null != null ? fuck_null : DEFAULT_REFRESH_TIME_MILLISECONDS;
    this.setNotLoading();
    AdLoader adLoader = AdLoader.fromAdResponse(this.adResponse, this);
    if(adLoader != null) {
      adLoader.load();
    }

    this.scheduleRefreshTimerIfEnabled();
  }

  @VisibleForTesting
  void onAdLoadError(VolleyError error) {
    if(error instanceof MoPubNetworkError) {
      MoPubNetworkError errorCode = (MoPubNetworkError)error;
      if(errorCode.getRefreshTimeMillis() != null) {
        this.refreshTimeMillis = errorCode.getRefreshTimeMillis();
      }
    }

    MoPubErrorCode var3 = getErrorCodeFromVolleyError(error, this.context);
    if(var3 == MoPubErrorCode.SERVER_ERROR) {
      ++this.backoffPower;
    }

    this.setNotLoading();
    this.adDidFail(var3);
  }

  @VisibleForTesting
  @NonNull
  static MoPubErrorCode getErrorCodeFromVolleyError(@NonNull VolleyError error, @Nullable Context context) {
    NetworkResponse networkResponse = error.networkResponse;
    if(error instanceof MoPubNetworkError) {
      switch(((MoPubNetworkError) error).getReason()) {
        case WARMING_UP: return MoPubErrorCode.WARMUP;
        case NO_FILL: return MoPubErrorCode.NO_FILL;
        default: return MoPubErrorCode.UNSPECIFIED;
      }
    }
    else {
      return networkResponse == null?(!DeviceUtils.isNetworkAvailable(context)?MoPubErrorCode.NO_CONNECTION:MoPubErrorCode.UNSPECIFIED):(error.networkResponse.statusCode >= 400?MoPubErrorCode.SERVER_ERROR:MoPubErrorCode.UNSPECIFIED);
    }
  }

  public void loadAd() {
    this.backoffPower = 1;
    this.internalLoadAd();
  }

  private void internalLoadAd() {
    this.adWasLoaded = true;
    if(TextUtils.isEmpty(this.adUnitId)) {
      MoPubLog.d("Can\'t load an ad in this ad view because the ad unit ID is not set. Did you forget to call setAdUnitId()?");
    }
    else if(!this.isNetworkAvailable()) {
      MoPubLog.d("Can\'t load an ad because there is no network connectivity.");
      this.scheduleRefreshTimerIfEnabled();
    }
    else { this.loadNonJavascript(generateAdUrl()); }
  }

  void loadNonJavascript(String url) {
    if(url != null) {
      MoPubLog.d("Loading url: " + url);
      if(this.isLoading) {
        if(!TextUtils.isEmpty(this.adUnitId)) {
          MoPubLog.i("Already loading an ad for " + this.adUnitId + ", wait to finish.");
        }

      } else {
        this.url = url;
        this.isLoading = true;
        this.fetchAd(this.url);
      }
    }
  }

  public void reload() {
    MoPubLog.d("Reload ad: " + this.url);
    this.loadNonJavascript(this.url);
  }

  void loadFailUrl(MoPubErrorCode errorCode) {
    this.isLoading = false;
    Log.v("MoPub", "MoPubErrorCode: " + (errorCode == null?"":errorCode.toString()));
    String failUrl = this.adResponse == null?"":this.adResponse.getFailoverUrl();
    if(!TextUtils.isEmpty(failUrl)) {
      MoPubLog.d("Loading failover url: " + failUrl);
      this.loadNonJavascript(failUrl);
    } else {
      this.adDidFail(MoPubErrorCode.NO_FILL);
    }

  }

  void setNotLoading() {
    this.isLoading = false;
    if(this.activeRequest != null) {
      if(!this.activeRequest.isCanceled()) {
        this.activeRequest.cancel();
      }
      this.activeRequest = null;
    }
  }

  public int getAdWidth() {
    return this.adResponse != null && this.adResponse.getWidth() != null?this.adResponse.getWidth():0;
  }

  public int getAdHeight() {
    return this.adResponse != null && this.adResponse.getHeight() != null?this.adResponse.getHeight():0;
  }

  void pauseRefresh() {
    this.previousAutoRefreshSetting = this.autoRefreshEnabled;
    this.setAutorefreshEnabled(false);
  }

  void unpauseRefresh() {
    this.setAutorefreshEnabled(this.previousAutoRefreshSetting);
  }

  void forceSetAutorefreshEnabled(boolean enabled) {
    this.previousAutoRefreshSetting = enabled;
    this.setAutorefreshEnabled(enabled);
  }

  private void setAutorefreshEnabled(boolean enabled) {
    boolean autorefreshChanged = this.adWasLoaded && this.autoRefreshEnabled != enabled;
    if(autorefreshChanged) {
      String enabledString = enabled?"enabled":"disabled";
      MoPubLog.d("Refresh " + enabledString + " for ad unit (" + this.adUnitId + ").");
    }

    this.autoRefreshEnabled = enabled;
    if(this.adWasLoaded && this.autoRefreshEnabled) {
      this.scheduleRefreshTimerIfEnabled();
    } else if(!this.autoRefreshEnabled) {
      this.cancelRefreshTimer();
    }

  }

  @Nullable
  public AdReport getAdReport() {
    return this.adUnitId != null && this.adResponse != null?new AdReport(this.adUnitId, ClientMetadata.getInstance(this.context), this.adResponse):null;
  }

  void cleanup() {
    if(!this.isDestroyed) {
      if(this.activeRequest != null) {
        this.activeRequest.cancel();
        this.activeRequest = null;
      }

      this.setAutorefreshEnabled(false);
      this.cancelRefreshTimer();
      this.moPubView = null;
      this.context = null;
      this.urlGenerator = null;
      this.isDestroyed = true;
    }
  }

  Integer getAdTimeoutDelay() {
    return this.adResponse == null?null:this.adResponse.getAdTimeoutMillis();
  }

  void trackImpression() {
    if(this.adResponse != null) {
      TrackingRequest.makeTrackingHttpRequest(this.adResponse.getImpressionTrackingUrl(), this.context, Name.IMPRESSION_REQUEST);
    }

  }

  void registerClick() {
    if(this.adResponse != null) {
      TrackingRequest.makeTrackingHttpRequest(this.adResponse.getClickTrackingUrl(), this.context, Name.CLICK_REQUEST);
    }

  }

  void fetchAd(String url) {
    MoPubView moPubView = this.moPubView;
    if(moPubView != null && this.context != null) {
      AdRequest adRequest = new AdRequest(url, moPubView.getAdFormat(), this.adUnitId, this.context, this.adListener);
      MoPubRequestQueue requestQueue = Networking.getRequestQueue(this.context);
      requestQueue.add(adRequest);
      this.activeRequest = adRequest;
    } else {
      MoPubLog.d("Can\'t load an ad in this ad view because it was destroyed.");
      this.setNotLoading();
    }
  }

  void forceRefresh() {
    this.setNotLoading();
    this.loadAd();
  }

  @Nullable
  String generateAdUrl() {
    return this.urlGenerator == null? null :
      this.urlGenerator.withAdUnitId(this.adUnitId)
        .withKeywords(this.keywords)
        .withLocation(this.location)
        .generateUrlString("ads.mopub.com");
  }

  void adDidFail(MoPubErrorCode errorCode) {
    MoPubLog.i("Ad failed to load.");
    this.setNotLoading();
    MoPubView moPubView = this.moPubView;
    if(moPubView != null) {
      this.scheduleRefreshTimerIfEnabled();
      moPubView.adFailed(errorCode);
    }
  }

  void scheduleRefreshTimerIfEnabled() {
    this.cancelRefreshTimer();
    if(this.autoRefreshEnabled && this.refreshTimeMillis > 0) {
      this.handler.postDelayed(this.refreshRunnable, Math.min(600000L, (long)this.refreshTimeMillis * (long)Math.pow(1.5D, (double)this.backoffPower)));
    }

  }

  private void cancelRefreshTimer() {
    this.handler.removeCallbacks(this.refreshRunnable);
  }

  private boolean isNetworkAvailable() {
    if(this.context == null) {
      return false;
    } else {
      int result = this.context.checkCallingPermission("android.permission.ACCESS_NETWORK_STATE");
      if(result == -1) {
        return true;
      } else {
        ConnectivityManager cm = (ConnectivityManager)
          this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
      }
    }
  }

  void setAdContentView(final View view) {
    this.handler.post(new Runnable() {
      public void run() {
        MoPubView mpv = moPubView;
        if(mpv != null) {
          mpv.removeAllViews();
          mpv.addView(view, getAdLayoutParams(view));
        }
      }
    });
  }

  private LayoutParams getAdLayoutParams(View view) {
    Integer width = null;
    Integer height = null;
    if(this.adResponse != null) {
      width = this.adResponse.getWidth();
      height = this.adResponse.getHeight();
    }

    if(width != null && height != null && getShouldHonorServerDimensions(view) && width > 0 && height > 0) {
      int scaledWidth = Dips.asIntPixels((float)width, this.context);
      int scaledHeight = Dips.asIntPixels((float)height, this.context);
      return new LayoutParams(scaledWidth, scaledHeight, 17);
    } else {
      return WRAP_AND_CENTER_LAYOUT_PARAMS;
    }
  }

}

abstract class AdLoader {
  WeakReference<MadController> mWeakAdViewController;

  AdLoader(MadController adViewController) {
    this.mWeakAdViewController = new WeakReference<>(adViewController);
  }

  abstract void load();

  @Nullable
  static AdLoader fromAdResponse(AdResponse response, MadController adViewController) {
    MoPubLog.i("Performing custom event.");
    String adTypeCustomEventName = response.getCustomEventClassName();
    if(adTypeCustomEventName != null) {
      Map customEventData = response.getServerExtras();
      return new AdLoader.CustomEventAdLoader(adViewController, adTypeCustomEventName, customEventData);
    } else {
      MoPubLog.i("Failed to create custom event.");
      return null;
    }
  }

  static class CustomEventAdLoader extends AdLoader {
    private String mCustomEventClassName;
    private Map<String, String> mServerExtras;

    public CustomEventAdLoader(MadController adViewController, String customEventCLassName, Map<String, String> serverExtras) {
      super(adViewController);
      this.mCustomEventClassName = customEventCLassName;
      this.mServerExtras = serverExtras;
    }

    void load() {
      MadController adViewController = this.mWeakAdViewController.get();
      if(adViewController != null && !adViewController.isDestroyed && !TextUtils.isEmpty(this.mCustomEventClassName)) {
        adViewController.setNotLoading();
        MoPubView moPubView = adViewController.moPubView;
        if(moPubView == null) {
          MoPubLog.d("Can\'t load an ad in this ad view because it was destroyed.");
        } else {
          moPubView.loadCustomEvent(this.mCustomEventClassName, this.mServerExtras);
        }
      }
    }

    @VisibleForTesting
    Map<String, String> getServerExtras() {
      return this.mServerExtras;
    }
  }
}*/