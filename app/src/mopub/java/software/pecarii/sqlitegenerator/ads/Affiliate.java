package software.pecarii.sqlitegenerator.ads;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.mopub.common.MoPub;
import com.mopub.nativeads.MoPubNative;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.NativeAd;
import com.mopub.nativeads.NativeErrorCode;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.ViewBinder;

import java.lang.ref.WeakReference;
import java.util.EnumSet;

import software.pecarii.sqlitegenerator.R;

final class Affiliate extends Bup {

  static final NNListener listener = new NNListener();
  static final RequestParameters params;
  static final MoPubStaticNativeAdRenderer renderer;

  static {
    MoPub.setLocationAwareness(MoPub.LocationAwareness.DISABLED);
    MoPub.setLocationPrecision(0);
    final ViewBinder binder = new ViewBinder.Builder(R.layout.bup)
      .mainImageId(R.id.bupage)
      .privacyInformationIconImageId(R.id.bupbrother)
      .build();
    renderer = new MoPubStaticNativeAdRenderer(binder);
    final EnumSet<RequestParameters.NativeAdAsset> desiredAssets =
      EnumSet.of(RequestParameters.NativeAdAsset.MAIN_IMAGE);
    params = new RequestParameters.Builder()
      .desiredAssets(desiredAssets).build();
  }

  static class NNListener implements 
    MoPubNative.MoPubNativeNetworkListener {

    static public View adView;
    static public WeakReference<NativeAd> nad;

    @Override
    public void onNativeFail(NativeErrorCode errorCode) {
      System.out.println(errorCode.toString());
    }

    @Override
    public void onNativeLoad(NativeAd nativeAd) {
      System.out.println("Loaded !\n\n");
      System.out.println(nativeAd.toString());
      nativeAd.renderAdView(adView);
      nativeAd.prepare(adView);
      nad = new WeakReference<NativeAd>(nativeAd);
      Log.d("PECARII", nativeAd.getClass().getName());
      /*adView.findViewById(R.id.bupbrother).setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            System.out.println("afezFEFZEFZEFZEFZEFEZZZZZZZZZZZZZZZZZZZZZZ");

          }
        });*/
    }

  }

  @Override
  public void
  preparations_protegees(ViewGroup conteneur, Activity contexte) {
    ViewGroup.inflate(contexte, R.layout.bup, conteneur);

    NNListener.adView = conteneur.getChildAt(0);
    MoPubNative pouip = new MoPubNative(contexte,
      "2a0aa7127773402888ca760b82433546", listener
    );
    pouip.registerAdRenderer(renderer);
    pouip.makeRequest(params);
  }

  @Override
  public void
  detruire_protege(ViewGroup conteneur, Activity contexte) {
    NativeAd nad = NNListener.nad.get();
    if (nad != null) nad.destroy();
    conteneur.removeAllViews();
  }
}
