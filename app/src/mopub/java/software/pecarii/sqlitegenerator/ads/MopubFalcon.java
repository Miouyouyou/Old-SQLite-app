package software.pecarii.sqlitegenerator.ads;

/* First version : 2015-12-09
  To instruct MoPub to use this adapter, You'll have to
  - Open the 'Networks' tab
  - Add a 'Custom Native Network'
  - Put the full class name in 'Custom Class Event'
    Example : 'your.wonderful.package.name.MopubFalcon'

  When testing, be aware of the geographical restrictions you put on this
  network. If you removed geographical restrictions, put them back again
  when finished.
  Recommended geographical restrictions are :
  'Serve Only In: United Arab Emirates, Bahrain, Egypt, Iraq, Jordan,
   Kuwait, Lebanon, Morocco, Oman, Qatar, Saudi Arabia'

 */

import com.mopub.common.util.Views;
import com.mopub.mobileads.CustomEventBanner;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.mopub.mobileads.MoPubErrorCode;
import com.noqoush.adfalcon.android.sdk.ADFAd;
import com.noqoush.adfalcon.android.sdk.ADFListener;
import com.noqoush.adfalcon.android.sdk.ADFTargetingParams;
import com.noqoush.adfalcon.android.sdk.ADFView;
import com.noqoush.adfalcon.android.sdk.constant.ADFAdSize;
import com.noqoush.adfalcon.android.sdk.constant.ADFErrorCode;

// Single-line comments are taken from the documentation.
// Some are slightly modified
public class MopubFalcon extends CustomEventBanner  {

  public final static String
    // The 'App/Site' ID provided by AdFalcon. Looks like a MD5 Hash.
    SITE_ID = "917b13bac77a41b7ada27328473a891c",
    TAG = "MopubFalcon"; // Android Log TAG

  // Add private member variable of type ADFView
  private ADFView adFalconView;
  private CustomEventBannerListener mBannerListener;

  @Override
  protected void loadBanner(
    final Context context,
    final CustomEventBannerListener customEventBannerListener,
    final Map<String, Object> localExtras,
    final Map<String, String> serverExtras) {

    mBannerListener = customEventBannerListener;
    try {
      // create new instance of ADFView
      adFalconView = new ADFView(context);
      //Ensure test mode is setting to false when your app is uploaded in the marketing
      adFalconView.setTestMode(true);
      // initialize the view by pass site id, ad unit size, targeting parameters, listener
      // and enable auto refresh.
      adFalconView.initialize(SITE_ID, ADFAdSize.AD_UNIT_320x50, null, new Listener(), true);
    } catch (Exception ex) { }

  }

  @Override
  protected void onInvalidate() {
    adFalconView.destroy();
    Views.removeFromParent(adFalconView);
  }

  class Listener implements ADFListener {

    /* This is the part that shows the advertisement through MoPub */
    @Override
    public void onLoadAd(ADFAd ad) {
      Log.d(TAG, "Loaded");
      mBannerListener.onBannerLoaded(adFalconView);
    }

    @Override
    public void onError(ADFAd ad, ADFErrorCode code, String message) {
      Log.e(TAG, message);
      mBannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
    }

    @Override
    public void onPresentAdScreen(ADFAd ad) {
      Log.d(TAG, "Present");
      mBannerListener.onBannerExpanded();
    }

    @Override
    public void onDismissAdScreen(ADFAd ad) {
      Log.d(TAG, "Dismissed");
      mBannerListener.onBannerCollapsed();
    }

    @Override
    public void onLeaveApplication() {
      Log.d(TAG, "Left");
      mBannerListener.onLeaveApplication();
    }
  }
}
