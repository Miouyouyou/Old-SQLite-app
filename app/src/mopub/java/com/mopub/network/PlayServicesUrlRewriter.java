package com.mopub.network;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GoogleApiAvailability;

import com.mopub.common.ClientMetadata;
import com.mopub.common.GpsHelper;
import com.mopub.volley.toolbox.HurlStack;

import software.pecarii.sqlitegenerator.application.Application;

/**
 * Url Rewriter that replaces MoPub templates for Google Advertising ID and Do Not Track settings
 * when a request is queued for dispatch by the HurlStack in Volley.
 */
public class PlayServicesUrlRewriter implements HurlStack.UrlRewriter {
    private static final String IFA_PREFIX = "ifa:";

    public static final String UDID_TEMPLATE = "mp_tmpl_advertising_id";
    public static final String DO_NOT_TRACK_TEMPLATE = "mp_tmpl_do_not_track";
    
    public String udid;
    public Context context;
    public String deviceId;

    public PlayServicesUrlRewriter(final String deviceId, final Context context) {
      /*new Thread() {
        @Override
        public void run() {*/
      udid = null;
      this.deviceId = deviceId;
      this.context = context;
        /*}
      }.run();*/
    }

    @Override
    public String rewriteUrl(final String url) {
        // Fill in the templates
        if (udid != null);
        else {
          try {
            GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
            int return_code =  instance.isGooglePlayServicesAvailable(context);
            if (return_code == 0)
              udid = Uri.encode(IFA_PREFIX + AdvertisingIdClient.getAdvertisingIdInfo(context).getId());
            else
              udid = deviceId;
          }
          catch (Exception e) { udid = deviceId; System.out.println(e.getMessage()); }
        }
        String toReturn = url.replace(UDID_TEMPLATE, udid);
        System.out.println("PECARII -- " + toReturn);
        toReturn = toReturn.replace(DO_NOT_TRACK_TEMPLATE, "1");
        return toReturn;
    }
}
