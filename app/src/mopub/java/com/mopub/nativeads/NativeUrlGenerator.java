package com.mopub.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mopub.common.AdUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Constants;

class NativeUrlGenerator extends AdUrlGenerator {
    @Nullable private String mDesiredAssets;
    @Nullable private String mSequenceNumber;

    public NativeUrlGenerator(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public NativeUrlGenerator withAdUnitId(final String adUnitId) {
        mAdUnitId = adUnitId;
        return this;
    }

    @NonNull
    public NativeUrlGenerator withRequest(@Nullable final RequestParameters requestParameters) {
        if (requestParameters != null) {
            mKeywords = requestParameters.getKeywords();
            mLocation = requestParameters.getLocation();
            mDesiredAssets = requestParameters.getDesiredAssets();
        }
        return this;
    }

    @NonNull
    public NativeUrlGenerator withSequenceNumber(final int sequenceNumber) {
        mSequenceNumber = String.valueOf(sequenceNumber);
        return this;
    }

    // TODO : Refaire ça !
    @Override
    public String generateUrlString(final String serverHostname) {
        initUrlString(serverHostname, Constants.AD_HANDLER);

        ClientMetadata clientMetadata = ClientMetadata.getInstance(mContext);
        addBaseParams(clientMetadata);

        setDesiredAssets();

        setSequenceNumber();

        String a = getFinalUrlString();
        System.out.println(a);
        return a;
    }

    public void setSequenceNumber() {
       if (!TextUtils.isEmpty(mSequenceNumber)) {
           addParam("MAGIC_NO", mSequenceNumber);
       }
    }

    public void setDesiredAssets() {
        if (!TextUtils.isEmpty(mDesiredAssets)) {
            addParam("assets", mDesiredAssets);
        }
    }

    @Override
    protected void setSdkVersion(String sdkVersion) {
        addParam("nsv", sdkVersion);
    }
}
