package com.mopub.common;

import android.graphics.Point;
import android.net.Uri;
import android.util.Log;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.mopub.network.Networking;
import com.mopub.network.PlayServicesUrlRewriter;

public abstract class BaseUrlGenerator {

    public static final String APIVERSION_KEY = "v";
    public static final String APPVERSION_KEY = "av";
    public static final String ANDROID_EXTERNAL_STORAGE_PERM =
      "android_perms_ext_storage";
    public static final String DEVICE_INFO_KEY = "dn";
    public static final String DO_NOT_TRACK_KEY = "dnt";
    public static final String UDID_KEY = "udid";
    public static final String WIDTH_KEY = "w";
    public static final String HEIGHT_KEY = "h";


    public char current_delimiter = '?';
    public StringBuilder mStringBuilder;

    public abstract String generateUrlString(String serverHostname);

    public void initUrlString(String serverHostname, String handlerType) {
        mStringBuilder = new StringBuilder("https://").append(serverHostname).append(handlerType);
        current_delimiter = '?';
    }

    public String getFinalUrlString() {
        return mStringBuilder.toString();
    }

    public void addParam(String key, String value) {
        if (TextUtils.isEmpty(value)) {
            return;
        }

        mStringBuilder.append(current_delimiter);
        mStringBuilder.append(key);
        mStringBuilder.append("=");
        mStringBuilder.append(Uri.encode(value));
        this.current_delimiter = '&';
    }

    public void setApiVersion(String apiVersion) {
        addParam(APIVERSION_KEY, apiVersion);
    }

    public void setAppVersion(String appVersion) {
        addParam(APPVERSION_KEY, appVersion);
    }

    public void setExternalStoragePermission(boolean isExternalStoragePermissionGranted) {
        addParam(ANDROID_EXTERNAL_STORAGE_PERM, 
                 isExternalStoragePermissionGranted ? "1" : "0");
    }

    public void setDeviceInfo(String... info) {
        StringBuilder parsed_infos = new StringBuilder();
        if (info != null || info.length != 0);
        else return;

        for (int i=0; i<info.length-1; i++) {
            parsed_infos.append(info[i]).append(",");
        }
        parsed_infos.append(info[info.length-1]);

        addParam(DEVICE_INFO_KEY, parsed_infos.toString());
    }

    public void setDoNotTrack(boolean dnt) {
            addParam(DO_NOT_TRACK_KEY, "1");
    }

    public void setUdid(String udid) { addParam(UDID_KEY, udid); }

    /**
     * Appends special keys/values for advertising id and do-not-track. PlayServicesUrlRewriter will
     * replace these templates with the correct values when the request is processed.
     */
    public void appendAdvertisingInfoTemplates() {
        Log.d("PECARII", "Called appendAdvertisingInfoTemplates");
        addParam("udid", PlayServicesUrlRewriter.UDID_TEMPLATE);
        setDoNotTrack(true);
    }

    /**
     * Adds the width and height.
     *
     * @param dimensions The width and height of the screen
     */
    public void setDeviceDimensions(@NonNull final Point dimensions) {
        addParam(WIDTH_KEY, "" + dimensions.x);
        addParam(HEIGHT_KEY, "" + dimensions.y);
    }
}
