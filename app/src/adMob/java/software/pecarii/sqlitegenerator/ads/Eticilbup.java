package software.pecarii.sqlitegenerator.ads;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Eticilbup {
  public static void
  tutup(@IdRes int id, ViewGroup conteneur, String motclef) {
    tutup(conteneur.findViewById(id), motclef);
  }
  public static void tutup(View view, String motclef) {
    ((AdView) view).loadAd(new AdRequest.Builder().addKeyword(motclef).build());
  }

  public static void dling(View view) { ((AdView) view).destroy(); }

  public static void preps(Context context) {}
}
